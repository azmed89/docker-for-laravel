FROM php:7.4-fpm-alpine

# @TODO use ARG to take in the version of Phalcon on build time
ENV PHALCON_VERSION 4.1.0
ENV XDEBUG_VERSION 3.0.1
ENV XDEBUG_PORT 9000

COPY ./php/local.ini /etc/php7/conf.d/local.ini

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer --version=1.10.19

# Pre run
RUN docker-php-source extract && apk add --update --virtual .build-deps autoconf g++ make pcre-dev icu-dev openssl-dev

# Install GIT
RUN apk add git openssh

# Install mysql goodness
RUN docker-php-ext-install mysqli pdo_mysql

# Instaling redis
# RUN pecl install redis

# Installing mongo
#RUN pecl install mongo

# enable pecl modules
# RUN docker-php-ext-enable redis
#RUN docker-php-ext-enable redis mongo

# Installing CakePHP deps
RUN apk add icu-libs icu
RUN docker-php-ext-install intl

# Installing Phalcon deps
# RUN cd /usr/local/etc/php/
# RUN curl -LO https://github.com/phalcon/cphalcon/archive/v${PHALCON_VERSION}.tar.gz
# RUN tar xzf v${PHALCON_VERSION}.tar.gz
# RUN cd cphalcon-${PHALCON_VERSION}/build
# RUN sh install && echo "extension=phalcon.so" > /usr/local/etc/php/conf.d/phalcon.ini

# Installing XDebug
# RUN cd /usr/local/etc/php/
# RUN curl -LO http://xdebug.org/files/xdebug-$XDEBUG_VERSION.tgz
# RUN tar -zxvf xdebug-$XDEBUG_VERSION.tgz
# RUN cd xdebug-$XDEBUG_VERSION && phpize
# RUN ./configure --enable-xdebug && make && make install
# RUN echo "zend_extension=xdebug.so" > /usr/local/etc/php/conf.d/xdebug.ini
# RUN echo "xdebug.remote_enable=on" >> /usr/local/etc/php/conf.d/xdebug.ini
# RUN echo "xdebug.remote_handler=dbgp" >> /usr/local/etc/php/conf.d/xdebug.ini
# RUN echo "xdebug.remote_connect_back=1" >> /usr/local/etc/php/conf.d/xdebug.ini
# RUN echo "xdebug.remote_autostart=on" >> /usr/local/etc/php/conf.d/xdebug.ini
# RUN echo "xdebug.remote_port=${XDEBUG_PORT}" >> /usr/local/etc/php/conf.d/xdebug.ini

# Post run
RUN docker-php-source delete
RUN apk del --purge .build-deps
RUN rm -rf /tmp/pear
RUN rm -rf /var/cache/apk/*
# RUN rm -rf /usr/local/etc/php/cphalcon-${PHALCON_VERSION}
# RUN rm /usr/local/etc/php/v${PHALCON_VERSION}.tar.g

# Set working directory
WORKDIR /var/www

# Copy composer.lock and composer.json
COPY composer.lock composer.json ./

#RUN php -d memory_limit=-1

RUN php -r "echo ini_get ('memory_limit'). PHP_EOL;"
# RUN rm -rf vendor
# RUN composer clear-cache

#RUN composer install --no-scripts --no-autoloader
COPY ./src ./
RUN cp .env.example .env
#RUN rm .env.pre_prod
#RUN composer dump-autoload --optimize
RUN composer -v
RUN composer dump-autoload
RUN composer clear-cache

RUN php artisan key:generate
RUN php artisan jwt:secret
RUN php artisan storage:link
RUN php artisan migrate

# Copy existing application directory contents
# COPY ./src /var/www

COPY --chown=www:www ./src /var/www

USER www

# Expose new ports
EXPOSE ${XDEBUG_PORT}