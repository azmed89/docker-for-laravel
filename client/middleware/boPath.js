export default function({ store, redirect, $auth, route }) {
  let user = $auth.$state.user
  let users_path = {
    user_prestataire: 'prestataire',
    user_assistance: 'assistance',
    user_assurance: 'assurance',
    user: 'admin'
  }
  //console.log(users_path[user.type])
  if(user) store.dispatch('path/setBoBase', users_path[user.type])
  else store.dispatch('path/setBoPath')
  //console.log(store.getters['path/bo_path'])
  let bo_path = store.getters['path/bo_path'].path
  //console.log('bo-'+bo_path)
  //console.log('route-'+route.name)
  if(route.name == 'Home' && user) redirect(`/${bo_path}`)
}
