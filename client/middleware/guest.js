export default function({ store, redirect }) {
  if(store.getters['authenticated']) {
    let base_path = store.getters['path/bo_path']
    return redirect('/' + base_path.path)
  }
}
