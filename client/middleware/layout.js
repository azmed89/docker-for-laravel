export default function({ store, route }) {
  const page = store.getters['path/bo_path'].path
  switch (page) {
    case 'prestataire':
      return 'prestataire'
    case 'assurance':
      return 'assurance'
    case 'assistance':
      return 'assistance'
    case 'admin':
      return 'admin'
    default:
      return 'default'
  }
}
