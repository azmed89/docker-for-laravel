export default class Constateur {
  constructor(id_constateur, nom, prenom, email, password, adresse, telephone, fonction, avatar, identifiant, num_agent, acces) {
    this.id_constateur = id_constateur
    this.nom = nom
    this.prenom = prenom
    this.adresse = adresse
    this.telephone = telephone
    this.email = email
    this.identifiant = identifiant
    this.avatar = avatar
    this.password = password
    this.num_agent = num_agent
    this.acces = acces
  }
}
