export default class Constat {
    constructor(id_constat, type_mission, code, num_immatriculation, etat, num_contrat, liee_a, motif, perimetre) {
      this.id_constat = id_constat
      this.type_mission = type_mission
      this.code = code
      this.num_immatriculation = num_immatriculation
      this.etat = etat
      this.num_contrat = num_contrat
      this.liee_a = liee_a
      this.motif = motif
      this.perimetre = perimetre
    }
}
