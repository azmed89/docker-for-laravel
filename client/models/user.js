export default class User {
    constructor(id, first_name, last_name, email, password, role, phone, identifier, avatar) {
        this.id = id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.phone = phone
        this.email = email;
        this.identifier = identifier
        this.avatar = avatar
        this.password = password;
        this.role = role;
    }
}
