import colors from 'vuetify/es5/util/colors'

export default {
  /*
  ** Nuxt rendering mode
  ** See https://nuxtjs.org/api/configuration-mode
  */
  mode: 'spa',
  /*
  ** Nuxt target
  ** See https://nuxtjs.org/api/configuration-target
  */
  target: 'server',
  /*
  ** Headers of the page
  ** See https://nuxtjs.org/api/configuration-head
  */
  head: {
    titleTemplate: '%s - ' + process.env.npm_package_name,
    title: process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ],
    script: [
      {
        src: 'https://code.jquery.com/jquery-3.5.1.min.js'
      }
    ]
  },
  /*
  ** Global CSS
  */
  css: [
    '~/assets/variables.scss',
    '~/assets/custom.css'
  ],
  /*
  ** Loading
   */
  loading: {
    color: '#AA2232'
  },
  loadingIndicator: {
    name: 'folding-cube',
    color: '#AA2232',
    background: 'white'
  },
  /*
  ** Plugins to load before mounting the App
  ** https://nuxtjs.org/guide/plugins
  */
  plugins: [
    { src: '~plugins/vuetify.js' },
    { src: '~plugins/echarts.js' },
    { src: '~plugins/vee-validate.js' },
    '~plugins/mixins/path.js',
    '~plugins/mixins/validation.js',
    '~plugins/mixins/user.js',
    '~plugins/mixins/models.js',
    '~plugins/axios.js',
    { src: '~plugins/base.js' },
    { src: '~plugins/chartist.js' },
    { src: '~plugins/popover.js' },
    { src: '~plugins/x5-gmaps.js' },
    { src: '~plugins/vue-pdf.js' },
    { src: '~plugins/vuescroll.js' }
  ],
  /*
  ** Auto import components
  ** See https://nuxtjs.org/api/configuration-components
  */
  components: true,
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    '@nuxtjs/vuetify',
    '@nuxtjs/router',
    '@nuxtjs/moment',
    '@nuxtjs/dotenv'
  ],
  moment: {
    defaultLocale: 'fr',
    locales: ['fr']
  },
  /*
  ** Nuxt.js modules
  */
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/auth',
    '@nuxtjs/proxy'
  ],
  /*
  ** Proxy
   */
  /*proxy: {
    '/storage/': {
      target: 'http://api.econsta:8001/',
      pathRewrite: {'^/storage/': ''},
      changeOrigin: true
    }
  },*/
  /*
  ** ENV
   */
  env: {
    storageUrl: process.env.APP_STORAGE
  },
  /*
  ** Middleware
   */
  router: {
    middleware: [
      'clearValidationErrors',
      'boPath',
      'auth'
    ]
  },
  /*
  * Axios
  */
  axios: {
    baseURL: 'http://localhost:80/api/v1'
  },

  /*
  * Auth schema
  */
  auth: {
    strategies: {
      admin: {
        scheme: 'refresh',
        _scheme: 'local',
        token: {
          property: 'access_token',
          maxAge: 1800,
          // type: 'Bearer'
        },
        refreshToken: {
          property: 'refresh_token',
          data: 'refresh_token',
          maxAge: 60 * 60 * 24 * 30
        },
        user: {
          property: 'payload',
          // autoFetch: true
        },
        endpoints: {
          login: { url: '/admin/auth/login', method: 'post', propertyName: 'payload.token' },
          logout: { url: '/admin/auth/logout', method: 'get' },
          user: { url: '/admin/auth/me', method: 'get', propertyName: 'payload' },
          refresh: { url: '/admin/auth/refresh', method: 'post', propertyName: 'payload.token' }
        }
      },
      prestataire: {
        scheme: 'refresh',
        _scheme: 'local',
        token: {
          property: 'access_token',
          maxAge: 1800,
          // type: 'Bearer'
        },
        refreshToken: {
          property: 'refresh_token',
          data: 'refresh_token',
          maxAge: 60 * 60 * 24 * 30
        },
        user: {
          property: 'payload',
          // autoFetch: true
        },
        endpoints: {
          login: { url: '/pre/auth/login', method: 'post', propertyName: 'payload.token' },
          logout: { url: '/pre/auth/logout', method: 'get' },
          user: { url: '/pre/auth/me', method: 'get', propertyName: 'payload' },
          refresh: { url: '/pre/auth/refresh', method: 'post', propertyName: 'payload.token' }
        }
        // tokenRequired: true,
        // tokenType: 'bearer',
        // globalToken: true,
        // autoFetchUser: true
      },
      assistance: {
        scheme: 'refresh',
        _scheme: 'local',
        token: {
          property: 'access_token',
          maxAge: 1800,
          // type: 'Bearer'
        },
        refreshToken: {
          property: 'refresh_token',
          data: 'refresh_token',
          maxAge: 60 * 60 * 24 * 30
        },
        user: {
          property: 'payload',
          // autoFetch: true
        },
        endpoints: {
          login: { url: '/ass/auth/login', method: 'post', propertyName: 'payload.token' },
          logout: { url: '/ass/auth/logout', method: 'get' },
          user: { url: '/ass/auth/me', method: 'get', propertyName: 'payload' },
          refresh: { url: '/ass/auth/refresh', method: 'post', propertyName: 'payload.token' }
        }
      },
      assurance: {
        scheme: 'refresh',
        _scheme: 'local',
        token: {
          property: 'access_token',
          maxAge: 1800,
          // type: 'Bearer'
        },
        refreshToken: {
          property: 'refresh_token',
          data: 'refresh_token',
          maxAge: 60 * 60 * 24 * 30
        },
        user: {
          property: 'payload',
          // autoFetch: true
        },
        endpoints: {
          login: { url: '/assu/auth/login', method: 'post', propertyName: 'payload.token' },
          logout: { url: '/assu/auth/logout', method: 'get' },
          user: { url: '/assu/auth/me', method: 'get', propertyName: 'payload' },
          refresh: { url: '/assu/auth/refresh', method: 'post', propertyName: 'payload.token' }
        }
      }
    },
    redirect: {
      login: '/auth/login',
      home: '/admin',
      home_pre: '/prestataire',
      home_ass: '/assistance',
      home_ass: '/assurance'
    },
    plugins: [
      './plugins/auth'
    ]
  },

  /*
  ** vuetify module configuration
  ** https://github.com/nuxt-community/vuetify-module
  */
  vuetify: {
    customVariables: ['~/sass/variables.scss'],
    theme: {
      dark: false,
      light: true,
      themes: {
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3
        },
        light: {
          primary: '#AA2232',
          accent: colors.grey.lighten3,
          secondary: colors.amber.lighten3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3
        }
      }
    }
  },
  /*
  ** Build configuration
  ** See https://nuxtjs.org/api/configuration-build/
  */
  build: {
    transpile: ["vee-validate/dist/rules"],
    transpileDependencies: ['x5-gmaps'],
    vendor: [
      'vue-pdf'
    ]
  },

  server: {
    port: 3000,
    host: '0.0.0.0'
  }
}
