export default function({ $axios, store }) {
  $axios.onError(error => {
    if(error.response.status == 422) {
      store.dispatch('validation/setErrors', error.response.data.errors)
    }

    if(error.response.status == 401) {
      //store.dispatch('validation/setErrors', error.response.data.errors)
      //this.$auth.refreshTokens()
      /*$axios.post('bo/auth/refresh').then(
        res => {
          this.$auth.token.set(res.data.payload.token)
        },
        error => {
          this.message =
            (error.response && error.response.data) ||
            error.message ||
            error.toString();
          console.log(this.message)
        }
      );*/
    }

    if(error.response.data._response.code == 'L401_01') {
      //console.log(error.response.data._response.message)
      store.dispatch('validation/setErrors', error.response.data._response.message)
      this.$auth.refreshTokens()
    }

    if(error.response.data._response.code == 'G406_00') {
      store.dispatch('validation/setErrors', error.response.data._response.message)
    }

    if(error.response.data._response.code == '400_00') {
      console.log(Object.values(error.response.data.payload))
      store.dispatch('validation/setErrors', Object.values(error.response.data.payload))
    }

    return Promise.reject(error)
  })

  $axios.onRequest(() => {
    store.dispatch('validation/clearErrors')
  })
}
