import Vue from 'vue'
import {ChartCard} from 'vue-material/src/components'

import 'echarts/lib/chart/bar'
import 'echarts/lib/component/title'
import 'echarts/lib/chart/line'

Vue.component('chart', Echarts)
