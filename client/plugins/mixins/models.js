import Vue from 'vue'

import { mapGetters } from 'vuex'

const Models = {
  install(Vue, options) {
    Vue.mixin({
      computed: {
        ...mapGetters({
          constat: 'dossier/details'
        })
      }
    })
  }
}

Vue.use(Models)
