import Vue from 'vue'

import { mapGetters } from 'vuex'

const Validation = {
  install(Vue, options) {
    Vue.mixin({
      computed: {
        ...mapGetters({
          boPath: 'path/bo_path'
        })
      }
    })
  }
}

Vue.use(Validation)
