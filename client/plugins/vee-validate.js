import { extend } from "vee-validate";
import { required, email, min, regex, numeric, image } from "vee-validate/dist/rules";

extend("required", {
  ...required,
  message: "{_field_} is required"
});

extend('min', {
  ...min,
  message: '{_field_} may not be less than {length} characters',
})

extend('email', {
  ...email,
  message: 'Email must be valid',
})

extend("regex", regex)

extend("numeric", numeric)

extend("image", image)
