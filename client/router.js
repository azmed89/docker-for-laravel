  import Vue from 'vue'
  import Router from 'vue-router'

  import Dashboard from '~/pages/Dashboard'
  import Login from '~/pages/auth/Login'
  import Register from '~/pages/auth/Register'
  import ListUsers from '~/pages/users/Listing'
  import Calendar from '~/pages/Calendar'
  import UserProfile from '~/pages/users/UserProfile'

  // Prestataire
  import ListDossiers from '~/pages/prestataire/dossiers/Listing'
  import DetailsDossier from '~/pages/prestataire/dossiers/_id'
  import ListAgents from '~/pages/prestataire/agents/Listing'
  import ListUsersBo from '~/pages/prestataire/utilisateursBo/Listing'
  import DashboardPrestataire from '~/pages/prestataire/index'

  // Assistance
  import ListDossiersAssistance from '~/pages/assistance/dossiers/Listing'
  import ListUsersBoAssistance from '~/pages/assistance/utilisateursBo/Listing'
  import DetailsDossierAssistance from '~/pages/assistance/dossiers/_id'
  import DashboardAssistance from '~/pages/assistance/index'

  // Assurance
  import ListDossiersAssurance from '~/pages/assurance/dossiers/Listing'
  import ListUsersBoAssurance from '~/pages/assurance/utilisateurs/Listing'
  import DetailsDossierAssurance from '~/pages/assurance/dossiers/_id'
  import DashboardAssurance from '~/pages/assurance/index'
  import UserAssuranceProfile from '~/pages/assurance/utilisateurs/UserProfile'


  Vue.use(Router)

  export function createRouter() {
    return new Router({
      mode: 'history',
      routes: [
        // Prestataire
        {
          name: 'home-prestataire',
          path: '/prestataire',
          component: DashboardPrestataire,
        },
        {
          name: 'dashboard-prestataire',
          path: '/prestataire/dashboard',
          component: DashboardPrestataire,
        },
        {
          name: 'gestion-dossiers',
          path: '/prestataire/dossiers',
          component: ListDossiers
        },
        {
          name: 'details-dossier',
          path: '/prestataire/dossiers/:id',
          component: DetailsDossier
        },
        {
          name: 'Gestions des agents',
          path: '/prestataire/agents',
          component: ListAgents
        },
        {
            name: 'Gestions des Utilisateurs',
            path: '/prestataire/utilisateursBo',
            component: ListUsersBo
        },
        {
          name: 'Traking',
          path: '/prestataire/tracking',
          component: ListDossiers
        },

        // Assistance
        {
          name: 'home-assistance',
          path: '/assistance',
          component: DashboardAssistance,
        },
        {
          name: 'dashboard-assistance',
          path: '/assistance/dashboard',
          component: DashboardAssistance,
        },
        {
          name: 'gestion-dossiers-assistance',
          path: '/assistance/dossiers',
          component: ListDossiersAssistance
        },
        {
          name: 'details-dossier-assistance',
          path: '/assistance/dossiers/:id',
          component: DetailsDossierAssistance
        },
        {
          name: 'gestions-utilisateurs-assistance',
          path: '/assistance/utilisateursBo',
          component: ListUsersBoAssistance
        },

        // Assurance
        {
          name: 'home-assurance',
          path: '/assurance',
          component: DashboardAssurance,
        },
        {
          name: 'dashboard-assurance',
          path: '/assurance/dashboard',
          component: DashboardAssurance,
        },
        {
          name: 'gestion-dossiers-assurance',
          path: '/assurance/dossiers',
          component: ListDossiersAssurance
        },
        {
          name: 'details-dossier-assurance',
          path: '/assurance/dossiers/:id',
          component: DetailsDossierAssurance
        },
        {
          name: 'gestions-utilisateurs-assurance',
          path: '/assurance/utilisateurs',
          component: ListUsersBoAssurance
        },
        {
          name: 'assurance-profile',
          path: '/assurance/profile',
          component: UserAssuranceProfile
        },

        // Dashboard
        {
          name: 'Home',
          path: '/',
          component: Dashboard
        },
        {
          name: 'Tableau de bord',
          path: '/dashboard',
          component: Dashboard
        },
        {
          name: 'Register',
          path: '/auth/register',
          component: Register
        },
        {
          name: 'Login',
          path: '/auth/login',
          component: Login
        },
        {
          name: 'Users',
          path: '/prestataire/users',
          component: ListUsers
        },
        {
          name: 'User profile',
          path: '/profile',
          component: UserProfile
        },
        {
          name: 'Calendar',
          path: '/calendar',
          component: Calendar
        }
      ]
    })
  }
