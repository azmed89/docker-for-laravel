//const API_URL = axios.defaults.baseURL;
class ConstatService {
  getDossiers() {
    return $nuxt.$axios.get('pre/dossier/list',
    {
      headers: {
        "Access-Control-Allow-Origin": "*",
        "Content-Type": "text/plain"
      }
    });
  }

  getLastDossiers(limit) {
    return $nuxt.$axios.get(`pre/dossier/list/${limit}`);
  }

  getDossier(id_constat, back_path = 'pre') {
    return $nuxt.$axios.get(`${back_path}/dossier/get/${id_constat}`);
  }

  search(filter) {
    return $nuxt.$axios.post(`pre/dossier/search`, filter);
  }

  check_file(id_constat) {
    return $nuxt.$axios.get(`pre/dossier/check_pdf/${id_constat}`);
  }
  getAssistanceDossiers() {
    return $nuxt.$axios.get('ass/dossier/list',
      {
        headers: {
          "Access-Control-Allow-Origin": "*",
          "Content-Type": "text/plain"
        }
      });
  }
    getLastAssistanceDossiers(limit) {
        return $nuxt.$axios.get(`ass/dossier/list/${limit}`);
    }
  searchAssistance(filter) {
    return $nuxt.$axios.post(`ass/dossier/search`, filter);
  }
  validate(id_constat) {
    return $nuxt.$axios.post(`ass/dossier/validate/${id_constat}`);
  }

  // Assurance
  getAssuranceDossiers() {
    return $nuxt.$axios.get('assu/dossier/list');
  }
  searchAssurance(filter) {
    return $nuxt.$axios.post(`assu/dossier/search`, filter);
  }
  getLastDossiersAssurance(limit) {
    return $nuxt.$axios.get(`assu/dossier/list/${limit}`);
  }
}

export default new ConstatService();
