//const API_URL = axios.defaults.baseURL;
class ConstateurService {
  getAgents() {
    return $nuxt.$axios.get('pre/user/listconst');
  }

  getAgent(id_constateur) {
    return $nuxt.$axios.get(`pre/user/getconst/${id_constateur}`);
  }

  editAgent(constateur) {
    let formData = new FormData()
    if(constateur.avatar != undefined && constateur.avatar != '') formData.append('avatar', constateur.avatar)
    formData.append('nom', constateur.nom)
    formData.append('identifiant', constateur.identifiant)
    formData.append('prenom', constateur.prenom)
    formData.append('telephone', constateur.telephone)
    formData.append('adresse', constateur.adresse)
    formData.append('fonction', constateur.fonction)
    formData.append('email', constateur.email)
    formData.append('num_agent', constateur.num_agent)
    if(constateur.password != undefined && constateur.password != '') formData.append('password', constateur.password)

    return $nuxt.$axios.post(`pre/user/updateconst/${constateur.id_constateur}`, formData,
      {
        headers: {
          "Content-Type": "multipart/form-data"
        }
      }
    );
  }

  addAgent(constateur) {
    let formData = new FormData()
    formData.append('avatar', constateur.avatar)
    formData.append('nom', constateur.nom)
    formData.append('identifiant', constateur.identifiant)
    formData.append('prenom', constateur.prenom)
    formData.append('telephone', constateur.telephone)
    formData.append('adresse', constateur.adresse)
    formData.append('fonction', constateur.fonction)
    formData.append('email', constateur.email)
    formData.append('num_agent', constateur.num_agent)
    formData.append('password', constateur.password)
    formData.append('id_prestataire', 1)

    /*return $nuxt.$axios({
      method: 'post',
      url: '/pre/user/createconst',
      data: formData,
      headers: {
        "Content-Type": "multipart/form-data"
      }
    });*/
    return $nuxt.$axios.post('/pre/user/createconst', formData,
      {
        headers: {
          "Content-Type": "multipart/form-data"
        }
      }
    );
  }
}

export default new ConstateurService();
