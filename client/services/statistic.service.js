//const API_URL = axios.defaults.baseURL;
class StatisticService {
  getPrestataireStatistics() {
    return $nuxt.$axios.post('pre/statistic/ops');
  }

  getAssuranceStatistics() {
    return $nuxt.$axios.post('assu/statistique/ops');
  }
}

export default new StatisticService();
