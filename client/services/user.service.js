import axios from 'axios';
import authHeader from './auth-header';

const API_URL = axios.defaults.baseURL;

class UserService {
    getUser() {
        return axios.get('/auth/user', { headers: authHeader() });
    }

    getUsers() {
        return axios.get('/bo/user/list', { headers: authHeader() });
    }

    addUser(user) {
        return axios.post('/user/new', {
            name: user.name,
            email: user.email,
            password: user.password
        }, { headers: authHeader() });
    }

    getAdminBoard() {
        return axios.get('admin', { headers: authHeader() });
    }
}

export default new UserService();
