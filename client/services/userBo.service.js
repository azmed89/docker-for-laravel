//const API_URL = axios.defaults.baseURL;
class UserBoService {
    getUsersBo() {
        return $nuxt.$axios.get('pre/user/list');
    }

    getUserBo(id) {
        return this.$axios.get(`pre/user/get/${id}`);
    }

    addUserBo(user) {
        let formData = new FormData()
        formData.append('avatar', user.avatar)
        formData.append('nom', user.nom)
        formData.append('prenom', user.prenom)
        formData.append('phone', user.phone)
        formData.append('fonction', user.role)
        formData.append('email', user.email)
        formData.append('password', user.password)
        console.log(user.role);
        return $nuxt.$axios.post('/pre/user/create', formData,{
            headers: {
                "Content-Type": "multipart/form-data"
            }

        }
        );
    }

    editUserBo(user) {
        let formData = new FormData()
        if(user.avatar != undefined && user.avatar != '') formData.append('avatar', user.avatar)
        formData.append('nom', user.nom)
        formData.append('identifiant', user.identifiant)
        formData.append('prenom', user.prenom)
        formData.append('phone', user.phone)
        formData.append('adresse', user.adresse)
        formData.append('fonction', user.fonction)
        formData.append('email', user.email)
        formData.append('num_agent', user.num_agent)
        if(user.password != undefined && user.password != '') formData.append('password', user.password)

        return $nuxt.$axios.post(`pre/user/update/${user.id}`, formData,
            {
                headers: {
                    "Content-Type": "multipart/form-data"
                }
            }
        );
    }
}

export default new UserBoService();