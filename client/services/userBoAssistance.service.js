//const API_URL = axios.defaults.baseURL;
class UserBoAssistanceService {
    getUsersBo() {
        return $nuxt.$axios.get('ass/user/list');
    }

    getUserBo(id) {
        return this.$axios.get(`ass/user/get/${id}`);
    }

    addUserBo(user) {
        //console.log(user.avatar);
        let formData = new FormData()
        formData.append('avatar', user.avatar)
        formData.append('nom', user.nom)
        formData.append('prenom', user.prenom)
        formData.append('telephone', user.telephone)
        formData.append('fonction', user.role)
        formData.append('email', user.email)
        formData.append('password', user.password)
        //console.log(user.role);
        return $nuxt.$axios.post('/ass/user/create', formData,{
                headers: {
                    "Content-Type": "multipart/form-data"
                }

            }
        );
    }

    editUserBo(user) {
        console.log(user.id_user_ass);
        let formData = new FormData()
        if(user.avatar != undefined && user.avatar != '') formData.append('avatar', user.avatar)
        formData.append('nom', user.nom)
        formData.append('prenom', user.prenom)
        formData.append('telephone', user.telephone)
        formData.append('fonction', user.fonction)
        formData.append('email', user.email)
        if(user.password != undefined && user.password != '') formData.append('password', user.password)

        return $nuxt.$axios.post(`ass/user/update/${user.id_user_ass}`, formData,
            {
                headers: {
                    "Content-Type": "multipart/form-data"
                }
            }
        );
    }
}

export default new UserBoAssistanceService();