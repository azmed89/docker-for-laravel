//const API_URL = axios.defaults.baseURL;
class UserAssuranceService {
  getUsers() {
    return $nuxt.$axios.get('assu/user/list');
  }

  getUser(id) {
    return this.$axios.get(`assu/user/get/${id}`);
  }

  addUser(user) {
    //console.log(user.avatar);
    let formData = new FormData()
    formData.append('avatar', user.avatar)
    formData.append('nom', user.nom)
    formData.append('prenom', user.prenom)
    formData.append('telephone', user.telephone)
    formData.append('role', user.role)
    formData.append('email', user.email)
    formData.append('password', user.password)
    return $nuxt.$axios.post('/assu/user/create', formData, {
        headers: {
          "Content-Type": "multipart/form-data"
        }

      }
    );
  }

  editUser(user) {
    let formData = new FormData()
    if (user.avatar != undefined && user.avatar != '') formData.append('avatar', user.avatar)
    formData.append('nom', user.nom)
    formData.append('prenom', user.prenom)
    formData.append('telephone', user.telephone)
    formData.append('role', user.role)
    formData.append('email', user.email)
    if (user.password != undefined && user.password != '') formData.append('password', user.password)

    return $nuxt.$axios.post(`assu/user/update/${user.id_user_assu}`, formData,
      {
        headers: {
          "Content-Type": "multipart/form-data"
        }
      }
    );
  }
}

export default new UserAssuranceService();
