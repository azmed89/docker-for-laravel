export const state = () => ({
  constat: {}
})

export const getters = {
  details(state) {
    return state.constat;
  }
}

export const mutations = {
  SAVE_DOSSIER_INFOS(state, constat) {
    state.constat = constat
  }
}

export const actions = {
  setDossierInfos({commit}, constat) {
    commit('SAVE_DOSSIER_INFOS', constat)
  }
}
