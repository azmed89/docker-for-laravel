export const getters = {
  authenticated(state) {
    return state.auth.loggedIn
  },
  user(state) {
    return state.auth.user
  }
}


export const state = () => ({
  mini: false
})

export const mutations = {
  change_drawer (state, status) {
    state.mini = status
  }
}

