export const state = () => ({
  boPath: {}
})

export const getters = {
  bo_path(state) {
    return state.boPath;
  }
}

export const mutations = {
  SET_BO_PATH(state, from) {
    let path = $nuxt.$route.path
    path.indexOf(1)
    path.toLowerCase()
    path = path.split("/")[1]
    if(from) {
      from.indexOf(1)
      from.toLowerCase()
      from = from.split("/")[1]
    }
    let bo_paths = [
      {path: 'admin', short: 'admin'},
      {path: 'prestataire', short: 'pre'},
      {path: 'assistance', short: 'ass'},
      {path: 'assurance', short: 'assu'}
    ]
    let cur_path = bo_paths.find(elem => elem.path === path)
    let cur_from = bo_paths.find(elem => elem.path === from)
    if(cur_path == undefined && cur_from == undefined) {
      state.boPath = {
        'path': 'admin',
        'short': 'admin'
      }
    } else {
      if(cur_from == undefined) {
        state.boPath = {
          'path': cur_path.path,
          'short': cur_path.short
        }
      } else {
        state.boPath = {
          'path': cur_from.path,
          'short': cur_from.short
        }
      }
    }
  },
  SET_BO_BASE(state, from) {
    let bo_paths = [
      {path: 'admin', short: 'admin'},
      {path: 'prestataire', short: 'pre'},
      {path: 'assistance', short: 'ass'},
      {path: 'assurance', short: 'assu'}
    ]
    let cur_from = bo_paths.find(elem => elem.path === from)
    if(cur_from == undefined) {
      state.boPath = {
        'path': 'admin',
        'short': 'admin'
      }
    } else {
      state.boPath = {
        'path': cur_from.path,
        'short': cur_from.short
      }
    }
  }
}

export const actions = {
  setBoPath({commit}, from) {
    commit('SET_BO_PATH', from)
  },
  setBoBase({commit}, from) {
    commit('SET_BO_BASE', from)
  }
}
