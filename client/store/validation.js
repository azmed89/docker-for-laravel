export const state = () => ({
  listErrors: {}
})

export const getters = {
  errors(state) {
    return state.listErrors;
  }
}

export const mutations = {
  SET_VALIDATION_ERRORS(state, errors) {
    state.listErrors = errors;
  }
}

export const actions = {
  setErrors({commit}, errors) {
    commit('SET_VALIDATION_ERRORS', errors)
  },
  clearErrors({commit}) {
    commit('SET_VALIDATION_ERRORS', {})
  }
}
