-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : jeu. 31 déc. 2020 à 12:29
-- Version du serveur :  10.4.13-MariaDB
-- Version de PHP : 7.4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `econsta`
--

--
-- Déchargement des données de la table `assistances`
--

INSERT INTO `assistances` (`id_assistance`, `nom`, `email`, `telephone`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Maroc Assistance Internationale', 'contact@mai.com', '05223698741', '2020-10-09 14:28:11', NULL, NULL),
(2, 'Services Assistance - SAHAM Assurance', 'contact@gmail.com', '0522631478', '2020-10-09 14:29:20', NULL, NULL),
(3, 'Gislason Group', 'oswald03@pfannerstill.com', '+9533661071191', '2020-12-16 13:28:17', '2020-12-16 13:28:17', NULL),
(4, 'Raynor-Ruecker', 'darius.barrows@kautzer.org', '+3706354088236', '2020-12-16 13:28:17', '2020-12-16 13:28:17', NULL),
(5, 'Nitzsche-Krajcik', 'volkman.monica@fahey.info', '+7354313861602', '2020-12-16 13:28:17', '2020-12-16 13:28:17', NULL),
(6, 'Leuschke LLC', 'kozey.ernesto@hand.info', '+8664778891079', '2020-12-16 13:28:17', '2020-12-16 13:28:17', NULL),
(7, 'Keebler PLC', 'margarete49@ebert.biz', '+8673034420771', '2020-12-16 13:28:17', '2020-12-16 13:28:17', NULL),
(8, 'Bartoletti, Bahringer and Fisher', 'prunolfsdottir@borer.com', '+3135386405762', '2020-12-16 13:28:17', '2020-12-16 13:28:17', NULL),
(9, 'Macejkovic-Smitham', 'wilbert.medhurst@schneider.com', '+8433788539921', '2020-12-16 13:28:17', '2020-12-16 13:28:17', NULL),
(10, 'Gutkowski, Kemmer and Cartwright', 'zankunding@metz.com', '+8395859049532', '2020-12-16 13:28:17', '2020-12-16 13:28:17', NULL),
(11, 'Murray-Kiehn', 'fnitzsche@will.com', '+5093215756712', '2020-12-16 13:28:17', '2020-12-16 13:28:17', NULL),
(12, 'Turcotte Inc', 'schamberger.kaia@schumm.com', '+1951061346188', '2020-12-16 13:28:17', '2020-12-16 13:28:17', NULL);

--
-- Déchargement des données de la table `assistance_prestataires`
--

INSERT INTO `assistance_prestataires` (`id_assistance`, `id_prestataire`) VALUES
(1, 1),
(1, 3),
(1, 4),
(1, 5),
(1, 6),
(2, 1),
(2, 2),
(2, 6);

--
-- Déchargement des données de la table `assurances`
--

INSERT INTO `assurances` (`id_assurance`, `nom`, `code_societe`, `email`, `created_at`, `updated_at`, `id_assistance`, `deleted_at`) VALUES
(1, 'Maroc assurance', 'SAHAM', 'contact@mcma.com', '2020-10-09 11:14:57', NULL, 1, NULL),
(2, 'Axa assurance', 'AXA', 'contact@axa.com', '2020-10-09 11:14:57', NULL, 1, NULL),
(3, 'Saham assurance', 'SA', 'saham-assurance@gmail.com', '2020-10-09 14:32:29', NULL, 2, NULL),
(10, 'Ferry and Sons', 'Feeney-Stamm', 'chris36@watsica.org', '2020-12-16 13:28:17', '2020-12-16 13:28:17', 1, NULL),
(11, 'Kozey, Wintheiser and Lind', 'Trantow, Dickens and Padberg', 'hauck.roselyn@cummings.net', '2020-12-16 13:28:17', '2020-12-16 13:28:17', 4, NULL),
(12, 'Osinski LLC', 'Nitzsche Ltd', 'hailee68@hyatt.com', '2020-12-16 13:28:17', '2020-12-16 13:28:17', 1, NULL),
(13, 'Fahey, O\'Keefe and Ziemann', 'Kassulke-Barton', 'pansy.oconner@franecki.com', '2020-12-16 13:28:17', '2020-12-16 13:28:17', 6, NULL),
(14, 'Gottlieb, Goyette and Hessel', 'Mills-Gutkowski', 'uziemann@ratke.net', '2020-12-16 13:28:17', '2020-12-16 13:28:17', 7, NULL),
(15, 'Adams, Swift and O\'Connell', 'Hessel-Dietrich', 'tierra31@cormier.com', '2020-12-16 13:28:17', '2020-12-16 13:28:17', 2, NULL),
(16, 'Homenick, Borer and Mosciski', 'Stroman-Schroeder', 'elisha67@beatty.com', '2020-12-16 13:28:17', '2020-12-16 13:28:17', 9, NULL),
(17, 'Hintz, Yundt and Douglas', 'Kuhlman-Metz', 'labadie.manuela@langworth.info', '2020-12-16 13:28:17', '2020-12-16 13:28:17', 10, NULL),
(18, 'Zboncak-Reichert', 'Simonis LLC', 'zion.nolan@vonrueden.info', '2020-12-16 13:28:17', '2020-12-16 13:28:17', 1, NULL),
(19, 'Brakus, Prosacco and Wilkinson', 'Johns, Rutherford and Skiles', 'warmstrong@gulgowski.biz', '2020-12-16 13:28:17', '2020-12-16 13:28:17', 12, NULL),
(20, 'Grant Ltd', 'Quigley, Cremin and Bauch', 'janet.koelpin@hammes.info', '2020-12-16 13:44:48', '2020-12-16 13:44:48', 5, NULL),
(21, 'Davis, Kling and Bernhard', 'Wisozk-Douglas', 'lkris@durgan.com', '2020-12-16 13:45:20', '2020-12-16 13:45:20', 5, NULL),
(22, 'Halvorson, O\'Kon and Kautzer', 'Mraz LLC', 'richard.metz@smith.info', '2020-12-16 13:45:20', '2020-12-16 13:45:20', 4, NULL),
(23, 'Lowe, Dibbert and Lubowitz', 'Schiller Inc', 'devan69@schultz.biz', '2020-12-16 13:45:20', '2020-12-16 13:45:20', 9, NULL),
(24, 'Kris-Rolfson', 'Wilderman and Sons', 'qhilpert@schmidt.org', '2020-12-16 13:45:20', '2020-12-16 13:45:20', 2, NULL),
(25, 'Nolan and Sons', 'Sanford Inc', 'leanna71@runolfsdottir.com', '2020-12-16 13:45:20', '2020-12-16 13:45:20', 7, NULL),
(26, 'Ankunding, McLaughlin and Murphy', 'Kiehn-Johns', 'danielle28@anderson.com', '2020-12-16 13:45:20', '2020-12-16 13:45:20', 8, NULL),
(27, 'Streich, Mayert and Schaden', 'Bruen, Lueilwitz and Mante', 'rempel.mekhi@rippin.com', '2020-12-16 13:45:20', '2020-12-16 13:45:20', 5, NULL),
(28, 'Ziemann LLC', 'Stehr, Green and McClure', 'elisa.mills@schaden.com', '2020-12-16 13:45:20', '2020-12-16 13:45:20', 3, NULL),
(29, 'Abshire-Zieme', 'McClure, Hermann and Bogisich', 'isawayn@bauch.com', '2020-12-16 13:45:20', '2020-12-16 13:45:20', 4, NULL),
(30, 'Paucek and Sons', 'Buckridge Inc', 'hilton.wilkinson@schamberger.org', '2020-12-16 13:45:20', '2020-12-16 13:45:20', 10, NULL);

--
-- Déchargement des données de la table `assures`
--

INSERT INTO `assures` (`id_assure`, `nom`, `prenom`, `adresse`, `n_attestation`, `n_police`, `ste_assurance`, `validite_attestation`, `agence`, `created_at`, `updated_at`, `id_constat`, `attes_valide_du`, `attes_valide_au`, `email`, `telephone`, `type_usage`) VALUES
(1, 'AKHDAR', 'ABDERRAHIM', '143,hay elwahda1', '1256', '698741', 'SAHAM', 1, 'SAHAM', '2020-10-09 16:10:43', '2020-12-09 17:04:28', 1, '2020-12-09', '2020-12-29', NULL, '0669926317', 'PARTICULIER'),
(2, 'EL HASSANI', 'AYOUB', 'bd amgala 46 hay Yasmina, Casablanca 20470', '051285497', '079168531', 'SAHAM', 1, 'SAHAM ASSURANCE', '2020-10-21 17:48:19', '2020-12-09 17:56:13', 1, '2020-12-18', '2020-12-19', NULL, '0123654789', 'PARTICULIER'),
(60, 'Mr. Lindsey Beer II', 'Henriette Kuhic DVM', '28179 Prosacco Point Apt. 062\nLeannonburgh, IL 31567', '5928', '55511', 'AXA', 0, 'Howe PLC', '2020-12-16 14:36:42', '2020-12-16 14:36:42', 2, '2002-03-09', '1990-11-16', 'huel.shea@example.com', '0321456987', 'PARTICULIER'),
(61, 'Dr. Angelica Littel', 'Peggie Schinner', '757 Mohr Court Apt. 729\nHansenbury, RI 11686', '52728', '32943', 'SAHAM', 0, 'Bins-Lind', '2020-12-16 14:36:42', '2020-12-16 14:36:42', 2, '1997-12-25', '1996-05-27', 'bennett.thompson@example.org', '0412365897', 'TAXI'),
(62, 'Coleman Runte', 'Ernestina Champlin', '51195 Charles Locks\nManteberg, IN 89424', '48409', '65919', 'SAHAM', 0, 'Haag Inc', '2020-12-16 14:36:49', '2020-12-16 14:36:49', 3, '1994-04-20', '1971-08-20', 'gibson.keeley@example.com', '0632145897', 'TAXI'),
(63, 'Miss Madilyn Skiles', 'Mr. Royce Morissette DDS', '849 Earnest Groves\nWest Mason, OK 81772', '45789', '21874', 'SAHAM', 0, 'Heidenreich, Donnelly and Sawayn', '2020-12-16 14:36:49', '2020-12-16 14:36:49', 3, '1988-11-16', '1977-12-25', 'ookon@example.org', '0632985471', 'TOURISTIQUE'),
(64, 'Arnoldo Gleason III', 'River Tromp I', '22721 Spinka Hollow Apt. 319\nMaximilliaview, VA 13297-9889', '6592', '95209', 'SAHAM', 1, 'Goyette-Heathcote', '2020-12-16 14:36:57', '2020-12-16 14:36:57', 4, '1971-05-12', '1971-02-16', 'raphaelle.mohr@example.com', '0693214785', 'AGENCE_LOCATION'),
(65, 'Evalyn Glover III', 'Kayleigh Kihn', '70401 Prosacco Crescent Apt. 488\nLake Catalina, UT 26018-1616', '59754', '20420', 'SAHAM', 0, 'Shanahan and Sons', '2020-12-16 14:36:57', '2020-12-16 14:36:57', 4, '1977-11-16', '2002-06-12', 'ktremblay@example.com', '0623145897', 'TOURISTIQUE'),
(66, 'Mr. Juwan Walter', 'Miss Beaulah Rohan', '515 Malika Village Apt. 813\nSouth Bobbystad, TN 10151-6762', '41521', '61971', 'SAHAM', 1, 'Pollich, Kling and Cassin', '2020-12-16 14:37:03', '2020-12-16 14:37:03', 5, '1983-08-28', '1993-09-26', 'ujacobs@example.org', '0563214789', 'TOURISTIQUE'),
(67, 'Napoleon Langworth', 'Rhoda Keeling', '8130 Chaim Parkway\nEast Eliezerside, IL 84543-7027', '92955', '97752', 'SAHAM', 0, 'Tremblay-Jenkins', '2020-12-16 14:37:03', '2020-12-16 14:37:03', 5, '2007-02-02', '1974-08-16', 'alanna.rau@example.org', '0741236985', 'PARTICULIER'),
(68, 'Katarina Stokes', 'Missouri Stiedemann', '4046 Jamel Drive\nPort Chadd, SD 19929-0200', '26337', '10494', 'SAHAM', 1, 'Schmeler, Ernser and Herman', '2020-12-16 14:37:09', '2020-12-16 14:37:09', 6, '2020-07-27', '1992-04-19', 'littel.aracely@example.com', '0741326598', 'PARTICULIER'),
(69, 'Dr. Maverick Hudson', 'Dr. Domenic Beer', '689 Lucinda Parks\nEast Terrell, MD 25108-6521', '30631', '83385', 'SAHAM', 0, 'Jacobi-Bechtelar', '2020-12-16 14:37:09', '2020-12-16 14:37:09', 6, '1973-03-26', '1982-10-31', 'khalid24@example.com', '0632145897', 'TAXI'),
(70, 'Miss Marion Heidenreich', 'Prof. Keshawn Hegmann Sr.', '1259 Anderson Walks\nJannieberg, AR 05295', '93931', '8673', 'SAHAM', 1, 'Renner-Daniel', '2020-12-16 14:37:16', '2020-12-16 14:37:16', 7, '2008-08-10', '1973-11-17', 'ashleigh65@example.net', '0632985471', 'AGENCE_LOCATION'),
(71, 'Harry Weissnat MD', 'Jillian Russel', '642 Connelly Field Apt. 493\nNorth Nannieton, KS 05582', '73209', '33163', 'SAHAM', 1, 'Johnston-Crist', '2020-12-16 14:37:16', '2020-12-16 14:37:16', 7, '2008-09-08', '2016-08-14', 'upurdy@example.net', '0632145897', 'PARTICULIER'),
(72, 'Karianne Rosenbaum', 'Dr. Ed Mitchell Jr.', '5982 Laurine Mall Suite 802\nErdmanberg, KS 30440', '24073', '25251', 'SAHAM', 0, 'Schmeler-Dietrich', '2020-12-16 14:37:22', '2020-12-16 14:37:22', 8, '2011-09-10', '1981-09-21', 'whitney.bernier@example.com', '0741258963', 'PARTICULIER'),
(73, 'Miss Maggie Shields II', 'Joy Collier', '771 Turner Ranch\nSouth Guidoton, WI 58937', '16874', '84307', 'SAHAM', 0, 'Stiedemann-Koch', '2020-12-16 14:37:22', '2020-12-16 14:37:22', 8, '1992-09-16', '1972-03-28', 'chauck@example.org', '0741236589', 'TAXI'),
(74, 'Lavina Farrell MD', 'Makayla Daugherty', '848 Conn Ferry Suite 386\nIsabellastad, MO 01904', '26018', '76290', 'SAHAM', 1, 'Yundt, Oberbrunner and Quitzon', '2020-12-16 14:40:03', '2020-12-16 14:40:03', 9, '1985-08-19', '1994-11-06', 'apurdy@example.org', '0693258147', 'AGENCE_LOCATION'),
(75, 'Cody Mertz', 'Mariela Sporer', '9267 Labadie Canyon Apt. 471\nAndersontown, TX 25684-0402', '33031', '89633', 'SAHAM', 1, 'Mante, Leannon and Sawayn', '2020-12-16 14:40:03', '2020-12-16 14:40:03', 9, '1992-12-26', '1992-08-16', 'hane.vivien@example.org', '0741256398', 'AGENCE_LOCATION'),
(76, 'Diamond Kris V', 'Emma Wolff', '9610 Keaton Knolls\nKierafort, MT 26596-1600', '6510', '85049', 'AXA', 1, 'Witting, Gleason and Huels', '2020-12-16 14:40:10', '2020-12-16 14:40:10', 10, '1975-04-04', '1977-05-05', 'wisozk.darion@example.com', '0789654123', 'AGENCE_LOCATION'),
(77, 'Dr. Johnpaul Botsford DVM', 'Maryse Wisoky', '571 Cathrine Trace\nPort Kaci, IA 37513', '90752', '85662', 'SAHAM', 1, 'Quitzon, Grady and Conn', '2020-12-16 14:40:10', '2020-12-16 14:40:10', 10, '2005-12-13', '2002-01-09', 'klein.rosalyn@example.org', '0789632541', 'PARTICULIER'),
(78, 'Dr. Cheyenne Lynch Jr.', 'Jewell Sauer DDS', '855 Hauck Isle Apt. 587\nLake Hailieberg, IL 61063', '3971', '58879', 'SAHAM', 1, 'Bogan-Shields', '2020-12-16 14:40:16', '2020-12-16 14:40:16', 11, '2011-02-12', '2020-02-11', 'reece.stiedemann@example.net', '0693251478', 'AGENCE_LOCATION'),
(79, 'Robert Crooks', 'Jada Gerhold', '58073 Schoen Divide Apt. 963\nMarkusmouth, MD 90273-7793', '388', '25024', 'SAHAM', 1, 'Reichel-Schuppe', '2020-12-16 14:40:16', '2020-12-16 14:40:16', 11, '2006-04-17', '2010-08-15', 'weimann.daren@example.net', '0745632189', 'PARTICULIER'),
(81, 'DHYDFU', 'CJJXJX', 'hfhwhw', '6556', '4568', 'fhfu', NULL, 'xhhxxh', '2020-12-16 16:45:22', '2020-12-16 16:45:22', 82, '2020-12-16', '2020-12-16', NULL, NULL, 'PARTICULIER'),
(82, 'YFUYF', 'FHFH', 'dyfyfyjfuf', '6886', '898668', 'cjfjfu', NULL, 'xbxjjx', '2020-12-16 16:45:40', '2020-12-16 16:45:40', 82, '2020-12-16', '2020-12-16', NULL, NULL, 'PARTICULIER'),
(83, 'BDJDJ', 'FBDB', 'djddj', '5949', '8989', 'dbdb', NULL, 'bdbbd', '2020-12-16 17:47:05', '2020-12-16 17:47:05', 83, '2020-12-16', '2020-12-16', NULL, NULL, 'PARTICULIER'),
(84, 'VDVDB', 'BFBD', 'bdbdb', '9898', '5949', 'bejd', NULL, 'hdhd', '2020-12-16 17:47:21', '2020-12-16 17:47:21', 83, '2020-12-16', '2020-12-16', NULL, NULL, 'PARTICULIER'),
(85, 'EL HASSANI', 'AYOUB', 'Adresse de ayoub', '467918', '046153', 'SAHAM', NULL, 'Saham ASSURANCE', '2020-12-17 12:34:34', '2020-12-17 12:34:34', 84, '2020-12-17', '2020-12-17', NULL, NULL, 'PARTICULIER'),
(86, 'AZE', 'TYU', 'rue 1', '123', '123', 'SAHAM', NULL, 'abc', '2020-12-17 12:54:36', '2020-12-17 12:54:36', 86, '2020-12-31', '2020-12-31', NULL, NULL, 'PARTICULIER'),
(87, 'AZ', 'AB', 'rue12', '123', '456', 'SAHAM', NULL, 'agent', '2020-12-17 12:55:05', '2020-12-17 12:55:05', 86, '2020-12-10', '2020-12-31', NULL, NULL, 'PARTICULIER'),
(88, 'WVVWS', 'FBDBD', 'fndnd', '5959', '89794', 'fbdbd', NULL, 'csvsd', '2020-12-21 09:39:10', '2020-12-21 09:39:10', 87, '2020-12-21', '2020-12-21', NULL, NULL, 'PARTICULIER'),
(89, 'DVVD', 'DJDJD', 'djdjdj', '95944', '9794', 'dbdbe', NULL, 'vdvd', '2020-12-21 09:39:28', '2020-12-21 09:39:28', 87, '2020-12-21', '2020-12-21', NULL, NULL, 'PARTICULIER'),
(90, 'VDBDB', 'BDBDB', 'jfjfjf', '9595', '8959', 'bfbd', NULL, 'vdbd', '2020-12-21 13:43:03', '2020-12-21 13:43:03', 89, '2020-12-21', '2020-12-21', NULL, NULL, 'PARTICULIER'),
(91, 'HDHDH', 'BDBEBE', 'jfjfjr', '9595', '9595', 'dvdvd', NULL, 'vdbd', '2020-12-21 13:43:20', '2020-12-21 13:43:20', 89, '2020-12-21', '2020-12-21', NULL, NULL, 'PARTICULIER'),
(92, 'CGFE', 'HHG', 'ggdeh', '888', '778', 'ggd', NULL, 'ffy', '2020-12-21 15:54:14', '2020-12-21 15:54:14', 90, '2020-12-21', '2020-12-21', NULL, NULL, 'PARTICULIER'),
(93, 'CSWH', 'CGSV', 'vcsfb', '888', '588', 'ccd', NULL, 'cbqy', '2020-12-21 15:54:44', '2020-12-21 15:54:44', 90, '2020-12-21', '2020-12-21', NULL, NULL, 'PARTICULIER'),
(94, 'SVS', 'DHID', 'shsj', '646', '764', 'shdj', NULL, 'cshd', '2020-12-22 10:21:47', '2020-12-22 10:21:47', 91, '2020-12-22', '2020-12-22', NULL, NULL, 'PARTICULIER'),
(95, 'HSHD', 'SHSY', 'hshs', '646', '7894', 'shsh', NULL, 'hshs', '2020-12-22 10:22:01', '2020-12-22 10:22:01', 91, '2020-12-22', '2020-12-22', NULL, NULL, 'PARTICULIER'),
(96, 'VWVD', 'BDBBD', 'jdjej', '9494', '9494', 'bdbe', NULL, 'csvd', '2020-12-22 11:28:17', '2020-12-22 11:28:17', 92, '2020-12-22', '2020-12-22', NULL, NULL, 'PARTICULIER'),
(97, 'GSG', 'HDHD', 'hdjd', '9495', '9494', 'bdbe', NULL, 'vdvd', '2020-12-22 11:28:32', '2020-12-22 11:28:32', 92, '2020-12-22', '2020-12-22', NULL, NULL, 'PARTICULIER'),
(98, 'VSHS', 'SHJS', 'susus', '7997', '897', 'sbs', NULL, 'vwvs', '2020-12-23 16:17:19', '2020-12-23 16:17:19', 93, '2020-12-23', '2020-12-23', NULL, NULL, 'PARTICULIER'),
(99, 'VSHS', 'SHSJ', 'sjsjs', '646', '7949', 'sjsj', NULL, 'bsbs', '2020-12-23 16:17:38', '2020-12-23 16:17:38', 93, '2020-12-23', '2020-12-23', NULL, NULL, 'PARTICULIER'),
(100, 'G  GVH', 'CT T', 'g g', '665', '0505', 'vu y', NULL, 'vvyvy', '2020-12-24 09:55:45', '2020-12-24 09:55:45', 94, '2020-12-24', '2020-12-24', NULL, NULL, 'PARTICULIER'),
(101, 'H H', 'CYCY', 'vu hv', '6606', '60060', 'vu y', NULL, 'vyvjjv', '2020-12-24 09:56:04', '2020-12-24 09:56:04', 94, '2020-12-24', '2020-12-24', NULL, NULL, 'PARTICULIER');

--
-- Déchargement des données de la table `conducteurs`
--

INSERT INTO `conducteurs` (`id_conducteur`, `nom`, `prenom`, `adresse`, `num_permis`, `date_delivrance`, `prefecture_delivrance`, `signature`, `delai_validite`, `created_at`, `updated_at`, `id_constat`, `type_permis`) VALUES
(1, 'AKHDARE', 'ABDERRAHIM', 'Adresse de Abderrahim', '154780', '2020-11-05', 'RABAT', '{\"filepath\":\"mNmhwDj0K3W1ZwDvMUvIzownI.jpg\",\"name\":\"sign.png\"}', '2020-11-29', '2020-10-23 09:18:48', '2020-12-16 16:54:17', 1, 'C'),
(2, 'EL HASSANI', 'AYOUB', 'Adresse de Ayoub', '154780', '2020-11-25', 'RABAT', '{\"filepath\":\"7vEXDQeE4r6AjlnJ29T9bEXkG.jpg\",\"name\":\"sign_b.png\"}', '2020-11-29', '2020-10-23 09:18:48', '2020-12-16 17:18:13', 1, 'D'),
(19, 'Hailie Beer', 'Meta Beahan Sr.', '868 Adelia Mountains Suite 202\nMcGlynnshire, NC 00169-7613', '21023', '1973-10-13', 'Fes', '{\"filepath\":\"TEBd1TBdJaTxDIvah5mYqhonK.jpg\",\"name\":\"sign.png\"}', '1998-12-07', '2020-12-16 14:54:12', '2020-12-16 17:18:22', 2, 'B'),
(20, 'Therese Heaney PhD', 'Mrs. Alexa Hudson I', '406 Hessel Common Apt. 139\nOlsonhaven, AZ 49066-0695', '79737', '1971-08-12', 'Meknes', '{\"filepath\":\"WDgqu68N8bhshOlD8H4KfWfG6.jpg\",\"name\":\"sign.png\"}', '1984-09-24', '2020-12-16 14:54:12', '2020-12-16 17:18:26', 2, 'D'),
(21, 'Luther Lueilwitz', 'Dr. Elias Bode I', '59922 Shields Summit\nNew Alvisport, MI 59842', '18648', '1972-09-11', 'Casablanca', '{\"filepath\":\"ZTfl8ZtshIcNb8w20O30GLoUL.jpg\",\"name\":\"sign.png\"}', '2009-07-02', '2020-12-16 14:54:35', '2020-12-16 17:18:38', 3, 'D'),
(22, 'Jon Kirlin', 'Belle Gottlieb', '6064 Vandervort Drives\nNorth Kenyatta, TN 45342-6738', '47485', '2017-07-28', 'Elhoceima', '{\"filepath\":\"jUtVplWw5ol0FeI2BjQ8079dX.jpg\",\"name\":\"sign.png\"}', '1983-10-09', '2020-12-16 14:54:35', '2020-12-16 17:18:42', 3, 'D'),
(23, 'Lelia Adams', 'Percy Weber IV', '88179 Raynor Well Apt. 752\nSouth Maiyamouth, FL 79518-8778', '54670', '2011-04-20', 'Meknes', '{\"filepath\":\"oQewgCjC7zYdYybLzsvrur6cW.jpg\",\"name\":\"sign.png\"}', '1986-12-31', '2020-12-16 14:54:43', '2020-12-16 17:18:56', 4, 'C'),
(24, 'Miss Cassie Willms', 'Bartholome Stiedemann', '884 Moises Dam Suite 105\nHipolitofort, VA 85902-8506', '74256', '2003-03-13', 'Fes', '{\"filepath\":\"TB9bmJH00xX3kfpdMOxDJcZFQ.jpg\",\"name\":\"sign.png\"}', '2019-08-24', '2020-12-16 14:54:43', '2020-12-16 17:19:01', 4, 'F'),
(25, 'Vaughn Wolf', 'Shakira Bartell', '458 Milford Mews\nNew Kaylee, WY 67535', '16569', '1984-10-26', 'Fes', '{\"filepath\":\"lmovz5bkqebgqk6jjEi5Mbiix.jpg\",\"name\":\"sign.png\"}', '1991-02-04', '2020-12-16 14:54:49', '2020-12-16 17:19:05', 5, 'F'),
(26, 'Norma Sauer', 'Mrs. Everette Rice', '446 VonRueden Stravenue Suite 642\nHayleyberg, IN 84822-2327', '79678', '2006-06-26', 'Marrakech', '{\"filepath\":\"1ulLohSJvVpib6TxS1t1C879X.jpg\",\"name\":\"sign.png\"}', '2012-02-07', '2020-12-16 14:54:49', '2020-12-16 17:19:08', 5, 'E'),
(27, 'Dr. Manuela Fahey PhD', 'Prof. Aric Heathcote IV', '319 Rempel Brook Suite 041\nWest Archville, WY 97123', '42992', '1997-02-17', 'Rabat', '{\"filepath\":\"XLLeYVj6jiMZ2vLw6lxD1EUE3.jpg\",\"name\":\"sign.png\"}', '1971-09-27', '2020-12-16 14:54:58', '2020-12-16 17:19:14', 6, 'E'),
(28, 'Tristian Hartmann MD', 'Marjorie Kris MD', '524 Lavon Isle Suite 917\nPort Shanna, AR 11925-8070', '56286', '2018-03-15', 'Wajda', '{\"filepath\":\"FD3BZ4NMTCXgJImlBnq9mia9e.jpg\",\"name\":\"sign.png\"}', '1980-01-07', '2020-12-16 14:54:58', '2020-12-16 17:19:17', 6, 'D'),
(29, 'Elisha Altenwerth III', 'Kaylin Kuhn', '8815 Kihn River\nNew Samaraborough, IN 35042-1791', '50793', '2013-01-10', 'Rabat', '{\"filepath\":\"tQvLsPWkeD8cWKjoO4MkKSe2u.jpg\",\"name\":\"sign.png\"}', '2002-12-16', '2020-12-16 14:55:06', '2020-12-16 17:19:20', 7, 'B'),
(30, 'Mr. Immanuel Boehm', 'Krystel Reichert II', '412 Jadon Glens\nSouth Sofia, IA 26265-9449', '13048', '1995-08-18', 'Elhoceima', '{\"filepath\":\"HraEttyNNCMFvCDa8inXJ26BC.jpg\",\"name\":\"sign.png\"}', '1997-02-27', '2020-12-16 14:55:06', '2020-12-16 17:19:24', 7, 'A1'),
(31, 'Miss Amanda Franecki IV', 'Daphney Goldner', '2343 Gleichner Way Suite 477\nGarretchester, DC 57212', '2594', '2018-07-28', 'Fes', '{\"filepath\":\"0xYNbpn8CLSIeHM1QTUstYupy.jpg\",\"name\":\"sign.png\"}', '2003-07-11', '2020-12-16 14:55:12', '2020-12-16 17:19:37', 8, 'F'),
(32, 'Dr. Israel Russel I', 'Mason Balistreri', '3660 Tommie Extensions\nSouth Luella, OK 52227-2676', '95892', '1976-05-12', 'Elhoceima', '{\"filepath\":\"ZyOPHMP7T4GmmJlBoioMqXJS9.jpg\",\"name\":\"sign.png\"}', '1985-12-13', '2020-12-16 14:55:12', '2020-12-16 17:19:40', 8, 'A1'),
(33, 'Mr. Stephon Hirthe', 'Burnice Durgan', '12086 Stanton Turnpike\nJadynstad, ND 27737', '21892', '1998-07-28', 'Meknes', '{\"filepath\":\"LRMt7ctwwOAAnIr91x8rGhV7N.jpg\",\"name\":\"sign.png\"}', '2017-06-04', '2020-12-16 14:55:25', '2020-12-16 17:19:45', 9, 'B'),
(34, 'Annabelle Heidenreich', 'Davon Welch', '27339 Considine Forge\nEleanorebury, AL 94449', '32071', '2008-11-17', 'Wajda', '{\"filepath\":\"GBBccpYyQoDUkriQDhKN9OLNO.jpg\",\"name\":\"sign.png\"}', '2010-01-28', '2020-12-16 14:55:25', '2020-12-16 17:19:49', 9, 'C'),
(35, 'Ocie Mante', 'Yvonne Mosciski', '5691 Rippin Glens Suite 366\nNew Kianstad, NM 45947', '64268', '1997-12-09', 'Casablanca', '{\"filepath\":\"Lwx8IWMjWN0OFkLp7nmG3PTvN.jpg\",\"name\":\"sign.png\"}', '2010-12-24', '2020-12-16 14:55:40', '2020-12-16 17:19:52', 10, 'D'),
(36, 'Vince Witting', 'Prof. Lawson Abshire', '9330 Colt Walk\nDominicview, UT 98971-1923', '15633', '1998-05-04', 'Marrakech', '{\"filepath\":\"WkC3M7SCoB3gzmpDn3EWkCODh.jpg\",\"name\":\"sign.png\"}', '1999-12-15', '2020-12-16 14:55:40', '2020-12-16 17:19:56', 10, 'A1'),
(37, 'Luna Leuschke', 'Rhoda Zulauf', '376 Bryon Mills\nSouth Paolo, ME 50420-2302', '93427', '1970-07-17', 'Elhoceima', '{\"filepath\":\"wGcJsXfH17lN1Z69zQXjwRt5D.jpg\",\"name\":\"sign.png\"}', '2003-12-31', '2020-12-16 14:55:47', '2020-12-16 17:19:59', 11, 'A1'),
(38, 'Alycia Cartwright V', 'Dr. Christian Ferry', '740 Bogisich Hill Apt. 329\nNew Jamarmouth, UT 93153', '16324', '1980-04-01', 'Agadir', '{\"filepath\":\"x2PTDS4lm6rEHEx3tsFnkAaIX.jpg\",\"name\":\"sign.png\"}', '2003-10-24', '2020-12-16 14:55:47', '2020-12-16 17:20:03', 11, 'F'),
(39, 'FYFYFY', 'XVVX', 'fyfuti', '154780', '2020-12-16', 'UFFUFY', '{\"filepath\":\"3EVNc25fNYfxxQEJaHV2LDkY3.jpg\",\"name\":\"sign.png\"}', '2020-12-16', '2020-12-16 16:45:55', '2020-12-16 17:20:07', 82, 'D'),
(40, 'GDHFYF', 'CHHCUG', 'ffigu', '154780', '2020-12-16', 'GJJGJGXJ', '{\"filepath\":\"gQ4Srr5YAHV4ObybXqQ7yFoee.jpg\",\"name\":\"sign.png\"}', '2020-12-16', '2020-12-16 16:46:08', '2020-12-16 17:20:11', 82, 'D'),
(41, 'BDBD', 'XBFB', 'bfbd', '154780', '2020-12-16', 'BDBD', NULL, '2020-12-16', '2020-12-16 17:47:36', '2020-12-16 17:47:36', 83, 'C'),
(42, 'VDBD', 'BDBD', 'bdbdbd', '154780', '2020-12-16', 'HDHDB', NULL, '2020-12-16', '2020-12-16 17:47:52', '2020-12-16 17:47:52', 83, 'D'),
(43, 'AZ', 'EV', 'rue 1', '154780', '2020-12-02', 'AZ', NULL, '2020-12-31', '2020-12-17 13:05:06', '2020-12-17 13:05:06', 86, 'A'),
(44, 'AZE', 'AZE', 'rue 1', '154780', '2020-12-01', 'AZE', NULL, '2020-12-31', '2020-12-17 13:05:57', '2020-12-17 13:05:57', 86, 'B'),
(45, 'HDJD', 'DJDJD', 'rjeje', '154780', '2020-12-21', 'VWBDJD', NULL, '2020-12-21', '2020-12-21 09:39:46', '2020-12-21 09:39:46', 87, 'C'),
(46, 'DJD', 'DJDJD', 'jddjd', '154780', '2020-12-21', 'VWBD', NULL, '2020-12-21', '2020-12-21 09:40:01', '2020-12-21 09:40:01', 87, 'A1'),
(47, 'DBBD', 'DBBD', 'bdbdbr', '154780', '2020-12-21', 'BDBBD', NULL, '2020-12-21', '2020-12-21 13:43:37', '2020-12-21 13:43:37', 89, 'C'),
(48, 'JDJFJ', 'BFBF', 'jfjfbr', '154780', '2020-12-21', 'JDJF', NULL, '2020-12-21', '2020-12-21 13:43:51', '2020-12-21 13:43:51', 89, 'D'),
(49, 'JAKA', 'JDJEJE', 'jejebe', '154780', '2020-12-21', 'VDBDBD', NULL, '2020-12-21', '2020-12-21 16:03:44', '2020-12-21 16:04:47', 90, 'D'),
(50, 'HDHD', 'JDJF', 'jfjfjf', '154780', '2020-12-21', 'HDJDJ', NULL, '2020-12-21', '2020-12-21 16:07:21', '2020-12-21 16:07:21', 90, 'D'),
(51, 'HDHDH', 'FJFJF', 'jdjdj', '154780', '2020-12-21', 'HDJDI', NULL, '2020-12-21', '2020-12-21 16:07:43', '2020-12-21 16:07:43', 90, 'A'),
(52, 'VVSD', 'SVSH', 'shhs', '154780', '2020-12-22', 'SHJS', NULL, '2020-12-22', '2020-12-22 10:22:13', '2020-12-22 10:22:13', 91, 'A1'),
(53, 'HSHS', 'SHSH', 'shsus', '154780', '2020-12-22', 'HSJS', NULL, '2020-12-22', '2020-12-22 10:22:25', '2020-12-22 10:22:25', 91, 'D'),
(54, 'CHD', 'HDHE', 'bebe', '154780', '2020-12-22', 'HDHD', NULL, '2020-12-22', '2020-12-22 11:28:46', '2020-12-22 11:28:46', 92, 'C'),
(55, 'VDV', 'VDBD', 'bdbd', '154780', '2020-12-22', 'BBDD', NULL, '2020-12-22', '2020-12-22 11:28:56', '2020-12-22 11:28:56', 92, 'A1'),
(56, 'VSJS', 'SVBS', 'bsjsj', '154780', '2020-12-23', 'BWBS', NULL, '2020-12-23', '2020-12-23 16:17:56', '2020-12-23 16:19:57', 93, 'D'),
(57, 'BDBJD', 'DJDJD', 'dvdhd', '154780', '2020-12-23', 'BDBD', NULL, '2020-12-23', '2020-12-23 16:18:13', '2020-12-23 16:19:59', 93, 'D'),
(58, 'VYVYYV', 'VH Y', 'h y', '154780', '2020-12-24', 'BUBUBU', NULL, '2020-12-24', '2020-12-24 09:56:27', '2020-12-24 09:56:27', 94, 'E'),
(59, 'YVVY', 'VYVY', 'yccy', '154780', '2020-12-24', 'VYUVU', NULL, '2020-12-24', '2020-12-24 09:56:43', '2020-12-24 09:56:43', 94, 'B');

--
-- Déchargement des données de la table `constateurs`
--

INSERT INTO `constateurs` (`id_constateur`, `email`, `password`, `nom`, `prenom`, `adresse`, `fonction`, `telephone`, `num_agent`, `acces`, `avatar`, `identifiant`, `created_at`, `updated_at`, `id_prestataire`) VALUES
(1, 'azmed@gmail.com', '$2y$10$XfLPbja8/fxDc/jQS5TRHOOZl4Z01acY6r9gWuFMMZcNAF0jiNbXu', 'AZOUZ', 'Med', 'Adresse', 'SUPERADMIN', '623659874', '1256', 'acces', 'FXVWICpmwDEkLuuiw7qQd53OV.jpg', '56923', '2020-10-09 11:27:38', '2020-12-10 13:31:23', 1),
(3, 'ahmed@gmail.com', '$2y$10$q5OdL1cGOuRXSMCS8aN4O.YBJdvcPMn.sOhSg9cMzt3ldSOaXUF92', 'ELAAROUSI', 'Ahmed', 'Adresse', 'CONSTATEUR', '632145789', '1256', 'acces', 'jHds1YxlQcAhPWut6i3YDwTpw.jpg', '1253', '2020-10-09 11:27:38', '2020-12-10 13:34:52', 1),
(6, 'azouz.mohammed@it.devcorp.fr', '$2y$10$U5b5qxzM7Plyxy32nUUEX.QT52byb5Mfk1qWrImF.FKL.WTca7kLm', 'AZOUZ', 'Mohammed', 'Roudani', 'SUPERADMIN', '669926317', '321456987', NULL, 'XYmw6L3NiWpr8LX58CyOGa5BU.jpg', '2365', '2020-11-20 08:37:59', '2020-11-20 08:37:59', 2),
(8, 'abderrahim.akhdar@it.devcorp.fr', '$2y$10$bw0jbj9IUJmpwmyrx6p7eu.DV27TBta5weu4woRrZGYBWZgh8ioge', 'AKHDAR', 'Abderrahim', 'Maarif', 'SUPERADMIN', '321456987', '632145897', NULL, '1QryuAgcbykN1RmIvPFBab70W.jpg', '9863', '2020-11-20 09:02:32', '2020-11-20 09:02:32', 5),
(9, 'y.elorfi@devcorp.fr', '$2y$10$XfLPbja8/fxDc/jQS5TRHOOZl4Z01acY6r9gWuFMMZcNAF0jiNbXu', 'Elorfi', 'Yassine', '265 Hay hassani', 'CONSTATEUR', '0669958741', '0522631478', NULL, NULL, '5632', '2020-12-04 10:27:04', '2020-12-04 10:27:04', 3),
(10, 'a.benjelloun@devcorp.fr', '$2y$10$XfLPbja8/fxDc/jQS5TRHOOZl4Z01acY6r9gWuFMMZcNAF0jiNbXu', 'Benjelloun', 'Abdessalam', '256 Beauséjour Maarif', 'CONSTATEUR', '0522631478', '0522314786', NULL, NULL, '5263', '2020-12-04 10:34:18', NULL, 4),
(11, 'elhoucine.labied@it.devcorp.fr', '$2y$10$XfLPbja8/fxDc/jQS5TRHOOZl4Z01acY6r9gWuFMMZcNAF0jiNbXu', 'Elhoucine', 'Labied', '25 Hay Farah Derb Sultan', 'CONSTATEUR', '0522369874', '0522147896', NULL, NULL, '5214', '2020-12-04 10:39:21', NULL, 2),
(12, 'test@gmail.com', '$2y$10$9XyN28ShVyrpout3ETenUeQf1wHVkdMwaP7fSZlUsJNsfc7dmeVBy', 'Test', 'Test', 'Test', 'SUPERADMIN', '693214587', '522631489', NULL, 'Qqikx7pi2MbWJ7Z2XoAlb2CqR.jpg', '54546', '2020-12-22 15:23:28', '2020-12-22 15:23:28', 1);

--
-- Déchargement des données de la table `constats`
--

INSERT INTO `constats` (`id_constat`, `type_mission`, `code`, `num_immatriculation`, `etat`, `num_contrat`, `created_at`, `updated_at`, `liee_a`, `id_workflow`, `id_constateur`, `motif`, `perimetre`, `photos`, `id_constateur_rejoignant`, `code_assistance_rejoignante`) VALUES
(1, 'Urban', 'c12563', '23659', 'IN_PROGRESS', 1236, '2020-10-09 11:22:42', '2020-11-02 09:49:49', NULL, 1, 1, NULL, 'URBAN', NULL, NULL, NULL),
(2, 'Urban', 'c12569', '21659', 'FINISHED', 1256, '2020-10-09 11:22:42', NULL, 1, 3, 3, NULL, 'RAYON50', NULL, NULL, NULL),
(3, 'Urban', 'c45263', '25669', 'FINISHED', 1256, '2020-12-04 10:45:38', '2020-12-29 11:21:02', NULL, 4, 8, NULL, 'URBAN', NULL, NULL, NULL),
(4, 'Urban', 'c45247', '256758', 'CREATED', 1278, '2020-12-04 10:53:38', NULL, NULL, 5, 9, NULL, 'RAYON50', NULL, NULL, NULL),
(5, 'Urban', 'c45243', '25769', 'FINISHED', 1156, '2020-12-04 10:50:38', '2020-12-29 11:22:00', NULL, 6, 8, NULL, 'URBAN', NULL, NULL, NULL),
(6, 'Urban', 'c15263', '25668', 'CREATED', 1251, '2020-12-04 10:51:38', NULL, NULL, 7, 10, NULL, 'INTERURBAIN', NULL, NULL, NULL),
(7, 'Urban', 'c42263', '25669', 'CREATED', 1356, '2020-12-04 10:53:38', NULL, NULL, 8, 6, NULL, 'URBAN', NULL, NULL, NULL),
(8, 'Urban', 'c45213', '25660', 'FINISHED', 1250, '2020-12-04 10:55:38', '2020-12-29 11:23:32', NULL, 9, 9, NULL, 'RAYON50', NULL, NULL, NULL),
(9, 'Urban', 'c45063', '25664', 'CREATED', 11256, '2020-12-04 10:57:38', NULL, NULL, 10, 8, NULL, 'URBAN', NULL, NULL, NULL),
(10, 'Urban', 'c45260', '25610', 'CREATED', 1258, '2020-12-04 10:57:38', NULL, NULL, 11, 1, NULL, 'URBAN', NULL, NULL, NULL),
(11, 'Urban', 'c45214', '25669', 'CREATED', 1200, '2020-12-04 10:58:38', NULL, NULL, 12, 6, NULL, 'URBAN', NULL, NULL, NULL),
(82, NULL, 'CBCBKC', NULL, 'CREATED', NULL, '2020-12-16 16:44:45', '2020-12-16 16:44:45', NULL, 83, 1, NULL, 'URBAN', NULL, NULL, NULL),
(83, NULL, 'AM3892', NULL, 'CREATED', NULL, '2020-12-16 17:46:20', '2020-12-16 17:46:20', NULL, 84, 1, NULL, 'URBAN', NULL, NULL, NULL),
(84, NULL, 'YAO6190', NULL, 'CREATED', NULL, '2020-12-17 12:31:21', '2020-12-17 12:31:21', NULL, 85, 1, NULL, 'URBAN', NULL, NULL, NULL),
(85, NULL, '123', NULL, 'CREATED', 25631, '2020-12-17 12:39:37', '2020-12-17 12:39:37', NULL, 86, 1, NULL, 'URBAN', NULL, NULL, NULL),
(86, NULL, '123', NULL, 'CREATED', 54896, '2020-12-17 12:42:46', '2020-12-17 12:42:46', NULL, 87, 1, NULL, 'URBAN', NULL, NULL, NULL),
(87, NULL, 'HSJZ', NULL, 'CREATED', NULL, '2020-12-21 09:38:24', '2020-12-21 09:38:24', NULL, 88, 1, NULL, 'URBAN', NULL, NULL, NULL),
(88, NULL, 'BDJDJ', NULL, 'CREATED', NULL, '2020-12-21 13:40:50', '2020-12-21 13:40:50', NULL, 89, 1, NULL, 'URBAN', NULL, NULL, NULL),
(89, NULL, 'BDBDB', NULL, 'CREATED', NULL, '2020-12-21 13:42:27', '2020-12-21 13:42:27', NULL, 90, 1, NULL, 'URBAN', NULL, NULL, NULL),
(90, NULL, 'AT-6828', NULL, 'CREATED', NULL, '2020-12-21 15:31:51', '2020-12-21 15:31:51', NULL, 91, 1, NULL, 'URBAN', NULL, NULL, NULL),
(91, NULL, 'HDJD', NULL, 'CREATED', NULL, '2020-12-22 10:21:15', '2020-12-22 10:21:15', NULL, 92, 1, NULL, 'URBAN', NULL, NULL, NULL),
(92, NULL, 'CWVS', NULL, 'CREATED', NULL, '2020-12-22 11:27:41', '2020-12-22 11:27:41', NULL, 93, 1, NULL, 'URBAN', NULL, NULL, NULL),
(93, NULL, 'AHX6953', NULL, 'CREATED', NULL, '2020-12-23 16:15:53', '2020-12-23 16:15:53', NULL, 94, 1, NULL, 'URBAN', NULL, NULL, NULL),
(94, NULL, 'BDBD', NULL, 'CREATED', NULL, '2020-12-24 09:16:44', '2020-12-24 09:16:44', NULL, 95, 1, NULL, 'URBAN', NULL, NULL, NULL);

--
-- Déchargement des données de la table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_08_31_091039_create_prestataires_table', 1),
(5, '2020_10_05_112547_create_constateurs_table', 1),
(6, '2020_10_05_144226_create_constats_table', 1),
(7, '2020_10_05_150109_create_temoins_table', 1),
(8, '2020_10_05_150157_create_assures_table', 1),
(9, '2020_10_05_150221_create_conducteurs_table', 1),
(10, '2020_10_05_150736_create_assurances_table', 1),
(11, '2020_10_05_150826_create_vehicles_table', 1),
(12, '2020_10_05_150908_create_degats_table', 1),
(13, '2020_10_05_150935_create_constat_workflow_table', 1),
(14, '2020_10_05_151718_create_assistances_table', 1),
(15, '2020_10_06_150309_create_user_assistances_table', 1),
(16, '2020_10_06_151200_add_prestataire_id_to_user', 1),
(17, '2020_10_08_175631_create_assistance_prestataires_table', 1),
(18, '2020_10_08_177927_generate_foreign_keys', 1),
(19, '2020_10_09_142542_create_assistances_prestataires_table', 2),
(20, '2020_10_09_164302_add_foreign_key_id_constat_to_degats_table', 3),
(21, '2020_10_09_142542_add_foreign_key_id_assistance_to_assurances_table', 3),
(22, '2020_10_12_095506_alter_add_motif_perimetre_constats_table', 4),
(23, '2020_10_12_094311_create_user_assurances_table', 5),
(34, '2020_10_12_172842_create_user_prestataires_table', 6),
(35, '2020_10_13_093933_alter_remove_fields_in_users_table', 6),
(36, '2020_10_13_150537_add_soft_deletes_to_prestataires_table', 7),
(37, '2020_10_13_161617_add_soft_deletes_to_assurances_table', 7),
(38, '2020_10_13_170255_add_soft_deletes_to_assistances_table', 7),
(39, '2020_10_14_155629_add_multiple_column_to_constat_workflow_table', 7),
(40, '2020_10_15_145217_add_multiple_colmun_to_assures_table', 7),
(41, '2020_10_15_165900_add_type_permis_in_conducteurs_table', 7),
(48, '2020_10_16_094332_alter_columns_degats_table', 8),
(50, '2020_10_16_110958_alter_add_columns_vehicles_table', 9),
(51, '2020_10_16_111254_alter_add_column_photos_workflows_table', 9),
(53, '2020_10_19_144011_alter_add_column_type_vehicles_table', 10),
(54, '2020_05_18_152008_create_templates_table', 11),
(55, '2020_05_18_152041_create_templates_body_table', 11),
(56, '2020_10_27_173631_alter_table_add_etape_pause_workflows_table', 12),
(58, '2020_10_28_103322_alter_table_set_nullable_validite_attestation_assures_table', 13),
(59, '2020_10_28_172222_alter_table_change_delai_validite_to_date_conducteurs_table', 14),
(60, '2020_11_03_094033_alter_table_change_date_delivrance_conducteurs_table', 15),
(61, '2020_10_28_143800_add_email_and_telephone_columns_to_assures_table', 16),
(62, '2020_11_12_093915_alter_add_column_ville_workflows_table', 17),
(63, '2020_11_20_091815_alter_table_set_null_to_acces_and_avatar_constateurs_table', 18),
(64, '2020_11_04_160742_add_classe_name_to_password_resets_table', 19),
(65, '2020_11_11_151652_change_type_classe_name_password_resets_table', 19),
(66, '2020_11_27_115348_add_columns_lat_lng_workflows_table', 19),
(67, '2020_11_30_150606_add_avatar_to_user_assistances_table', 20),
(68, '2020_12_10_101359_add_columns_to_workflows_and_constats_tbales', 21),
(70, '2020_12_11_141852_add_column_avatar_to_user_assurances_table', 22),
(71, '2020_12_14_150219_add_column_degats_materials_to_workflows_table', 23),
(74, '2020_12_14_151555_alter_column_autres_degats_in_workflows_table', 24),
(75, '2020_12_14_151841_add_column_type_usage_to_assures_table', 24),
(76, '2020_12_29_171516_alter_table_change_etape_workflows_table', 25);

--
-- Déchargement des données de la table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`, `class_name`) VALUES
('azouz.mohammed@it.devcorp.fr', 'b41a7635a1a7bc23b00ca6b632e9e9dead275177f2d27fbed2c6c2bf9b3ce8f1', '2020-11-20 08:20:13', 'Assurance'),
('azouz.mohammed@it.devcorp.fr', '3ff7a07d71d04eb5e5b772a0c3cd226e01b46b0eadfff44cd5e93c79f0e38d22', '2020-11-20 08:34:24', 'Assurance'),
('azouz.mohammed@it.devcorp.fr', 'ab5051eae00ed05532148bf2d553e312200b4e33e33af8f785486e9cc7e3cc97', '2020-11-20 08:37:59', 'Assurance'),
('akhdar@gmail.com', '7d135578913a15e0799aefb037c31dd69e3fc90bb67957e0a6501cc12329949c', '2020-11-20 08:57:23', 'Assurance'),
('akhdar@gmail.com', '6a0aa69d724839665eb3ce1b07e8eb60b7f7ff425bd31d5cea553ced14db392c', '2020-11-20 09:02:32', 'Assurance'),
('said.iduas@it.devcorp.fr', '15ac714e86d360f8709b4feb8a1758f34cbe95b361329ef225ac6f8e31b1c926', '2020-12-11 09:58:39', 'UserAssistance'),
('said.iduas@it.devcorp.fr', '1dc608785a771402a7cf0afb037fd5582195acb80da9f8a9d53ed583faed82b5', '2020-12-11 13:48:11', 'UserAssurance'),
('elhassani@it.devcorp.fr', 'a876908b61c6e94bd3d9e46a343e7633ae7bdae4e8144554d97a8543b93738ea', '2020-12-11 14:11:48', 'UserAssurance'),
('test@gmail.com', '97a2446fd6c62cdefabbcc81c227d240038f44a34dc45889fd5178fa90fb2177', '2020-12-22 15:23:28', 'Constateur');

--
-- Déchargement des données de la table `prestataires`
--

INSERT INTO `prestataires` (`id_prestataire`, `code_groupe`, `name`, `adresse`, `ville`, `telephone`, `email`, `type_prestataire`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'c145', 'Karim Mekouar', '15 Boulevard Zerktouni', 'Casablanca', '05226314789', 'karim@gmail.com', 'CONSTATEURS', '2020-10-09 14:07:48', NULL, NULL),
(2, 'G1526', 'Said Essaoudi', '16 Hay Nassim Bernoussi', 'Casablanca', '0522631478', 'said.iduoas@it.devcorp.fr', 'CONSTATEURS', '2020-12-04 10:05:38', '2020-12-04 10:05:38', NULL),
(3, 'G1526', 'Anas Senhaji', '30 Hay Farah', 'Casablanca', '0522631789', 'senhajianas@it.devcorp.fr', 'CONSTATEURS', '2020-12-04 10:05:38', '2020-12-04 10:05:38', NULL),
(4, 'G1526', 'Anas Agouriane', '120 Hay Hassani', 'Casablanca', '0522631712', 'anas.agouriane@it.devcorp.fr', 'CONSTATEURS', '2020-12-04 10:05:38', '2020-12-04 10:10:38', NULL),
(5, 'G1526', 'Alex Saba', '30 Beauséjour', 'Casablanca', '0522631747', 'alex.saba@devcorp.fr', 'AMBULANCES', '2020-12-04 10:05:38', '2020-12-04 10:15:38', NULL),
(6, 'G1526', 'Ayoub Elhassani', '30 Rue Nejma Boulevard Afghanistan', 'Casablanca', '0522631781', 'ayoub.elhassani@it.devcorp.fr', 'DEPANEUSES', '2020-12-04 10:20:38', '2020-12-04 10:20:38', NULL);

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `nom`, `prenom`, `phone`, `email`, `password`, `avatar`, `created_at`, `updated_at`) VALUES
(3, 'Admin', 'Admin', '06-96325874', 'admin@admin.com', '$2y$10$pvmnkXEncc6kNje40uGBmu7bFraU/sGFAGa1VTM0sd1h8dhrOyJs.', NULL, '2020-10-13 10:20:32', '2020-10-13 13:24:12'),
(4, 'Sabba', 'Alex', NULL, 'alex@gmail.com', '$2y$10$pvy7IQWlyaN79C29E52aTOuP8ZXTSUkaSzZ8Bgb0KcfrYUSqzW5aW', NULL, '2020-11-09 10:11:07', '2020-11-09 10:11:07'),
(5, 'Admin', 'Admin', NULL, 'admin1@admin.com', '$2y$10$xKxRh3ztGnpQ4641SrTl8OvPyBwJ9SMk3C3MbKcBngREqZUURYNge', NULL, '2020-11-09 10:12:22', '2020-11-09 10:12:22');

--
-- Déchargement des données de la table `user_assistances`
--

INSERT INTO `user_assistances` (`id_user_ass`, `nom`, `prenom`, `telephone`, `email`, `password`, `role`, `id_assistance`, `created_at`, `updated_at`, `avatar`) VALUES
(1, 'Admin', 'Admin', '669926317', 'admin@admin.com', '$2y$10$nA6aRvHfvlzhpFqH9dht3Oo.acfcfK0rnvQdgIjomqgFDnRBuwqOe', 'SUPERADMIN', 1, '2020-10-09 15:12:19', '2020-12-11 09:20:29', '75U3wmyDtqWJHEjYSD7xLUXMR.jpg'),
(2, 'AZOUZ', 'Mohammed', '0669926317', 'azmed@gmail.com', '$2y$10$pVmomnvngEpvIZ9b3pat.eKRr.l8JYtzLotuKKfPVZdw.EoCccWZm', 'EXPERT', 1, '2020-10-09 14:16:12', '2020-10-09 14:16:12', NULL),
(3, 'Elaarousi', 'Med', '0669926317', 'ahmed@gmail.com', '$2y$10$JbwFqj8Ku/bDvy6nIFCId.e55kSwkWZWVnSCSSh57cgXAFjzVSrm.', 'EXPERT', 2, '2020-10-09 14:25:13', '2020-10-09 14:25:13', NULL),
(4, 'Elhassani', 'Ayoub', '0656314789', 'ayoub@gmail.com', '$2y$10$UWFF6xoG.kB.Vp3bCLhBCuSYaOfnJjt0q7fmGDBwKNBnMCR1AKTfi', 'EXPERT', NULL, '2020-10-09 14:29:30', '2020-10-09 14:29:30', NULL),
(5, 'Essaoudi', 'Said', '669926345', 'said.iduas@it.devcorp.fr', '$2y$10$92WC/sfpZKGuxmXuRaVO5umzcItRtxGq67w6vJQXWlxOvNldWKZ3y', 'GESTIONNAIRE_CA', NULL, '2020-12-11 09:58:39', '2020-12-11 09:58:39', 'EWzG2y0Nx7Yn7Rk8Z4JXgMZ89.jpg');

--
-- Déchargement des données de la table `user_assurances`
--

INSERT INTO `user_assurances` (`id_user_assu`, `nom`, `prenom`, `telephone`, `email`, `password`, `role`, `id_assurance`, `created_at`, `updated_at`, `avatar`) VALUES
(1, 'Admin', 'User', '321456987', 'admin@admin.com', '$2y$10$nA6aRvHfvlzhpFqH9dht3Oo.acfcfK0rnvQdgIjomqgFDnRBuwqOe', 'SUPERADMIN', 1, '2020-12-11 11:25:11', NULL, 'Cv9A3XZdxpJWNAcbp1rzUQcPK.jpg'),
(5, 'Simone Muller', 'Doris D\'Amore', '+1101285355727', 'toy.krystina@example.net', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'SUPERADMIN', 1, '2020-12-17 08:32:54', '2020-12-17 08:32:54', 'Cv9A3XZdxpJWNAcbp1rzUQcPK.jpg'),
(6, 'Niko Hackett', 'Terrence Nienow', '+1885828570201', 'bpfannerstill@example.net', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'SUPERADMIN', 1, '2020-12-17 08:32:54', '2020-12-17 08:32:54', 'Cv9A3XZdxpJWNAcbp1rzUQcPK.jpg'),
(7, 'Dr. Emanuel Marquardt', 'Ms. Phoebe Friesen', '+5134044165848', 'jaron.wilderman@example.com', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'EXPERT', 1, '2020-12-17 08:32:54', '2020-12-17 08:32:54', 'Cv9A3XZdxpJWNAcbp1rzUQcPK.jpg'),
(8, 'Van Ryan', 'Mrs. Kara Botsford V', '+5527850263357', 'bridgette.stokes@example.net', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'SUPERADMIN', 1, '2020-12-17 08:32:54', '2020-12-17 08:32:54', 'Cv9A3XZdxpJWNAcbp1rzUQcPK.jpg'),
(9, 'Josue Turcotte II', 'Prof. Kendrick Schoen', '+8678329545340', 'fritsch.vinnie@example.net', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'EXPERT', 1, '2020-12-17 08:32:54', '2020-12-17 08:32:54', 'Cv9A3XZdxpJWNAcbp1rzUQcPK.jpg'),
(10, 'Misty Raynor', 'Darius Gaylord', '+5433434692513', 'efren12@example.org', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'EXPERT', 1, '2020-12-17 08:32:54', '2020-12-17 08:32:54', 'Cv9A3XZdxpJWNAcbp1rzUQcPK.jpg'),
(11, 'Travon Mohr', 'Camryn Stehr', '+5817917243211', 'mortimer.ernser@example.com', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'EXPERT', 1, '2020-12-17 08:32:54', '2020-12-17 08:32:54', 'Cv9A3XZdxpJWNAcbp1rzUQcPK.jpg'),
(12, 'Olga Erdman MD', 'Mr. Arely Kuhn', '+8783030705514', 'isabel16@example.net', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'GESTIONNAIRE_CA', 1, '2020-12-17 08:32:54', '2020-12-17 08:32:54', 'Cv9A3XZdxpJWNAcbp1rzUQcPK.jpg'),
(13, 'Nona Flatley MD', 'Reilly Erdman', '+4716714566423', 'lgrant@example.net', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'GESTIONNAIRE_CA', 1, '2020-12-17 08:32:54', '2020-12-17 08:32:54', 'Cv9A3XZdxpJWNAcbp1rzUQcPK.jpg'),
(14, 'Dr. Kennedy Rohan', 'Adrien Lebsack', '+2081726075744', 'ewalsh@example.org', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'GESTIONNAIRE_CA', 1, '2020-12-17 08:32:54', '2020-12-17 08:32:54', 'Cv9A3XZdxpJWNAcbp1rzUQcPK.jpg'),
(15, 'Prof. Akeem Dooley', 'Mr. Kris Kovacek', '+7627884133924', 'kautzer.fannie@example.org', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'SUPERADMIN', 1, '2020-12-17 08:32:54', '2020-12-17 08:32:54', 'Cv9A3XZdxpJWNAcbp1rzUQcPK.jpg'),
(16, 'Dr. Hortense Larkin', 'Madyson Koch DDS', '+9625102958752', 'kendra96@example.com', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'GESTIONNAIRE_CA', 1, '2020-12-17 08:32:54', '2020-12-17 08:32:54', 'Cv9A3XZdxpJWNAcbp1rzUQcPK.jpg'),
(17, 'Ephraim Oberbrunner IV', 'Mrs. Catalina Torphy DVM', '+3793501611193', 'psmith@example.net', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'GESTIONNAIRE_CA', 1, '2020-12-17 08:32:54', '2020-12-17 08:32:54', 'Cv9A3XZdxpJWNAcbp1rzUQcPK.jpg'),
(18, 'Marian Moen', 'Birdie Rolfson', '+4040670543874', 'schneider.jean@example.net', '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', 'GESTIONNAIRE_CA', 1, '2020-12-17 08:32:55', '2020-12-17 08:32:55', 'Cv9A3XZdxpJWNAcbp1rzUQcPK.jpg');

--
-- Déchargement des données de la table `user_prestataires`
--

INSERT INTO `user_prestataires` (`id`, `nom`, `prenom`, `phone`, `email`, `identifier`, `password`, `avatar`, `role`, `id_prestataire`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'ADMIN', '0669926317', 'admin@admin.com', NULL, '$2y$10$nA6aRvHfvlzhpFqH9dht3Oo.acfcfK0rnvQdgIjomqgFDnRBuwqOe', NULL, 'SUPERADMIN', 1, NULL, '2020-11-23 10:10:36');

--
-- Déchargement des données de la table `vehicles`
--

INSERT INTO `vehicles` (`id_vehicle`, `num_immatriculation`, `modele`, `marque`, `venant_de`, `allant_vers`, `created_at`, `updated_at`, `id_constat`, `point_choc`, `zone_choc`, `photos`, `autres`, `type`) VALUES
(116, 963682, 'Logan', 'Dacia', 'Katlyn Isle', 'Jerde Forest', '2020-12-16 15:18:38', '2020-12-24 13:40:27', 1, '01, 02', '\"Face arri\\u00e8re,Par choc,Malle arri\\u00e8re\"', '[null]', NULL, NULL),
(117, 237569, 'Stelvio', 'Alfa Romeo', 'Cruickshank Terrace', 'Goodwin Ridge', '2020-12-16 15:18:38', '2020-12-16 15:18:38', 1, NULL, NULL, NULL, NULL, 'AUTOMOBILE'),
(118, 992950, 'Q5', 'Audi', 'Mario Port', 'Marks Hollow', '2020-12-16 15:19:06', '2020-12-16 15:19:06', 2, NULL, NULL, NULL, NULL, 'MOTO'),
(119, 362250, 'Q5', 'Audi', 'Lorena Drive', 'Jessy Throughway', '2020-12-16 15:19:06', '2020-12-16 15:19:06', 2, NULL, NULL, NULL, NULL, 'CAMION'),
(120, 630733, 'Countryman', 'Mini', 'Raven Parks', 'Theodora Springs', '2020-12-16 15:19:14', '2020-12-16 15:19:14', 3, NULL, NULL, NULL, NULL, 'BUS'),
(121, 314622, 'Countryman', 'Mini', 'Johnpaul Lock', 'Melany Extensions', '2020-12-16 15:19:14', '2020-12-16 15:19:14', 3, NULL, NULL, NULL, NULL, 'BUS'),
(122, 159257, 'Q5', 'Audi', 'Lizzie Ranch', 'Douglas Mountains', '2020-12-16 15:19:22', '2020-12-16 15:19:22', 4, NULL, NULL, NULL, NULL, 'TRIPORTEUR'),
(123, 532709, 'Yaris', 'Toyota', 'Lebsack Island', 'Dare Harbor', '2020-12-16 15:19:22', '2020-12-16 15:19:22', 4, NULL, NULL, NULL, NULL, 'AUTOMOBILE'),
(124, 113384, '3008', 'Peugeot', 'Wuckert Lock', 'Dibbert Summit', '2020-12-16 15:19:30', '2020-12-16 15:19:30', 5, NULL, NULL, NULL, NULL, 'MOTO'),
(125, 443480, '3008', 'Peugeot', 'Homenick Crescent', 'Duane Ports', '2020-12-16 15:19:30', '2020-12-16 15:19:30', 5, NULL, NULL, NULL, NULL, 'AUTOMOBILE'),
(126, 144032, 'Campus', 'Clio', 'Bartoletti Ways', 'Hansen Prairie', '2020-12-16 15:19:43', '2020-12-16 15:19:43', 6, NULL, NULL, NULL, NULL, 'BUS'),
(127, 908209, 'Logan', 'Dacia', 'Selena Lake', 'Angeline Villages', '2020-12-16 15:19:43', '2020-12-16 15:19:43', 6, NULL, NULL, NULL, NULL, 'TRIPORTEUR'),
(128, 128005, 'Q5', 'Audi', 'Okuneva Rest', 'Stracke Pine', '2020-12-16 15:19:50', '2020-12-16 15:19:50', 7, NULL, NULL, NULL, NULL, 'AUTOMOBILE'),
(129, 980045, 'Yaris', 'Toyota', 'Giovani Circle', 'Windler Lodge', '2020-12-16 15:19:50', '2020-12-16 15:19:50', 7, NULL, NULL, NULL, NULL, 'TRIPORTEUR'),
(130, 215166, 'Campus', 'Clio', 'Crona Expressway', 'Everette Extension', '2020-12-16 15:19:56', '2020-12-16 15:19:56', 8, NULL, NULL, NULL, NULL, 'MOTO'),
(131, 402762, 'classe a', 'Mercedes', 'Dillon Expressway', 'Will Ridge', '2020-12-16 15:19:56', '2020-12-16 15:19:56', 8, NULL, NULL, NULL, NULL, 'BUS'),
(132, 769388, 'Stelvio', 'Alfa Romeo', 'Colin Burg', 'Breitenberg Crescent', '2020-12-16 15:20:02', '2020-12-16 15:20:02', 9, NULL, NULL, NULL, NULL, 'MOTO'),
(133, 306806, 'classe a', 'Mercedes', 'Kuphal Cliffs', 'Pfannerstill Bypass', '2020-12-16 15:20:02', '2020-12-16 15:20:02', 9, NULL, NULL, NULL, NULL, 'MOTO'),
(134, 675692, 'Campus', 'Clio', 'Shirley Gardens', 'Bertrand Roads', '2020-12-16 15:20:08', '2020-12-16 15:20:08', 10, NULL, NULL, NULL, NULL, 'TRIPORTEUR'),
(135, 146090, 'Ecosport', 'Ford', 'Wuckert Ford', 'Cleta Light', '2020-12-16 15:20:08', '2020-12-16 15:20:08', 10, NULL, NULL, NULL, NULL, 'TRIPORTEUR'),
(136, 343113, 'Ecosport', 'Ford', 'Holly Run', 'Champlin Village', '2020-12-16 15:20:14', '2020-12-16 15:20:14', 11, NULL, NULL, NULL, NULL, 'MOTO'),
(137, 227008, 'Campus', 'Clio', 'Lauriane Stream', 'Murphy Trafficway', '2020-12-16 15:20:14', '2020-12-16 15:20:14', 11, NULL, NULL, NULL, NULL, 'MOTO'),
(142, 1648, 'RENAULT', 'CLIO', 'BOULEVARD HAJ FATAH', 'MÊME SENS', '2020-12-17 12:31:44', '2020-12-30 15:32:56', 84, 'M,D,U', NULL, NULL, NULL, NULL),
(143, 61879, 'RENAULT', 'MEGANE', 'BOULEVARD HAJ FATAH', 'MÊME SENS', '2020-12-17 12:32:02', '2020-12-30 09:29:00', 84, 'G, D, A, L, M, R', NULL, '[null]', NULL, NULL);

--
-- Déchargement des données de la table `workflows`
--

INSERT INTO `workflows` (`id_workflow`, `created_at`, `updated_at`, `date_etablissement`, `heure_etablissement`, `date_accident`, `heure_accident`, `lieu`, `autres_degats`, `circonstances_accident`, `croquis`, `constateur`, `heure_missionnement`, `heure_depart`, `heure_arrivée`, `heure_fin`, `photos`, `etape`, `date_pause`, `date_end_pause`, `period_pause`, `ville`, `lat`, `lng`, `quartier`, `degats_materials`) VALUES
(1, '2020-10-21 15:07:46', '2020-12-24 13:40:27', '2020-12-13', '16:07:45', NULL, NULL, 'Av Youssef Ibn Tachfine', NULL, '[{\"id_vehicle\":51,\"values\":[\"HEURTAIT_ARRIERE_MEME_SENS_MEME_FILE\"]}]', '{\"filepath\":\"lgqYvDRi8tAopMrDWJflk1eyw.jpg\",\"name\":\"croquis.png\"}', NULL, NULL, NULL, NULL, NULL, '[\"LT8XDl0XnjIjTgovhXajiscO9.jpg\",\"3mDhaO6QDWmeS3EljbadmxzlW.jpg\",\"kL4Y58t8EhPqlEMavGvytX5Oy.jpg\",\"aKObGY5vcUushMVy5od4QRiVK.jpg\"]', 'ETAPE_DEGAT_A', NULL, NULL, 13, 'Casablanca', '33.5898427194672', '-7.603333448132107', 'Maarif', 0),
(3, '2020-12-04 11:04:25', '2020-12-17 12:01:55', '2020-12-13', '12:05:25', '2020-12-04', '12:05:25', 'Av Youssef Ibn Tachfine', NULL, NULL, '{\"filepath\":\"YULS6uTtlE4eB7sFXN7M5mM1z.jpg\",\"name\":\"croquis.png\"}', NULL, NULL, NULL, NULL, NULL, '[\"ltWxWNRgI7owoFconVArLSZpG.jpg\",\"PDnw0flbfYHUeIFuWQJw1qrJM.jpg\",\"QARd2xDDUes9uyJHntOphObNa.jpg\",\"JFD6JblP38tSX7zj6U7bUF8qG.jpg\"]', 'ETAPE_PHOTOS', NULL, NULL, NULL, 'Casablanca', '33.5898427194672', '-7.603333448132107', 'Hay hassani', 0),
(4, '2020-12-01 11:04:25', '2020-12-17 12:01:58', '2020-12-13', '12:10:31', NULL, NULL, 'Angle boulevard capitaine Puissesseau et, Rue Rabia Al Adaouia', NULL, NULL, '{\"filepath\":\"6VBiBFl5NiDYhsmeLEVTarSmM.jpg\",\"name\":\"croquis.png\"}', NULL, NULL, NULL, NULL, NULL, '[\"FF4Zpk6THATIv37Hbf6H1hjt1.jpg\",\"nmJEqN3jYnzlJn1cNYxFTRbBV.jpg\",\"9myIJBgYR8ai923nBvTeakamD.jpg\",\"5w2thceTnTaUAxs24pDIgQKcC.jpg\"]', 'ETAPE_PHOTOS', NULL, NULL, NULL, 'Rabat', '33.58819448629985', '-7.603210600165201', 'Maarif', 0),
(5, '2020-11-18 11:17:05', '2020-12-17 12:02:02', '2020-12-13', '18:17:05', NULL, NULL, 'Rue Ahmed el Figuigui', NULL, NULL, '{\"filepath\":\"UaUxiF2yVG17zgpyRoKhNfOXl.jpg\",\"name\":\"croquis.png\"}', NULL, NULL, NULL, NULL, NULL, '[\"Vb83KCvsSJSEPmZ3O5IpcbWN2.jpg\",\"nDrx5r0uzDp1Wz2ase1JH9xeG.jpg\",\"uVn8TGjiSerNO6mYIshoI3ZU8.jpg\",\"LbxQfLtfIu3auFnrr6ulkzNK4.jpg\"]', 'ETAPE_PHOTOS', NULL, NULL, NULL, 'Tanger', '33.5796907485111', '-7.6078454597804726', 'Mers sultan', 0),
(6, '2020-11-23 11:17:05', '2020-12-17 12:02:05', '2020-12-14', '08:17:05', NULL, NULL, 'Boulevard Ibn Tachfine', NULL, NULL, '{\"filepath\":\"YLkP3RpovDuOFxJfCOlUADcxb.jpg\",\"name\":\"croquis.png\"}', NULL, NULL, NULL, NULL, NULL, '[\"H9w7AbpWchtsdaOM4KTPrqnPZ.jpg\",\"4OLwdPXGkURWudmw45CcIulOI.jpg\",\"LhwdUvqbW9Dlq4YPAhUYFQA89.jpg\",\"a5zqb0z557cGZsVtgFdDr9sv9.jpg\"]', 'ETAPE_PHOTOS', NULL, NULL, NULL, 'Casablanca', '33.58917221735013', '-7.601970738330355', 'Maarif', 0),
(7, '2020-10-06 11:28:16', '2020-12-17 12:02:08', '2020-12-14', '16:11:16', NULL, NULL, 'Boulevard Ibn Tachfine', NULL, NULL, '{\"filepath\":\"ao4TI4RFjyTa2b8yGmR2wDfvA.jpg\",\"name\":\"croquis.png\"}', NULL, NULL, NULL, NULL, NULL, '[\"wcu0MKKh4v9jcBWk82HzoAmo0.jpg\",\"Rdz0cIAX1F4OVZctG06b0zM7W.jpg\",\"pv6ZSf33G2NmHuNr9ejY1WJ22.jpg\",\"7rD1dTloGAD7KkZSmvVm5bofQ.jpg\"]', 'ETAPE_PHOTOS', NULL, NULL, NULL, 'Rabat', '33.587080841060796', '-7.594406909114008', 'Maarif', 0),
(8, '2020-11-02 11:28:16', '2020-12-17 12:02:12', '2020-12-15', '06:33:11', NULL, NULL, 'Boulevard Oued Oum Rabia', NULL, NULL, '{\"filepath\":\"8JDtVQiL567Jchc7MyjZL5s9O.jpg\",\"name\":\"croquis.png\"}', NULL, NULL, NULL, NULL, NULL, '[\"zzr9UstNOuJKmSrJmjQRbsX8O.jpg\",\"xZ2qOJcCbdnbVkpeq6sQFopto.jpg\",\"ToVu9xnmtjdTHBc7pQomgtQrc.jpg\",\"JUyLeRI2mEGFBi9ptfgnCRQdO.jpg\"]', 'ETAPE_PHOTOS', NULL, NULL, NULL, 'Casablanca', '33.55978886347337', '-7.67506934003212', 'Oulfa', 0),
(9, '2020-11-26 11:36:42', '2020-12-17 12:02:18', '2020-12-15', '22:36:42', NULL, NULL, 'Residence al firdaouisse', NULL, NULL, '{\"filepath\":\"DWGlrYo3a0ivcbf68UW1HhRLw.jpg\",\"name\":\"croquis.png\"}', NULL, NULL, NULL, NULL, NULL, '[\"rTC57swXkH37qE7TUpAYIhsRX.jpg\",\"xJ6KueMCxks6zPixJDOIEE5j8.jpg\",\"zN4ksWDpLYDOCWFkxxhu8aXNe.jpg\",\"7M4tQL45ELr8ku580tr8k229Q.jpg\"]', 'ETAPE_PHOTOS', NULL, NULL, NULL, 'Fes', '33.55989093777965', '-7.670263173615341', 'Oulfa', 0),
(10, '2020-12-01 11:36:42', '2020-12-17 12:02:36', '2020-12-15', '09:22:12', NULL, NULL, 'Quartier Al Firdaouss', NULL, NULL, '{\"filepath\":\"VjsqIF7hMABQt6hKvTGwdIC7k.jpg\",\"name\":\"croquis.png\"}', NULL, NULL, NULL, NULL, NULL, '[\"Cn0nlv6zOyClwbLsUeYoN3fqJ.jpg\",\"DZIuJWv8tTp5KgkIEokPoaGHt.jpg\",\"FybOItKGxTZsllcQ5l6XgFBRJ.jpg\",\"zuOmRoiP7Vx40FMhAjCpIhZSP.jpg\"]', 'ETAPE_PHOTOS', NULL, NULL, NULL, 'Casablanca', '33.55841087409316', '-7.671396394516435', 'Oulfa', 0),
(11, '2020-12-01 11:43:39', '2020-12-17 12:02:40', '2020-12-16', '18:21:20', NULL, NULL, 'Boulevard Oued Laou', NULL, NULL, '{\"filepath\":\"Y7wPfggw5QcHu4hvtoJkTyLyd.jpg\",\"name\":\"croquis.png\"}', NULL, NULL, NULL, NULL, NULL, '[\"hkXNS1dV0FeJRUv9FRArg6i7h.jpg\",\"pc5ameDrp1VLObrsTcAYmzbZm.jpg\",\"908tMF8PtIZiRL1M4xOCgNusS.jpg\",\"GUxsNpHiJfnx3n0p7KZbkh4d7.jpg\"]', 'ETAPE_PHOTOS', NULL, NULL, NULL, 'Fes', '33.557494991012206', '-7.674171817451199', 'Oulfa', 0),
(12, '2020-12-03 11:43:39', '2020-12-17 12:02:43', '2020-12-16', '19:21:23', NULL, NULL, 'Boulevard Oued Laou', NULL, NULL, '{\"filepath\":\"njlkcIUC6oTeTUoberL7WPfK3.jpg\",\"name\":\"croquis.png\"}', NULL, NULL, NULL, NULL, NULL, '[\"IZZQ6Nj6xx080NuyCVgK05i0r.jpg\",\"w5jZtegbN8bcyNa5hxTsyRQYp.jpg\",\"O9y0nlrIELmb9PvtCaQMdAC40.jpg\",\"IvsGGKqBKoZSyZsmWFV75egpZ.jpg\"]', 'ETAPE_PHOTOS', NULL, NULL, NULL, 'Casablanca', '33.55957814371854', '-7.674965751255142', 'Oulfa', 0),
(83, '2020-12-16 16:44:45', '2020-12-16 17:20:07', '2020-12-16', '06:44:20', '2020-12-16', '06:44:20', 'FHFJGI', NULL, NULL, '{\"filepath\":\"3R1nIs5dmKSjeRppGHlSAsU4g.jpg\",\"name\":\"croquis.png\"}', NULL, NULL, NULL, NULL, NULL, NULL, 'ETAPE_SIGN_B', NULL, NULL, NULL, 'Casablanca', NULL, NULL, 'Maarif', 0),
(84, '2020-12-16 17:46:20', '2020-12-16 17:47:52', '2020-12-16', '07:45:54', '2020-12-16', '07:45:54', 'CASABLANCA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ETAPE_CONDUCTEUR_B', NULL, NULL, NULL, 'Casablanca', NULL, NULL, 'Hay hassani', 0),
(85, '2020-12-17 12:31:21', '2020-12-30 15:32:56', '2020-12-17', '02:30:55', '2020-12-17', '02:30:55', 'CASABLANCA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ETAPE_DEGAT_CHOC_A', NULL, NULL, NULL, 'Casablanca', NULL, NULL, 'Oulfa', 0),
(86, '2020-12-17 12:39:37', '2020-12-17 12:39:37', '2020-12-17', '02:37:18', '2020-12-17', '01:37:18', '1 RUE DU ABC', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Casablanca', NULL, NULL, 'Oulfa', 0),
(87, '2020-12-17 12:42:46', '2020-12-17 13:05:57', '2020-12-17', '02:40:52', '2020-12-17', '10:40:52', 'RUE 1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ETAPE_CONDUCTEUR_B', NULL, NULL, NULL, 'Casablanca', NULL, NULL, 'Oulfa', 0),
(88, '2020-12-21 09:38:24', '2020-12-21 09:40:01', '2020-12-21', '11:37:57', '2020-12-21', '11:37:57', 'HDJEJ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ETAPE_CONDUCTEUR_B', NULL, NULL, NULL, 'Casablanca', NULL, NULL, 'Hay hassani', 0),
(89, '2020-12-21 13:40:50', '2020-12-21 13:41:31', '2020-12-21', '03:40:24', '2020-12-21', '03:40:24', 'VDBDJD', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ETAPE_VEHICLE_A', NULL, NULL, NULL, 'Casablanca', NULL, NULL, 'Maarif', 0),
(90, '2020-12-21 13:42:27', '2020-12-21 13:43:51', '2020-12-21', '03:42:08', '2020-12-21', '03:42:08', 'BDBBDR', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ETAPE_CONDUCTEUR_B', NULL, NULL, NULL, 'Casablanca', NULL, NULL, 'Hay hassani', 0),
(91, '2020-12-21 15:31:51', '2020-12-21 16:07:43', '2020-12-21', '05:12:33', '2020-12-21', '05:12:33', 'CASABLANCA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ETAPE_CONDUCTEUR_B', NULL, NULL, NULL, 'Casablanca', NULL, NULL, 'Maarif', 0),
(92, '2020-12-22 10:21:15', '2020-12-22 10:22:25', '2020-12-22', '12:20:55', '2020-12-22', '12:20:55', 'VWBDB', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ETAPE_CONDUCTEUR_B', NULL, NULL, NULL, 'Casablanca', NULL, NULL, 'Hay hassani', 0),
(93, '2020-12-22 11:27:41', '2020-12-22 11:28:56', '2020-12-22', '01:27:22', '2020-12-22', '01:27:22', 'BDDB', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ETAPE_CONDUCTEUR_B', NULL, NULL, NULL, 'Casablanca', NULL, NULL, 'Maarif', 0),
(94, '2020-12-23 16:15:53', '2020-12-23 16:18:13', '2020-12-23', '06:15:20', '2020-12-23', '06:15:20', 'CASABLANCA', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ETAPE_CONDUCTEUR_B', NULL, NULL, NULL, 'Casablanca', NULL, NULL, 'Maarif', 0),
(95, '2020-12-24 09:16:44', '2020-12-24 09:56:43', '2020-12-24', '11:16:02', '2020-12-24', '11:16:02', 'HDJDJ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ETAPE_CONDUCTEUR_B', NULL, NULL, NULL, 'Casablanca', NULL, NULL, 'Oulfa', 0);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
