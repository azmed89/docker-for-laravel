<?php

namespace App\Enums;

use App\Libs\Traits\EnumIterator;

class eApp{

    use EnumIterator;

    const APP_ARTISAN = 'APP_CONSTATEUR';
    const APP_CLIENT = 'APP_SINISTRE';
}
