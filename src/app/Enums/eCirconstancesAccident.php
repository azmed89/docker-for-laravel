<?php
/**
 * Created by PhpStorm.
 * User: abderrahim akhdar
 * Date: 16/10/2020
 * Time: 11:56
 */
namespace App\Enums;

use App\Libs\Traits\EnumIterator;

class eCirconstancesAccident
{

    use EnumIterator;

    const HEURTAIT_ARRIERE_MEME_SENS_MEME_FILE = 'Heurtait à l\'arriere en roulant dans le meme sens et sur une meme file  ';
    const ROULAIT_MEME_SENS_FILE_DIFFERENTE = 'Roulait dans le meme sens et sur une file différente';
    const ROULAIT_EN_SENS_INVERSE = 'Roulet en sens inverse ';
    const PROUVENAIT_CHAUSSEE_DIFFERENTE = 'Prouvenait d\'une chaussée différente';
    const VENAIT_DROITE = 'Venait de droite(dans un carrefour)';
    const S_ENGAGEAIT_PLACE_A_SENS_GIRATOIRE = 'S\'engager sur une place à sens giratoire';
    const EN_STATIONNEMENT = 'En stationnement';
    const QUITTAIT_STATIONNEMENT = 'Quittait un stationnement';
    const RECULAIT = 'Reculait';
    const DOUBLAIT = 'Doublait(Dépassait)';
    const DEPACEMENT_IRREGULIER = 'Dépacement irrégulier';
    const CHANGEAIT_FILE = 'Changeait de file';
    const VIRAIT_DROITE = 'Virait à droit';
    const VIRAIT_GAUCHE = 'Virait à gauche';
    const S_ENGAGEAIT_PARKING_LIEUPRIVE_CHEMINTERRE='S\'engageait dans un parking,un lieu privé,un chemin de terre ';
    const SORTAIT_PARKING_LIEUPRIVE_CHEMINTERRE='Sortait d\'un parking, d\'un lieu privé,un chemin de terre';
    const EMPIETAIT_CHAUSSEE_SENS_INVERSE = 'Empiétait sur la partie de chaussée réservée à la circulation en sens inverse';
    const ROULAIT_SENS_INTERDIT = 'Roulait en sens interdit';
    const PAS_RESPECTE_SIGNAL_PRIORITE = 'N\'a pas respécté un signal de priorité';
    const FAISAIT_DEMI_TOUR ='Faisait un demi tour';
    const OUVRAIT_PORTIERE = 'Ouvrait une portiére';



}