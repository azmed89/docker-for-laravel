<?php
/**
 * Created by PhpStorm.
 * User: AZMED
 * Date: 08/10/2020
 * Time: 11:13
 */

namespace App\Enums;

use App\Libs\Traits\EnumIterator;

class eConstatStatut
{
    use EnumIterator;

    const CREATED = 'CREATED';
    const AFFECTED = 'AFFECTED';
    const REFUSED = 'REFUSED';
    const CANCELED = 'CANCELED';
    const IN_PROGRESS = 'IN_PROGRESS';
    const FINISHED = 'FINISHED';
    const PAUSE = 'PAUSE';
}
