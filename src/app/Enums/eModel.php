<?php
/**
 * Created by PhpStorm.
 * User: abderrahim akhdar
 * Date: 11/11/2020
 * Time: 15:01
 */

namespace App\Enums;

use App\Libs\Traits\EnumIterator;

class eModel{

    use EnumIterator;

    const ASU = 'Assurance';
    const ASI = 'Assistance';
    const PRE = 'Prestataire';
    const CON = 'Constateur';
    const UASU = 'UserAssurance';
    const UASI = 'UserAssistance';
    const UPRE = 'UserPrestataire';

}