<?php
/**
 * Created by PhpStorm.
 * User: AZMed
 * Date: 31/08/2020
 * Time: 10:23
 */

namespace App\Enums;

use App\Libs\Traits\EnumIterator;

class ePerimetre
{
    use EnumIterator;

    const URBAN = 'URBAN';
    const RAYON50 = 'RAYON50';
    const INTERURBAIN = 'INTERURBAIN';


}
