<?php

namespace App\Enums;

use DevcorpIt\ResponseCode\Enum\eBaseResponseCode;

abstract class eResponseCode extends eBaseResponseCode
{

    /*
     * examples :
     * de 00 à 04 sont réservés pour les actions suivants, tout autre action nécessite un code supérieur à 04
     * const S_LISTED_200_00 = ['S200_00' => 'Sheets succèsfully listed']; 00 -> pour lister
     * const S_GET_200_01 = ['S200_01' => 'Sheet returned succèssfully'];  01 -> retourner un element
     * const S_GET_200_01x1 = ['S200_01x1' => 'Sheet returned succèssfully']; 01xN -> attention à utiliser uniquement si pour le même code on a des variantes, le même example peut être appliquer sur les autres codes
     * const S_CREATED_200_02 = ['S200_02' => 'Sheet succèsfully created'];02 -> créer element
     * const S_UPDATED_200_03 = ['S200_03' => 'Sheet succèsfully updated'];03 -> mettre à jour
     * const S_DELETED_200_04 = ['S200_04' => 'Sheet succèsfully deleted'];04 -> supprimer un element
     *
     */

    // Login
    const LOGGED_IN_INFO_200_01 = ['L200_01' => 'Vous êtes désormais connecté(e)'];
    const LOGGED_IN_200_05      = ['L200_05' => 'Vous êtes désormais connecté(e)'];
    const LOGGED_OUT_200_06     = ['L200_06' => 'Vous êtes désormais déconnecté(e)'];
    const LOGGED_401_01         = ['L401_01' => 'Mot de passe ou adresse email incorrects'];
    const LOGGED_401_02         = ['L401_02' => 'Invalid Token'];

    // Forgot Password
    const FP_MAIL_SENT_200_05 = ['FP200_05' => 'L’email de réinitialisation du mot de passe vous a été envoyé'];
    const FP_403_MAX_TENTATIVE = ['FP403_00' => 'Vous avez atteint le nombre de tentatives possible '];

    // Password Reset
    const RP_MODIFIED_200_03 = ['RP200_03' => 'Mot de passe modifié avec succès'];
    const RP_CORRECT_200_05 = ['RP200_05' => 'Mot de passe correct'];
    const RP_INCORRECT_200_06 = ['RP200_06' => 'Mot de passe incorrect'];
    const RP_403_EXPIRED = ['RP403_00' => 'Mot de passe expiré'];
    const RP_404_NOT_FOUND = ['RP404_00' => 'Mot de passe introuvable'];

    // Email Validation
    const EV_403_EXPIRED = ['EV403_00' => 'Mot de passe expiré'];
    const EV_404_NOT_FOUND = ['EV404_00' => 'Mot de passe introuvable'];
    const EV_VALIDATED_200_05 = ['EV200_05' => 'Adresse email invalide'];
    const EV_INCORRECT_200_06 = ['EV200_06' => 'Mot de passe incorrect'];

    //User
    const U_LISTED_200_00 = ['U200_00' => 'Utilisateurs listés avec succès'];
    const U_GET_200_01    = ['U200_01' => 'Utilisateur listé avec succès'];
    const U_CREATE_200_02 = ['U200_02' => 'Utilisateur créé avec succès'];
    const U_UPDATE_200_03 = ['U200_03' => 'Utilisateur modifié avec succès'];
    const U_DELETE_200_04 = ['U200_04' => 'Utilisateur supprimé avec succès'];
    const U_404_NOT_FOUND = ['U404_00' => 'Utilisateur introuvable'];

    //Prestataire
    const P_LISTED_200_00 = ['P200_00' => 'Prestataires listés avec succès'];
    const P_GET_200_01    = ['P200_01' => 'Prestataire listé avec succès'];
    const P_CREATE_200_02 = ['P200_02' => 'Presatataire créé avec succès'];
    const P_UPDATE_200_03 = ['P200_03' => 'Prestataire modifié avec succès'];
    const P_DELETE_200_04 = ['P200_04' => 'Prestataire supprimé avec succès'];
    const P_404_NOT_FOUND = ['P404_00' => 'Prestataire introuvable'];

    //Assurance
    const A_LISTED_200_00 = ['A200_00' => 'Assurances listés avec succès'];
    const A_GET_200_01    = ['A200_01' => 'Assurance listé avec succès'];
    const A_CREATE_200_02 = ['A200_02' => 'Assurance créé avec succès'];
    const A_UPDATE_200_03 = ['A200_03' => 'Assurance modifié avec succès'];
    const A_DELETE_200_04 = ['A200_04' => 'Assurance supprimé avec succès'];
    const A_404_NOT_FOUND = ['A404_00' => 'Assurance introuvable'];

    //Assistance
    const AS_LISTED_200_00 = ['AS200_00' => 'Assistances listés avec succès'];
    const AS_GET_200_01    = ['AS200_01' => 'Assistance listé avec succès'];
    const AS_CREATE_200_02 = ['AS200_02' => 'Assistance créé avec succès'];
    const AS_UPDATE_200_03 = ['AS200_03' => 'Assistance modifié avec succès'];
    const AS_DELETE_200_04 = ['AS200_04' => 'Assistance supprimé avec succès'];
    const AS_404_NOT_FOUND = ['AS404_00' => 'Assistance introuvable'];

    //Constat
    const CT_LISTED_200_00 = ['CT200_00' => 'Constats listés avec succès'];
    const CT_GET_200_01    = ['CT200_01' => 'Constat listé avec succès'];
    const CT_CREATE_200_02 = ['CT200_02' => 'Constat créé avec succès'];
    const CT_UPDATE_200_03 = ['CT200_03' => 'Constat modifié avec succès'];
    const CT_DELETE_200_04 = ['CT200_04' => 'Constat supprimé avec succès'];
    const CT_404_NOT_FOUND = ['CT404_00' => 'Constat introuvable'];
    const CT_VALIDATE_200_05 = ['CT200_05' => 'Constat validée avec succès'];
    const CT_CANCEL_200_06    = ['CT200_01' => 'Constat Annulé avec succès'];

    //assuré
    const ASU_CREATE_200_02 = ['ASU200_02' => 'infos Assuré ajoutée avec succès'];
    const ASU_UPDATE_200_03 = ['ASU200_03' => 'Assuré modifié avec succès'];
    const ASU_LISTED_200_00 = ['ASU200_00' => 'Assurés listés avec succès'];

    //conducteur
    const COND_CREATE_200_02 = ['COND200_02' => 'infos Conducteur ajoutée avec succès'];
    const COND_UPDATE_200_03 = ['COND200_03' => 'Conducteur modifié avec succès'];
    const COND_LISTED_200_00 = ['COND200_00' => 'Conducteurs listés avec succès'];
    const COND_404_NOT_FOUND = ['COND404_00' => 'Conducteur introuvable'];

    //signature
    const SIG_CREATE_200_02 = ['SIGO200_02' => 'Signature ajoutée avec succès'];
    const SIG_LISTED_200_00 = ['SIGO200_02' => 'Signatures listées avec succès'];

    //circonstances
    const CIRCO_CREATE_200_02 = ['CIRCO200_02' => 'Circonstances ajoutée avec succès'];
    const CIRCO_UPDATE_200_03 = ['CIRCO200_03' => 'Circonstance modifié avec succès'];
    const CIRCO_LISTED_200_00 = ['CIRCO200_00' => 'Circonstances listés avec succès'];

    //croquis
    const CROQ_UPLOAD_200_07 = ['CROQ200_07' => 'Croquis téléchargé avec succès'];
    const CROQ_404_NOT_UPLOADED = ['CROQ404_00' => 'Croquis non téléchargé'];
    const CROQ_GET_200_01    = ['CROQ200_01' => 'Croquis listé avec succès'];



    //Constateur
    const C_LISTED_200_00      = ['C200_00' => 'Constateurs listés avec succès'];
    const C_GET_200_01         = ['C200_01' => 'Constateur listé avec succès'];
    const C_CREATE_200_02      = ['C200_02' => 'Constateur créé avec succès'];
    const C_UPDATE_200_03      = ['C200_03' => 'Constateur modifié avec succès'];
    const C_DELETE_200_04      = ['C200_04' => 'Constateur supprimé avec succès'];
    const C_VILLES_200_05      = ['C200_05' => 'Villes Constateur listées avec succès'];
    const C_FACTURATION_200_06 = ['C200_06' => 'Information de Constateur listées avec succès'];
    const C_404_NOT_FOUND      = ['C404_00' => 'Constateur introuvable'];
    const C_IDENTIFIANT_TAKEN  = ['C406_00' => 'Identifiant déjà utilisé'];

    //Vehicle
    const V_LISTED_200_00 = ['V200_00' => 'Vehicules listés avec succès'];
    const V_GET_200_01    = ['V200_01' => 'Vehicule listé avec succès'];
    const V_CREATE_200_02 = ['V200_02' => 'Vehicule créé avec succès'];
    const V_UPDATE_200_03 = ['V200_03' => 'Vehicule modifié avec succès'];
    const V_404_NOT_FOUND = ['V404_00' => 'Vehicule introuvable'];

    //Degats
    const D_LISTED_200_00 = ['V200_00' => 'Degats listés avec succès'];
    const D_UPLOAD_200_07 = ['D200_07' => 'Degats téléchargés avec succès'];

    //Mails
    const M_SENT_200_08 = ['M200_00' => 'Mails envoyés avec succès'];

    // Reacapitulatif
    const RC_GET_200_00    = ['V200_01' => 'Recapitulatif listé avec succès'];

    // Common
    const CMN_S_AND_V_LISTED_200_00         = ['CMN200_00' => 'Services et villes listés avec succès'];
    const CMN_S_WITH_CHILDREN_LISTED_200_00x1 = ['CMN200_01x1' => 'Services avec sous-services listés avec succès'];
    const CMN_GRAND_PARENTS_LISTED_200_00x2 = ['CMN200_02x2' => 'Service grande parents listés avec succès'];

    // Global
    const EMAIL_TAKEN      = ['G406_00' => 'Email déjà utilisé'];
    const CLIENT_OBJECT_NOT_FOUND = ['G406_01' => 'Objet client introuvable'];
    const QUESTION_TAKEN      = ['G406_02' => 'Question déjà utilisée'];
    const NAME_TAKEN      = ['G406_03' => 'Nom déjà utilisé'];
    const INVALID_AUDIO_TYPE      = ['G406_04' => 'Type d’audio non supporté. Le format attendu est mp3'];

    const STEPS_CONSTAT = ['G200_00' => 'Constats étapes listés avec succès'];
    const TYPES_VEHICLE = ['G200_01' => 'Types vehicule listés avec succès'];
    const CHECK_FILE = ['G200_00' => 'Checking file'];

    // Stats
    const STS_LISTED_200_00 = ['STS200_00' => 'Statistiques listées avec succès'];
}
