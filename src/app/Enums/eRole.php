<?php

namespace App\Enums;

use App\Libs\Traits\EnumIterator;

class eRole{

    use EnumIterator;

    const SUPERADMIN = 'SUPERADMIN';
    const ADMIN = 'ADMIN';
    const MANAGER = 'MANAGER';
    const OPERATEUR = 'OPERATEUR';
}
