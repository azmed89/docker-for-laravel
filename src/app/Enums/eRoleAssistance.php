<?php
/**
 * Created by PhpStorm.
 * User: AZOUZ Med
 * Date: 06/10/2020
 * Time: 16:08
 */

namespace App\Enums;

use App\Libs\Traits\EnumIterator;

class eRoleAssistance
{
    use EnumIterator;

    const SUPERADMIN = 'SUPERADMIN';
    const EXPERT = 'EXPERT';
    const GESTIONNAIRE_CA = 'GESTIONNAIRE_CA';

}
