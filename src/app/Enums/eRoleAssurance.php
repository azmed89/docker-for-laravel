<?php
/**
 * Created by PhpStorm.
 * User: abderrahim akhdar
 * Date: 12/10/2020
 * Time: 11:46
 */

namespace App\Enums;

use App\Libs\Traits\EnumIterator;

class eRoleAssurance
{
    use EnumIterator;

    const SUPERADMIN = 'SUPERADMIN';
    const EXPERT = 'EXPERT';
    const GESTIONNAIRE_CA = 'GESTIONNAIRE_CA';

}