<?php
/**
 * Created by PhpStorm.
 * User: abderrahim akhdar
 * Date: 07/10/2020
 * Time: 18:34
 */

namespace App\Enums;

use App\Libs\Traits\EnumIterator;

class eRolePrestataire
{
    use EnumIterator;

    const SUPERADMIN = 'SUPERADMIN';
    const CONSTATEUR = 'CONSTATEUR';
    const GESTIONNAIRE_P = 'GESTIONNAIRE_P';

}