<?php
/**
 * Created by PhpStorm.
 * User: ismDev
 * Date: 23/12/2019
 * Time: 16:20
 */
namespace App\Enums;

use App\Libs\Traits\EnumIterator;

class eTypeCode{

    use EnumIterator;

    const NUMERIC = 'NUMERIC';
    const ALPHABET = 'ALPHABET';
    const ALPHANUMERIC = 'ALPHANUMERIC';
}
