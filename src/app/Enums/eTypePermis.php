<?php
/**
 * Created by PhpStorm.
 * User: abderrahim akhdar
 * Date: 15/10/2020
 * Time: 17:21
 */

namespace App\Enums;

use App\Libs\Traits\EnumIterator;

class eTypePermis{

    use EnumIterator;

    const A = 'A';
    const A1 = 'A1';
    const B = 'B';
    const C = 'C';
    const D = 'D';
    const E = 'E';
    const F = 'F';

}