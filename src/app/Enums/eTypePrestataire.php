<?php
/**
 * Created by PhpStorm.
 * User: abderrahim akhdar
 * Date: 31/08/2020
 * Time: 11:25
 */

namespace App\Enums;

use App\Libs\Traits\EnumIterator;

class eTypePrestataire
{
    use EnumIterator;

    const CONSTATEURS = 'CONSTATEURS';
    const AMBULANCES = 'AMBULANCES';
    const DEPANEUSES = 'DEPANEUSES';


}