<?php
/**
 * Created by PhpStorm.
 * User: AZMed
 * Date: 14/12/2020
 * Time: 16:19
 */
namespace App\Enums;

use App\Libs\Traits\EnumIterator;

class eTypeUsage{

    use EnumIterator;

    const TAXI = 'TAXI';
    const AGENCE_LOCATION = 'AGENCE_LOCATION';
    const TOURISTIQUE = 'TOURISTIQUE';
    const PARTICULIER = 'PARTICULIER';
}
