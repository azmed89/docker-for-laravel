<?php

namespace App\Enums;

use App\Libs\Traits\EnumIterator;
use App\Models\Artisan;
use App\Models\Client;
use App\Models\Constateur;
use App\Models\User;
use App\Models\UserAssistance;
use App\Models\UserPrestataire;

class eTypeUser{

    use EnumIterator;

    const TYPE = array(
        'App\Models\User' => array('tag' => 'user', 'model' => User::class),
        'App\Models\UserAssistance' => array('tag' => 'user_assistance', 'model' => UserAssistance::class),
        'App\Models\UserAssurance' => array('tag' => 'user_assurance', 'model' => UserAssurance::class),
        'App\Models\Constateur' => array('tag' => 'constateur', 'model' => Constateur::class),
        'App\Models\UserPrestataire' => array('tag' => 'user_prestataire', 'model' => UserPrestataire::class),
    );
}
