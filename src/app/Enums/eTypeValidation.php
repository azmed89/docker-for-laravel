<?php
/**
 * Created by PhpStorm.
 * User: ismDev
 * Date: 23/12/2019
 * Time: 16:20
 */
namespace App\Enums;

use App\Libs\Traits\EnumIterator;

class eTypeValidation{

    use EnumIterator;

    const PASSWORD_RESET = 'PASSWORD_RESET';
    const EMAIL_VALIDATION = 'EMAIL_VALIDATION';
}
