<?php
/**
 * Created by PhpStorm.
 * User: AZMed
 * Date: 19/10/2020
 * Time: 15:41
 */

namespace App\Enums;

use App\Libs\Traits\EnumIterator;

class eTypeVehicle{

    use EnumIterator;

    const AUTOMOBILE = 'AUTOMOBILE';
    const CAMION = 'CAMION';
    const TRIPORTEUR = 'TRIPORTEUR';
    const MOTO = 'MOTO';
    const BUS = 'BUS';

}
