<?php

namespace App\Exceptions;

use App\Enums\eResponseCode;
use DevcorpIt\ResponseCode\JsonResp;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;
use Throwable;
use Exception;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Throwable  $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Throwable  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $exception)
    {
        if ($exception instanceof UnauthorizedHttpException) {
            return response()->json(['_response' => [
                'code' => key(eResponseCode::LOGGED_401_02),
                'message' => array_values(eResponseCode::LOGGED_401_02)[0]
            ]], $exception->getStatusCode());
        } elseif($this->isHttpException($exception) && $exception->getStatusCode() != 404) {
            $code = get_class($exception) == Exception::class ? ( $exception->getCode() > 0 ? $exception->getCode() : 500 ) : $exception->getStatusCode();

            return response()->json(['_response' => [
                'code' => $code,
                'message' => $exception->getMessage()
            ]], $code);
        } elseif($exception instanceof ModelNotFoundException){
            $model_name = explode('\\',$exception->getModel());
            return (new JsonResp())->not_found(['404'=> end($model_name).' not found']);
        } elseif($exception instanceof AuthorizationException){
            return response()->json(['_response' => [
                'code' => '403',
                'message' => 'Unauthorized'
            ]], 403);
        } else {
            return parent::render($request, $exception);
        }
    }
}
