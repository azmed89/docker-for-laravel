<?php
/**
 * Created by PhpStorm.
 * User: AZMed
 * Date: 05/10/2020
 * Time: 10:52
 */

/***************** Auth Prestataire ***************/

/**
 * @OA\Post(
 * path="/api/v1/admin/auth/login",
 * summary="Sign in Administration",
 * description="Login by email, password",
 * tags={"Auth Administration"},
 * @OA\RequestBody(
 *    required=true,
 *    description="Pass user credentials",
 *    @OA\JsonContent(
 *       required={"email","password"},
 *       @OA\Property(property="email", type="string", format="email", example="azmed@gmail.com"),
 *       @OA\Property(property="password", type="string", format="password", example="123456"),
 *    ),
 * ),
 * @OA\Response(
 *    response=422,
 *    description="Wrong credentials response",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
 *        )
 *     )
 * )
 */

/**
 * @OA\Post(
 * path="/api/v1/admin/auth/register",
 * summary="Sign up to Administration",
 * description="Register new user",
 * tags={"Auth Administration"},
 * @OA\RequestBody(
 *    required=true,
 *    description="Pass user credentials",
 *    @OA\JsonContent(
 *       required={"email","password"},
 *       @OA\Property(property="prenom", type="string", example="Admin"),
 *       @OA\Property(property="nom", type="string", example="Admin"),
 *       @OA\Property(property="telephone", type="string", example="0522369874"),
 *       @OA\Property(property="email", type="string", format="email", example="admin@admin.com"),
 *       @OA\Property(property="password", type="string", format="password", example="password"),
 *    ),
 * ),
 * @OA\Response(
 *    response=422,
 *    description="Wrong credentials response",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Email déjà utilisé")
 *        )
 *     )
 * )
 */

/**
 * @OA\Get(
 * path="/api/v1/admin/auth/logout",
 * summary="Sign out from Administration",
 * description="Sign Out",
 * tags={"Auth Administration"},
 * security={ {"apiAuth": {} }},
 * @OA\Response(
 *    response=200,
 *    description="Success"
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Returns when user is not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */

/**
 * @OA\Get(
 * path="/api/v1/admin/auth/me",
 * summary="My profile",
 * description="My profile in Administration",
 * tags={"Auth Administration"},
 * security={ {"apiAuth": {} }},
 * @OA\Response(
 *    response=200,
 *    description="Success"
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */


/***************** Auth Prestataire ***************/

/**
 * @OA\Post(
 * path="/api/v1/pre/auth/login",
 * summary="Sign in BO Prestataire",
 * description="Login by email, password",
 * tags={"Auth BO Prestataire"},
 * @OA\RequestBody(
 *    required=true,
 *    description="Pass user credentials",
 *    @OA\JsonContent(
 *       required={"email","password"},
 *       @OA\Property(property="email", type="string", format="email", example="azmed@gmail.com"),
 *       @OA\Property(property="password", type="string", format="password", example="123456"),
 *    ),
 * ),
 * @OA\Response(
 *    response=422,
 *    description="Wrong credentials response",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
 *        )
 *     )
 * )
 */

/**
 * @OA\Post(
 * path="/api/v1/pre/auth/register",
 * summary="Sign up to BO",
 * description="Register new user",
 * tags={"Auth BO Prestataire"},
 * @OA\RequestBody(
 *    required=true,
 *    description="Pass user credentials",
 *    @OA\JsonContent(
 *       required={"email","password"},
 *       @OA\Property(property="prenom", type="string", example="Alex"),
 *       @OA\Property(property="nom", type="string", example="Sabba"),
 *       @OA\Property(property="email", type="string", format="email", example="alex@gmail.com"),
 *       @OA\Property(property="password", type="string", format="password", example="123456"),
 *    ),
 * ),
 * @OA\Response(
 *    response=422,
 *    description="Wrong credentials response",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Email déjà utilisé")
 *        )
 *     )
 * )
 */

/**
 * @OA\Get(
 * path="/api/v1/pre/auth/logout",
 * summary="Sign out from BO Prestataire",
 * description="Sign Out",
 * tags={"Auth BO Prestataire"},
 * security={ {"apiAuth": {} }},
 * @OA\Response(
 *    response=200,
 *    description="Success"
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Returns when user is not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */


/***************** Auth Constateur ***************/

/**
 * @OA\Post(
 * path="/api/v1/cons/auth/login",
 * summary="Sign in App Constateur",
 * description="Login by email, password",
 * tags={"Auth App Constateur"},
 * @OA\RequestBody(
 *    required=true,
 *    description="Pass user credentials",
 *    @OA\JsonContent(
 *       required={"email","password"},
 *       @OA\Property(property="email", type="string", format="email", example="azmed@gmail.com"),
 *       @OA\Property(property="password", type="string", format="password", example="123456"),
 *    ),
 * ),
 * @OA\Response(
 *    response=422,
 *    description="Wrong credentials response",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
 *        )
 *     )
 * )
 */

/**
 * @OA\Get(
 * path="/api/v1/cons/auth/logout",
 * summary="Sign out from App Constateur",
 * description="Sign Out",
 * tags={"Auth App Constateur"},
 * security={ {"apiAuth": {} }},
 * @OA\Response(
 *    response=200,
 *    description="Success"
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Returns when user is not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */


/*********************** Auth Assistance ********************/

/**
 * @OA\Post(
 * path="/api/v1/ass/auth/login",
 * summary="Sign in BO Assistance",
 * description="Login by email, password",
 * tags={"Auth BO Assistance"},
 * @OA\RequestBody(
 *    required=true,
 *    description="Pass user credentials",
 *    @OA\JsonContent(
 *       required={"email","password"},
 *       @OA\Property(property="email", type="string", format="email", example="azmed@gmail.com"),
 *       @OA\Property(property="password", type="string", format="password", example="password"),
 *    ),
 * ),
 * @OA\Response(
 *    response=422,
 *    description="Wrong credentials response",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
 *        )
 *     )
 * )
 */

/**
 * @OA\Get(
 * path="/api/v1/ass/auth/logout",
 * summary="Sign out from BO Assistance",
 * description="Sign Out",
 * tags={"Auth BO Assistance"},
 * security={ {"apiAuth": {} }},
 * @OA\Response(
 *    response=200,
 *    description="Success"
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Returns when user is not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */

/******************************* Auth Assurance *************************/

/**
 * @OA\Post(
 * path="/api/v1/assu/auth/login",
 * summary="Sign in BO Assurance",
 * description="Login by email, password",
 * tags={"Auth BO Assurance"},
 * @OA\RequestBody(
 *    required=true,
 *    description="Pass user credentials",
 *    @OA\JsonContent(
 *       required={"email","password"},
 *       @OA\Property(property="email", type="string", format="email", example="aabde@gmail.com"),
 *       @OA\Property(property="password", type="string", format="password", example="password"),
 *    ),
 * ),
 * @OA\Response(
 *    response=422,
 *    description="Wrong credentials response",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
 *        )
 *     )
 * )
 */

/**
 * @OA\Get(
 * path="/api/v1/assu/auth/logout",
 * summary="Sign out from BO Assurance",
 * description="Sign Out",
 * tags={"Auth BO Assurance"},
 * security={ {"apiAuth": {} }},
 * @OA\Response(
 *    response=200,
 *    description="Success"
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Returns when user is not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */
/***************** Activation Compte ***************/

/**
 * @OA\Post(
 * path="/api/v1/activation_compte",
 * summary="Activation Comptes",
 * description="Activation  by email,token",
 * tags={"Activation Comptes"},
 * @OA\Parameter(
 *     description="token",
 *     in="path",
 *     name="token_avtivation",
 *     required=true,
 *     example=" ",
 *     @OA\Schema(
 *       type="string",
 *       format="int64"
 *    )
 * ),
 * @OA\RequestBody(
 *    required=true,
 *    description="Pass user email, New password",
 *    @OA\JsonContent(
 *       required={"email","password","token"},
 *       @OA\Property(property="email", type="string", format="email", example="a.abderrahim@gmail.com"),
 *       @OA\Property(property="password", type="string", format="password", example="123456"),
 *    ),
 * ),
 * @OA\Response(
 *    response=422,
 *    description="Wrong credentials response",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Sorry, wrong email address or password. Please try again")
 *        )
 *     )
 * )
 */
