<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Enums\eResponseCode;


class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */


    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */

    protected $redirectTo = RouteServiceProvider::HOME;

    public function reset(Request $request){


        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' =>'required|string',
            'token' => 'required_without:code|string',

        ]);
        if ($validator->fails()) {
            return $this->resp->bad_request($validator);
        }

        if ($request->has('token')) {
            $pwd_reset = DB::table('password_resets')
                ->where('email', $request->email)
                ->where('token', $request->token);
        } else {
            return $this->resp->internal_error();
        }
        if (empty($pwd_reset->first())) {

            return $this->resp->not_found(eResponseCode::RP_404_NOT_FOUND);
        } else  {

            $Model=$pwd_reset->first()->class_name;
            $Model=app("App\Models\\".$Model);

            //Change Password
            $user = $Model::where('email', $request->email)->first();

            $user->password = Hash::make($request->password);
            $user->update();

            $pwd_reset->delete();
        }
        return $this->resp->ok(eResponseCode::RP_MODIFIED_200_03);
    }
}
