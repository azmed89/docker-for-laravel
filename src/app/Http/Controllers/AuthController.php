<?php

namespace App\Http\Controllers;

use App\Enums\eResponseCode;
use App\Enums\eRoleAssistance;
use App\Enums\eTypeUser;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Auth;

class AuthController extends Controller
{
    /**
     * Register a new user
     */
    public function register(Request $request)
    {
        $v = Validator::make($request->all(), [
            'nom' => 'required',
            'prenom' => 'required',
            'email' => 'required|email|unique:users',
            'password'  => 'required|min:3',
        ]);
        if ($v->fails())
        {
            $failedRules = $v->failed();
            if (isset($failedRules['email']['Unique'])) {
                return $this->resp->not_acceptable(eResponseCode::EMAIL_TAKEN);
            }

            return $this->resp->bad_request($v);
        }
        $user = new User();
        $user->nom = $request->nom;
        $user->prenom = $request->prenom;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->save();
        if($user) {
            return $this->resp->ok(eResponseCode::U_CREATE_200_02, $user);
        }

        return $this->resp->internal_error();
    }
    /**
     * Login user and return a token
     */
    public function login(Request $request)
    {
        $credentials = request(['email', 'password']);
        $token = null;
        $respondWithToken = null;

        /*if (request()->is('api/v1/auth/login') or request()->is('api/v1/auth/login')) {
            config()->set('jwt.ttl', 60*24*90);
        }*/

        if (!$token = auth()->attempt($credentials)) {
            return response()->json(['_response' => [
                'code' => key(eResponseCode::LOGGED_401_01),
                'message' => array_values(eResponseCode::LOGGED_401_01)[0]
            ]], 401);
        }

        $respondWithToken = $this->respondWithToken($token);

        return $respondWithToken;
    }
    /**
     * Logout User
     */
    public function logout()
    {
        auth()->logout();
        return response()->json([
            'success' => true,
            'msg' => 'Vous êtes désormais déconnecté(e)'
        ], 200);
        //return $this->resp->ok(eResponseCode::LOGGED_OUT_200_06);
    }
    /**
     * Get authenticated user
     */
    public function me(Request $request)
    {
        $user = auth()->user();
        if(request()->is('api/v1/admin/auth/*')){
            $user['type'] = eTypeUser::TYPE['App\Models\User']['tag'];
        } elseif(request()->is('api/v1/pre/auth/*')){
            $user['type'] = eTypeUser::TYPE['App\Models\UserPrestataire']['tag'];
        } elseif(request()->is('api/v1/ass/auth/*')){
            $user['type'] = eTypeUser::TYPE['App\Models\UserAssistance']['tag'];
        } elseif(request()->is('api/v1/assu/auth/*')){
            $user['type'] = eTypeUser::TYPE['App\Models\UserAssurance']['tag'];
        } elseif(request()->is('api/v1/cons/auth/*')){
            $user['type'] = eTypeUser::TYPE['App\Models\Constateur']['tag'];
        }
        return $this->resp->ok(eResponseCode::LOGGED_IN_INFO_200_01, $user);
    }
    /**
     * Refresh JWT token
     */
    public function refresh()
    {
        if ($token = $this->guard()->refresh()) {
            $respondWithToken = $this->respondWithRefreshToken($token);
            return $respondWithToken;
        }
        return response()->json(['_response' => [
            'code' => key(eResponseCode::LOGGED_401_01),
            'message' => array_values(eResponseCode::LOGGED_401_01)[0]
        ]], 401);
    }
    /**
     * Return auth guard
     */
    private function guard()
    {
        return Auth::guard();
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        $response = null;

        if(request()->is('api/v1/admin/auth/*')){
            $user = auth()->guard('user')->user();
            $response = [
                'id' => $user->id,
                'email' => $user->email,
                'nom' => $user->nom,
                'prenom' => $user->prenom,
                'phone' => $user->phone,
                'type' => eTypeUser::TYPE['App\Models\User']['tag'],
                'avatar' => $user->avatar,
                'token' => $token,
                'expires_in' => auth()->factory()->getTTL() * 60
            ];
        } elseif(request()->is('api/v1/pre/auth/*')){
            $user = auth()->guard('user_prestataire')->user();
            $response = [
                'id' => $user->id,
                'email' => $user->email,
                'nom' => $user->nom,
                'prenom' => $user->prenom,
                'telephone' => $user->telephone,
                'type' => eTypeUser::TYPE['App\Models\UserPrestataire']['tag'],
                'role' => $user->role,
                'avatar' => $user->avatar,
                'token' => $token,
                'id_prestataire'=>$user->id_prestataire,
                'expires_in' => auth()->factory()->getTTL() * 60
            ];
        } elseif(request()->is('api/v1/ass/auth/*')){
            $user = auth()->guard('user_assistance')->user();
            $response = [
                'id' => $user->id_user_ass,
                'email' => $user->email,
                'nom' => $user->nom,
                'prenom' => $user->prenom,
                'telephone' => $user->telephone,
                'id_assistance' => $user->id_assistance,
                'type' => eTypeUser::TYPE['App\Models\UserAssistance']['tag'],
                'role' => $user->role,
                'token' => $token,
                'expires_in' => auth()->factory()->getTTL() * 60
            ];
        } elseif(request()->is('api/v1/assu/auth/*')){
            $user = auth()->guard('user_assurance')->user();
            $response = [
                'id' => $user->id_user_ass,
                'email' => $user->email,
                'nom' => $user->nom,
                'prenom' => $user->prenom,
                'telephone' => $user->telephone,
                'id_assurance' => $user->id_assurance,
                'type' => eTypeUser::TYPE['App\Models\UserAssurance']['tag'],
                'role' => $user->role,
                'token' => $token,
                'expires_in' => auth()->factory()->getTTL() * 60
            ];
        } elseif(request()->is('api/v1/cons/auth/*')){
            $user = auth()->guard('constateur')->user();
            $response = [
                'id' => $user->id_constateur,
                'email' => $user->email,
                'nom' => $user->nom,
                'prenom' => $user->prenom,
                'adresse' => $user->adresse,
                'fonction' => $user->fonction,
                'telephone' => $user->telephone,
                'num_agent' => $user->num_agent,
                'acces' => $user->acces,
                'avatar' => $user->avatar,
                'identifiant' => $user->identifiant,
                'id_prestataire' => $user->id_prestataire,
                'type' => eTypeUser::TYPE['App\Models\Constateur']['tag'],
                'role' => $user->role,
                'token' => $token,
                'expires_in' => auth()->factory()->getTTL() * 60
            ];
        }

        return $this->resp->ok(eResponseCode::LOGGED_IN_200_05, $response);
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithRefreshToken($token)
    {
        return $this->resp->ok(eResponseCode::LOGGED_IN_200_05, [
            'token' => $token,
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }
}
