<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use DevcorpIt\ResponseCode\Controllers\ResponseController;

/**
 * @OA\Info(
 *    title="E-consta API",
 *    version="1.0.0",
 * )
 */

/**
 * @OA\SecurityScheme(
 * type="http",
 * description="Login with email and password to get the authentication token",
 * name="Token based Based",
 * in="header",
 * scheme="bearer",
 * bearerFormat="JWT",
 * securityScheme="apiAuth",
 * )
 **/

class Controller extends ResponseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
