<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class AuthentificateUserAssistance
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::guard('user_assistance')->check()){
            return response()->json(['_response' => [
                'code' => '403',
                'message' => 'Unauthorized'
            ]], 403);
        }

        return $next($request);
    }
}
