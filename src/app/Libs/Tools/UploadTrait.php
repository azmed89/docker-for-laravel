<?php

namespace App\Libs\Tools;

use Illuminate\Support\Str;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

trait UploadTrait
{

    public function uploadOne(&$uploadedFile, $folder = null, $extension = 'jpg', $filename = null, $disk = 'public')
    {
        if(!$uploadedFile instanceof UploadedFile){
            return ['relative_path' => null, 'file_name' => null];
        }

//        $extension = !is_null($uploadedFile->getClientOriginalExtension()) ? $uploadedFile->getClientOriginalExtension() : 'jpg';
        $name = !is_null($filename) ? $filename . '.' . $extension : Str::random(25) . '.' . $extension;

        $file = $uploadedFile->storeAs($folder, $name, $disk);

        return ['relative_path' => $file, 'file_name' => $name];
    }

    public function deleteOne($folder, $filename, $disk = 'public'){
        if(Storage::disk($disk)->exists($folder.DIRECTORY_SEPARATOR.$filename)) {
            Storage::disk($disk)->delete($folder.DIRECTORY_SEPARATOR.$filename);
        }
    }
}
