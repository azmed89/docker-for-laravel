<?php

namespace App\Libs\Tools;

use Illuminate\Mail\Mailable;
use Illuminate\Support\Str;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Mail;
use App\Mail\EmailConstat;

class Utils
{

    public static function uploadOne(&$uploadedFile,$folder = null,$filename = null,$disk = 'public')
    {
        if(!$uploadedFile instanceof UploadedFile){return ['relative_path'=> null,'file_name' => null];}

        $name = !is_null($filename) ? $filename : Str::random(25) . '.' . $uploadedFile->getClientOriginalExtension();

        $file = $uploadedFile->storeAs($folder, $name, $disk);

        return ['relative_path'=>$file,'file_name' => $name];
    }

    public static function validateIntegerCsv($csv,$className=null,$primary_key=null){
        $ids = explode(',', $csv);
        foreach($ids as $id) {
            if(!is_numeric($id)) return false;
        }

        if(!empty($className) && !empty($primary_key)){
            $exists = $className::whereIn($primary_key, $ids)->pluck($primary_key)->toArray();
            $result = array_diff($ids,$exists);
            return (object)['not_found' => $result , 'exists' => $exists];
        }

        return true;
    }

    public static function mergeCsvArray(array $array, $attr_name){
        $ids = array_map(function($val) {
            return explode(',', $val);
        }, array_column(array_values($array),$attr_name));

        return array_unique(call_user_func_array("array_merge", $ids));
    }

    public static function recursive_array_search($needle, $haystack, $column, $currentKey = '') {
        foreach($haystack as $key => $value) {
            if (is_array($value)) {
                $nextKey = self::recursive_array_search($needle,$value, $column, $currentKey . '[' . $key . ']');
                if ($nextKey) {
                    return $nextKey;
                }
            } else if($key == $column && $value == $needle) {
                return true;
            }
        }

        return false;
    }



}
