<?php

namespace App\Libs\Traits;

use App\Enums\eTypeCode;
use Illuminate\Support\Str;

trait GenerateCode // Use this in model to get access to functions below
{
    public function generateCode($column, $length = 6, $type = eTypeCode::ALPHANUMERIC){
        $token = null;

        switch ($type){
            case eTypeCode::NUMERIC:
                $token = $this->generateRandomNumericCode($column, $length);
                break;
            case eTypeCode::ALPHABET:
                $token = $this->generateRandomAlphabetUniqueCode($column, $length);
                break;
            case eTypeCode::ALPHANUMERIC:
                if ($column == null)
                    $token = $this->generateRandomAlphaNumericString($length);
                else
                    $token = $this->generateRandomAlphaNumericUniqueCode($column, $length);
                break;
        }

        return $token;
    }

    private function generateRandomNumericCode($column, $length = 6)
    {
        $token = null;
        $token = random_int( 10 ** ( $length - 1 ), ( 10 ** $length ) - 1);

        if (self::codeExists($column, $token)) {
            return self::generateRandomNumericCode($column, $length);
        }

        return $token;
    }

    private function generateRandomAlphabetUniqueCode($column, $length = 6)
    {
        $token = null;
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
        $codeAlphabet = str_shuffle($codeAlphabet);
        $max = strlen($codeAlphabet);

        for ($i = 0; $i < $length; $i++) {
            $token .= $codeAlphabet[random_int(0, $max - 1)];
        }

        // call the same function if exists already
        if (self::codeExists($column, $token)) {
            return self::generateRandomAlphabetUniqueCode($column, $length);
        }

        // otherwise, it's valid and can be used
        return $token;
    }

    private function generateRandomAlphaNumericUniqueCode($column, $length = 6)
    {
        $token = null;
        $token = strtoupper(Str::random($length));
        if (self::codeExists($column, $token)) {
            return self::generateRandomAlphaNumericUniqueCode($column, $length);
        }

        return $token;
    }

    private function generateRandomAlphaNumericString($length = 6)
    {
        $token = null;
        $token = null;
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
        $codeAlphabet.= "0123456789";
        $codeAlphabet = str_shuffle($codeAlphabet);
        $max = strlen($codeAlphabet);

        for ($i = 0; $i < $length; $i++) {
            $token .= $codeAlphabet[random_int(0, $max - 1)];
        }

        return $token;
    }

    private function codeExists($column, $token)
    {
        // query the database and return a boolean
        return $this->where($column, '=', $token)->exists();
    }


}
