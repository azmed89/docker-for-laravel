<?php

namespace App\Libs\Traits;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;


Trait GenerateToken {


public function generateToken ($user){

    $token = hash_hmac('sha256', Str::random(40), config('app.key'));

    DB::table('password_resets')->insert([
        'email' => $user->email,
        'token' => $token,
        'created_at' => Carbon::now()->toDateTimeString(),
        'class_name' =>substr(get_class($user), strrpos(get_class($user), '\\') + 1)
    ]);

   return $reset_token = env('APP_URL')."/api/v1/activation_compte?token=".$token;
}









}