<?php
/**
 * Created by PhpStorm.
 * User: abderrahim akhdar
 * Date: 11/12/2020
 * Time: 11:18
 */

namespace App\Libs\Traits;

Trait ReverseGeoCoding{

    public function reverseGeoCoding($lat,$lon){

        $search_url = "https://nominatim.openstreetmap.org/reverse?format=json&lat=".$lat."&lon=".$lon."&zoom=10";

        $httpOptions = [
            "http" => [
                "method" => "GET",
                "header" => "User-Agent: Nominatim-Test"
            ]
        ];

        $streamContext = stream_context_create($httpOptions);
        $json = file_get_contents($search_url, false, $streamContext);
        $decoded=json_decode($json,true);

        //$lieu = $decoded['address']['city'] ?? $decoded['address']['village'];
        $lieu = 'Casablanca';

       return  $result=(trim(preg_replace('/[^a-z0-9_ ]/i','', $lieu)));
    }
}
