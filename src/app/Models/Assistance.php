<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class Assistance extends Model
{
    use SoftDeletes,Notifiable;
    protected $table = 'assistances';
    protected $primaryKey = 'id_assistance';
    protected $softDelete = true;

    protected $fillable = [
    'nom','email','telephone'
    ];

    protected $hidden = [
        'remember_token'
    ];

    public function assurances(){
        return $this->hasMany('App\Models\Assurance', 'id_assurance', 'id_assistance');
    }


}
