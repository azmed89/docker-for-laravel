<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;

class Assurance extends Model
{
    use SoftDeletes ,Notifiable;

    protected $table = 'assurances';
    protected $primaryKey = 'id_assurance';
    protected $softDelete = true;

    protected $fillable = [
        'nom', 'code_societe', 'email','id_assistance'
    ];

    protected $hidden = [

    ];

    public function assistance() {
        return $this->belongsTo('App\Models\Assistance', 'id_assurance', 'id_assistance');
    }
}
