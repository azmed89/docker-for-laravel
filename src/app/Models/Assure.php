<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Assure extends Model
{
    protected $table = 'assures';
    protected $primaryKey = 'id_assure';

    protected $fillable = [
        'nom', 'prenom', 'adresse', 'n_attestation', 'n_police', 'email','ste_assurance', 'validite_attestation', 'agence', 'id_constat','attes_valide_du','attes_valide_au', 'type_usage'
    ];

    protected $hidden = [

    ];




    public function assurance() {
        return $this->belongsTo('App\Models\Assurance', 'ste_assurance', 'code_societe');
    }

    public function getIdConstat(){
        return $this->id_constat;
    }

    public function nomComplet(){
        return $this->nom .' '.$this->prenom;
    }
}
