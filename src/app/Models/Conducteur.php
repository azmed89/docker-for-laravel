<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Conducteur extends Model
{
    protected $table = 'conducteurs';
    protected $primaryKey = 'id_conducteur';

    protected $fillable = [
        'nom','prenom','adresse','type_permis','num_permis','prefecture_delivrance','delai_validite','date_delivrance','signature'

    ];

    protected $hidden = [

    ];

    public function constat() {
        return $this->belongsTo('App\Models\Constat', 'id_constat', 'id_constat');
    }
}
