<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Constat extends Model
{


    use Notifiable;

    protected $table = 'constats';
    protected $primaryKey = 'id_constat';
    protected $keyType = 'integer';

    protected $fillable = [
        'type_mission', 'code', 'num_immatriculation', 'etat', 'num_contrat', 'liee_a', 'id_workflow', 'id_constateur','motif'
    ];

    protected $hidden = [

    ];




    public function constateur(){
        //return  $this->belongsTo('App\Models\Prestataire','id_prestataire','id_prestataire');
        return $this->hasOne('App\Models\Constateur','id_constateur','id_constateur');
    }

    public function assures(){
        return $this->hasMany('App\Models\Assure','id_constat','id_constat');
    }
    public function conducteurs(){
        return $this->hasMany('App\Models\Conducteur','id_constat','id_constat');
    }

    public function workflow() {
        return $this->hasOne('App\Models\WorkFlow','id_workflow','id_workflow');
    }
    public function degat() {
        return $this->hasOne('App\Models\Degat','id_degat','id_degat');
    }
    public function vehicules(){
        return $this->hasMany('App\Models\Vehicule','id_constat','id_constat');
    }
    public function linked_constats(){
        return $this->hasMany('App\Models\Constat','liee_a','id_constat');
    }
    public function changeStatus(string $status) {
        $workflow = WorkFlow::find($this->workflow->id_workflow);
        $workflow->etape = $status;
        $workflow->update();
        return true;
    }
}
