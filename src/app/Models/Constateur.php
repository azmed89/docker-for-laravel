<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use App\Libs\Traits\GenerateToken;

class Constateur extends Authenticatable implements JWTSubject
{
    use Notifiable,GenerateToken;

    protected $table = 'constateurs';
    protected $primaryKey = 'id_constateur';


    public function nomComplet(){
        return $this->nom .' '.$this->prenom;
    }

    public function getIdConstateur(){
        return $this->id_constateur;
    }

    protected $fillable = [
        'email','password','nom','prenom','telephone','identifiant','avatar','id_prestataire','acces','adresse','num_agent','acces','fonction'
    ];
    protected $hidden = [
        'remember_token'
    ];

    public function prestataire() {
        return $this->hasOne('App\Models\Prestataire', 'id_prestataire', 'id_prestataire');
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
