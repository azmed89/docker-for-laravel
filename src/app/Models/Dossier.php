<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Dossier extends Model
{
    //

    protected $fillable = [

    ];

    protected $primaryKey = 'id_dossier';

    protected $hidden = [
        'remember_token'
    ];

    public function prestataire(){
      return  $this->belongsTo('App\Models\Prestataire','id_prestataire','id_prestataire');
    }

    public function compagnie_assitance(){
        return  $this->belongsTo('App\Models\Assistance','id_assistance','id_assistance');
    }

    public function constats() {
        return $this->hasMany('App\Models\Constat', 'id_dossier', 'id_dossier');
    }

}
