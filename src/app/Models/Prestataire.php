<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Prestataire extends Model
{

    use SoftDeletes,Notifiable;

    protected $table = 'prestataires';
    protected $primaryKey = 'id_prestataire';
    protected $softDelete = true;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */


    protected $fillable = [
        'email','code_groupe', 'name', 'ville','adresse', 'telephone','type_prestataire'
    ];

    protected $hidden = [
        'remember_token'
    ];

    public function Dossiers(){
       return $this->hasMany('App\Models\Dossier');
    }

}
