<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use App\Libs\Traits\GenerateToken;

class UserAssurance  extends Authenticatable implements JWTSubject
{

    use Notifiable,GenerateToken;

    protected $table = 'user_assurances';
    protected $primaryKey = 'id_user_assu';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
        'nom','prenom', 'email', 'password','telephone', 'role', 'id_assurance', 'avatar'
    ];

    protected $hidden = [
        'password'
    ];





    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

}
