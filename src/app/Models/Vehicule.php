<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Vehicule extends Model
{
    protected $table = 'vehicles';
    protected $primaryKey = 'id_vehicle';


    protected $fillable = [
        'num_immatriculation', 'modele', 'marque', 'venant_de', 'allant_vers', 'id_constat', 'point_choc', 'zone_choc', 'autres', 'photos', 'type'
    ];

    protected $hidden = [

    ];

    public function constat() {
        return $this->belongsTo('App\Models\Constat', 'id_constat', 'id_constat');
    }
}
