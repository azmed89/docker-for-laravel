<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WorkFlow extends Model
{

    protected $table = 'workflows';
    protected $primaryKey = 'id_workflow';


    protected $fillable = [
        'ville', 'date_etablissement',  'heure_etablissement', 'date_accident', 'heure_accident',
        'lieu', 'autres_degats', 'circonstances_accident', 'croquis', 'constateur', 'heure_missionnement',
        'heure_depart', 'heure_arrivée', 'heure_fin','photos', 'etape'
    ];

    protected $hidden = [

    ];

    public function constat() {
        return $this->hasOne('App\Models\Constat','id_constat','id_constat');
    }
}
