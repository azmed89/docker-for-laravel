<?php
/**
 * Created by PhpStorm.
 * User: AZMed
 * Date: 15/10/2020
 * Time: 12:30
 */

/************* Global routes *************/
/**
 * @OA\Get(
 * path="/api/v1/cons/dossier/steps",
 * summary="List Constat Steps",
 * description="List steps of constat",
 * tags={"Global - routes"},
 * security={ {"apiAuth": {} }},
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */

/**
 * @OA\Get(
 * path="/api/v1/cons/dossier/types-vehicle",
 * summary="List Types Vehicle",
 * description="List types of vehicle",
 * tags={"Global - routes"},
 * security={ {"apiAuth": {} }},
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */

/**
 * @OA\Post(
 * path="/api/v1/cons/dossier/pause-period/{id_constat}",
 * summary="Update Pause Period",
 * description="Update the pause period of a specified constat",
 * tags={"Global - routes"},
 * security={ {"apiAuth": {} }},
 * @OA\Parameter(
 *    description="ID of Constat",
 *    in="path",
 *    name="id_constat",
 *    required=true,
 *    example="1",
 *    @OA\Schema(
 *       type="integer",
 *       format="int64"
 *    )
 * ),
 * @OA\RequestBody(
 *    required=true,
 *    description="Pass constateur credentials",
 *    @OA\MediaType(
 *      mediaType="application/x-www-form-urlencoded",
 *      @OA\Schema(
 *          type="object",
 *          required={"etat"},
 *          @OA\Property(property="etat", type="string", example="PAUSE"),
 *      )
 *    )
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */

/***************** Vehicules ****************/
/**
 * @OA\Post(
 * path="/api/v1/cons/dossier/vehicle/assign/{id_constat}",
 * summary="Create Vehicle",
 * description="Create new vehicle and assign it to a constat",
 * tags={"Constateur - Dossier"},
 * security={ {"apiAuth": {} }},
 * @OA\Parameter(
 *    description="ID of Constat",
 *    in="path",
 *    name="id_constat",
 *    required=true,
 *    example="1",
 *    @OA\Schema(
 *       type="integer",
 *       format="int64"
 *    )
 * ),
 * @OA\RequestBody(
 *    required=true,
 *    description="Pass constateur credentials",
 *    @OA\MediaType(
 *      mediaType="application/x-www-form-urlencoded",
 *      @OA\Schema(
 *          type="object",
 *          required={"num_immatriculation","modele","marque","venant_de","allant_vers","etape"},
 *          @OA\Property(property="num_immatriculation", type="string", example="2536"),
 *          @OA\Property(property="modele", type="string", example="FIAT"),
 *          @OA\Property(property="marque", type="date", example="PALIO"),
 *          @OA\Property(property="venant_de", type="string", example="Boulevard Haj Fatah"),
 *          @OA\Property(property="allant_vers", type="string", example="Meme sens"),
 *          @OA\Property(property="etape", type="string", example="ETAPE_VEHICLE_A"),
 *          @OA\Property(property="type", type="string", example="VOITURE")
 *      )
 *    )
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */
/**
 * @OA\Post(
 * path="/api/v1/cons/dossier/vehicle/edit/{id_vehicle}",
 * summary="Edit Vehicle",
 * description="Edit a vehicle",
 * tags={"Constateur - Dossier"},
 * security={ {"apiAuth": {} }},
 * @OA\Parameter(
 *    description="ID of Vehicle",
 *    in="path",
 *    name="id_vehicle",
 *    required=true,
 *    example="1",
 *    @OA\Schema(
 *       type="integer",
 *       format="int64"
 *    )
 * ),
 * @OA\RequestBody(
 *    required=true,
 *    description="Pass constateur credentials",
 *    @OA\MediaType(
 *       mediaType="multipart/form-data",
 *       @OA\Schema(
 *          type="object",
 *          required={"num_immatriculation","modele","marque","venant_de","allant_vers","etape"},
 *          @OA\Property(property="num_immatriculation", type="string", example="2536"),
 *          @OA\Property(property="modele", type="string", example="FIAT"),
 *          @OA\Property(property="marque", type="date", example="PALIO"),
 *          @OA\Property(property="venant_de", type="string", example="Boulevard Haj Fatah"),
 *          @OA\Property(property="allant_vers", type="string", example="Meme sens"),
 *          @OA\Property(property="type", type="string", example="VOITURE")
 *      )
 *    ),
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */

/**
 * @OA\Get(
 * path="/api/v1/cons/dossier/vehicle/list/{id_constat}",
 * summary="List Vehicles",
 * description="List vehicles of a specified constat",
 * tags={"Constateur - Dossier"},
 * security={ {"apiAuth": {} }},
 * @OA\Parameter(
 *    description="ID of Constat",
 *    in="path",
 *    name="id_constat",
 *    required=true,
 *    example="1",
 *    @OA\Schema(
 *       type="integer",
 *       format="int64"
 *    )
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */

/**
 * @OA\Post(
 * path="/api/v1/cons/dossier/vehicle/assign_degats/{id_vehicle}",
 * summary="Assigns degats to Vehicle",
 * description="Assigns degats to Vehicle",
 * tags={"Constateur - Dossier"},
 * security={ {"apiAuth": {} }},
 * @OA\Parameter(
 *    description="ID of Vehicle",
 *    in="path",
 *    name="id_vehicle",
 *    required=true,
 *    example="1",
 *    @OA\Schema(
 *       type="integer",
 *       format="int64"
 *    )
 * ),
 * @OA\RequestBody(
 *    required=true,
 *    description="Pass constateur credentials",
 *    @OA\MediaType(
 *       mediaType="multipart/form-data",
 *       @OA\Schema(
 *          type="object",
 *          @OA\Property(property="point_choc", type="string", example="01"),
 *          @OA\Property(property="zone_choc", type="string", example="Face arrière,Par choc,Malle arrière"),
 *          @OA\Property(property="autres", type="string"),
 *          @OA\Property(
 *              property="photos[]",
 *              type="array",
 *              @OA\Items (
 *                  type="string",
 *                  format="binary"
 *              )
 *          ),
 *          @OA\Property(property="etape", type="string", example="ETAPE_DEGAT_A"),
 *       )
 *    ),
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */

/**
 * @OA\Post(
 * path="/api/v1/cons/dossier/vehicle/upload_degats/{id_constat}",
 * summary="Assigns Degats Photos to a Constat",
 * description="Assigns Degats Photos to Constat",
 * tags={"Constateur - Dossier"},
 * security={ {"apiAuth": {} }},
 * @OA\Parameter(
 *    description="ID of Constat",
 *    in="path",
 *    name="id_constat",
 *    required=true,
 *    example="1",
 *    @OA\Schema(
 *       type="integer",
 *       format="int64"
 *    )
 * ),
 * @OA\RequestBody(
 *    required=true,
 *    description="Pass constateur credentials",
 *    @OA\MediaType(
 *       mediaType="multipart/form-data",
 *       @OA\Schema(
 *          type="object",
 *          @OA\Property(
 *              property="photos[]",
 *              type="array",
 *              @OA\Items (
 *                  type="string",
 *                  format="binary"
 *              )
 *          )
 *       )
 *    ),
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */


/**
 * @OA\Get(
 * path="/api/v1/cons/dossier/vehicle/degats_photos/{id_constat}",
 * summary="List Degats Photos",
 * description="List degats photos of a specified constat",
 * tags={"Constateur - Dossier"},
 * security={ {"apiAuth": {} }},
 * @OA\Parameter(
 *    description="ID of Constat",
 *    in="path",
 *    name="id_constat",
 *    required=true,
 *    example="1",
 *    @OA\Schema(
 *       type="integer",
 *       format="int64"
 *    )
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */

/***************** CONSTATEUR-CONSTAT****************/
/**
 * @OA\Post(
 * path="/api/v1/cons/dossier/create",
 * summary="Create Dossier",
 * description="Create Dossier By Constateur",
 * tags={"Constateur - Dossier"},
 * security={ {"apiAuth": {} }},
 * @OA\RequestBody(
 *    required=true,
 *    description="Pass constateur credentials",
 *    @OA\JsonContent(
 *       @OA\Property(property="date_etablissement", type="date", example="2020-10-15"),
 *       @OA\Property(property="heure_etablissement", type="time", example="10:00:00"),
 *       @OA\Property(property="date_accident", type="date", example="2020-10-15"),
 *       @OA\Property(property="heure_accident", type="time", example=" 11:00:00"),
 *       @OA\Property(property="lieu", type="string", example="hey raha,beausejour"),
 *       @OA\Property(property="degats_materials", type="boolean", example="0"),
 *       @OA\Property(property="lat", type="string", example="28.921631"),
 *       @OA\Property(property="lng", type="string", example="-7.849476"),
 *       @OA\Property(property="code", type="string", example=" "),
 *    ),
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */
             /****** annulé constat *****/

/**
 * @OA\Post(
 * path="/api/v1/cons/dossier/annule/{id_constat}",
 * summary="Cancel Dossier",
 * description="Cancel Dossier By Constateur",
 * tags={"Constateur - Dossier"},
 * security={ {"apiAuth": {} }},
 * @OA\Parameter(
 *    description="ID of Constat",
 *    in="path",
 *    name="id_constat",
 *    required=true,
 *    example="1",
 *    @OA\Schema(
 *       type="integer",
 *       format="int64"
 *    )
 * ),
 * @OA\RequestBody(
 *    required=true,
 *    description="Pass constateur credentials",
 *    @OA\JsonContent(
 *       @OA\Property(property="motif", type="string", example="pas d'accident"),
 *    ),
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */

            /****** assignAssurés to constat *****/

/**
 * @OA\Post(
 * path="/api/v1/cons/dossier/assure/assign/{id_constat}",
 * summary="Create Assuré",
 * description="Create Assuré and assign it to a contstat",
 * tags={"Constateur - Dossier"},
 * security={ {"apiAuth": {} }},
 * @OA\Parameter(
 *     description="ID of Constat",
 *     in="path",
 *     name="id_constat",
 *     required=true,
 *     example="1",
 *     @OA\Schema(
 *       type="integer",
 *       format="int64"
 *     )
 * ),
 * @OA\RequestBody(
 *    required=true,
 *    description="Pass constateur credentials",
 *    @OA\JsonContent(
 *       @OA\Property(property="nom", type="string", example="akhdar"),
 *       @OA\Property(property="prenom", type="string", example="abderrahim"),
 *       @OA\Property(property="adresse", type="string", example="hey elhana,..."),
 *       @OA\Property(property="ste_assurance", type="string", example=" SAHAM"),
 *       @OA\Property(property="n_attestation", type="string", example="658972"),
 *       @OA\Property(property="n_police", type="string", example="4567882"),
 *       @OA\Property(property="attes_valide_du", type="date", example="2020-10-15"),
 *       @OA\Property(property="attes_valide_au", type="date", example="2020-10-15"),
 *       @OA\Property(property="agence", type="string", example="MASAHAM"),
 *       @OA\Property(property="type_usage", type="string", example="PARTICULIER")
 *    ),
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */

               /****** edit Assuré  *****/
/**
 * @OA\Post(
 * path="/api/v1/cons/dossier/assure/edit/{id_assure}",
 * summary="Edit Assuré",
 * description="Edit a Assuré",
 * tags={"Constateur - Dossier"},
 * security={ {"apiAuth": {} }},
 * @OA\Parameter(
 *     description="ID of Constat",
 *     in="path",
 *     name="id_assure",
 *     required=true,
 *     example="1",
 *     @OA\Schema(
 *       type="integer",
 *       format="int64"
 *     )
 * ),
 * @OA\RequestBody(
 *    required=true,
 *    description="Pass constateur credentials",
 *    @OA\JsonContent(
 *       @OA\Property(property="nom", type="string", example="akhdar"),
 *       @OA\Property(property="prenom", type="string", example="abderrahim"),
 *       @OA\Property(property="adresse", type="string", example="hey elhana,..."),
 *       @OA\Property(property="ste_assurance", type="string", example=" SAHAM"),
 *       @OA\Property(property="n_attestation", type="string", example="658972"),
 *       @OA\Property(property="n_police", type="string", example="4567882"),
 *       @OA\Property(property="attes_valide_du", type="date", example="2020-10-15"),
 *       @OA\Property(property="attes_valide_au", type="date", example="2020-10-15"),
 *       @OA\Property(property="agence", type="string", example="MASAHAM"),
 *       @OA\Property(property="type_usage", type="string", example="PARTICULIER")
 *    ),
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */

             /****** List Assurés  *****/
/**
 * @OA\Get(
 * path="/api/v1/cons/dossier/assure/list/{id_constat}",
 * summary="List Assurés",
 * description="List Assurés of a specified constat",
 * tags={"Constateur - Dossier"},
 * security={ {"apiAuth": {} }},
 * @OA\Parameter(
 *    description="ID of Constat",
 *    in="path",
 *    name="id_constat",
 *    required=true,
 *    example="1",
 *    @OA\Schema(
 *       type="integer",
 *       format="int64"
 *    )
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */

/******** create and assign Conducteurs to constat*******/

/**
 * @OA\Post(
 * path="/api/v1/cons/dossier/conducteur/assign/{id_constat}",
 * summary="Create Conducteur",
 * description="Create and assign conducteur to a contstat",
 * tags={"Constateur - Dossier"},
 * security={ {"apiAuth": {} }},
 * @OA\Parameter(
 *     description="ID of Constat",
 *     in="path",
 *     name="id_constat",
 *     required=true,
 *     example="1",
 *     @OA\Schema(
 *       type="integer",
 *       format="int64"
 *     )
 * ),
 * @OA\RequestBody(
 *    required=true,
 *    description="Pass constateur credentials",
 *    @OA\JsonContent(
 *       @OA\Property(property="nom", type="string", example="akhdar"),
 *       @OA\Property(property="prenom", type="string", example="abderrahim"),
 *       @OA\Property(property="adresse", type="string", example="hey elhana,..."),
 *       @OA\Property(property="type_permis", type="string", example=" B "),
 *       @OA\Property(property="num_permis", type="string", example="658972"),
 *       @OA\Property(property="prefecture_delivrance", type="string", example=""),
 *       @OA\Property(property="date_delivrance", type="date", example="2013-10-15"),
 *       @OA\Property(property="delai_validite", type="date", example="2020-10-15"),
 *    ),
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */
               /****** edit Conducteur  *****/


/**
 * @OA\Post(
 * path="/api/v1/cons/dossier/conducteur/edit/{id_conducteur}",
 * summary="Edit Conducteur",
 * description="Edit a Conducteur",
 * tags={"Constateur - Dossier"},
 * security={ {"apiAuth": {} }},
 * @OA\Parameter(
 *     description="ID of Conducteur",
 *     in="path",
 *     name="id_conducteur",
 *     required=true,
 *     example="1",
 *     @OA\Schema(
 *       type="integer",
 *       format="int64"
 *     )
 * ),
 * @OA\RequestBody(
 *    required=true,
 *    description="Pass constateur credentials",
 *    @OA\JsonContent(
 *       @OA\Property(property="nom", type="string", example="akhdar"),
 *       @OA\Property(property="prenom", type="string", example="abderrahim"),
 *       @OA\Property(property="adresse", type="string", example="hey elhana,..."),
 *       @OA\Property(property="type_permis", type="string", example=" B "),
 *       @OA\Property(property="num_permis", type="string", example="658972"),
 *       @OA\Property(property="prefecture_delivrance", type="string", example=""),
 *       @OA\Property(property="date_delivrance", type="date", example="2013-10-15"),
 *       @OA\Property(property="delai_validite", type="date", example="2020-10-15"),
 *    ),
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */

              /****** List Conducteurs  *****/
/**
 * @OA\Get(
 * path="/api/v1/cons/dossier/conducteur/list/{id_constat}",
 * summary="List Conducteurs",
 * description="List Conducteurs of a specified constat",
 * tags={"Constateur - Dossier"},
 * security={ {"apiAuth": {} }},
 * @OA\Parameter(
 *    description="ID of Constat",
 *    in="path",
 *    name="id_constat",
 *    required=true,
 *    example="1",
 *    @OA\Schema(
 *       type="integer",
 *       format="int64"
 *    )
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */

/********  sign constat by a conducteur *******/

/**
 * @OA\Post(
 * path="/api/v1/cons/dossier/conducteur/sign/{id_conducteur}",
 * summary="Sign Constat",
 * description="Sign contstat by a conducteur",
 * tags={"Constateur - Dossier"},
 * security={ {"apiAuth": {} }},
 * @OA\Parameter(
 *     description="ID of Conducteur",
 *     in="path",
 *     name="id_conducteur",
 *     required=true,
 *     example="1",
 *     @OA\Schema(
 *       type="integer",
 *       format="int64"
 *     )
 * ),
 * @OA\RequestBody(
 *    required=true,
 *    description="Pass constateur credentials",
 *    @OA\MediaType(
 *       mediaType="multipart/form-data",
 *       @OA\Schema(
 *          type="object",
 *          @OA\Property(
 *              property="signature",
 *              type="file",
 *              @OA\Items (
 *                  type="string",
 *                  format="binary"
 *              )
 *          ),
 *          @OA\Property(property="etape", type="string", example="ETAPE_SIGN_A"),
 *       )
 *    ),
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */

       /****** List signsConducteurs  *****/
/**
 * @OA\Get(
 * path="/api/v1/cons/dossier/conducteur/listSigns/{id_constat}",
 * summary="List Conducteurs signs of a constat",
 * description="List Conducteurs signs of a specified constat",
 * tags={"Constateur - Dossier"},
 * security={ {"apiAuth": {} }},
 * @OA\Parameter(
 *    description="ID of Constat",
 *    in="path",
 *    name="id_constat",
 *    required=true,
 *    example="1",
 *    @OA\Schema(
 *       type="integer",
 *       format="int64"
 *    )
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */





/********  add circonstances to a workflow *******/


/**
 * @OA\Post(
 * path="/api/v1/cons/dossier/circonstances/assign/{id_constat}",
 * summary="Create circonstances",
 * description="add circonstances to a workflow",
 * tags={"Constateur - Dossier"},
 * security={ {"apiAuth": {} }},
 * @OA\Parameter(
 *     description="ID of Constat",
 *     in="path",
 *     name="id_constat",
 *     required=true,
 *     example="1",
 *     @OA\Schema(
 *       type="integer",
 *       format="int64"
 *     )
 * ),
 * @OA\RequestBody(
 *    required=true,
 *    description="Pass constateur credentials",
 *    @OA\JsonContent(
 *          @OA\Property(type="string", property="etape", example="ETAPE_CIRCONSTANCE"),
 *          @OA\Property(
 *              property="circonstances",
 *              type="array",
 *              @OA\Items(
 *                  type="object",
 *                  @OA\Property(type="integer", property="id_vehicle", example=51),
 *                  @OA\Property(type="array", collectionFormat="multi", property="values",
 *                      @OA\Items(
 *                          type="string",
 *                          example="HEURTAIT_ARRIERE_MEME_SENS_MEME_FILE"
 *                      )
 *                  )
 *              )
 *          ),
 *    ),
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */
        /****** List Circonstances  *****/
/**
 * @OA\Get(
 * path="/api/v1/cons/dossier/circonstances/list/{id_constat}",
 * summary="List Circonstances",
 * description="List Circonstances of a specified constat",
 * tags={"Constateur - Dossier"},
 * security={ {"apiAuth": {} }},
 * @OA\Parameter(
 *    description="ID of Constat",
 *    in="path",
 *    name="id_constat",
 *    required=true,
 *    example="1",
 *    @OA\Schema(
 *       type="integer",
 *       format="int64"
 *    )
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */

   /********  add croquis to a workflow *******/


/**
 * @OA\Post(
 * path="/api/v1/cons/dossier/croquis/upload/{id_constat}",
 * summary="Create croquis",
 * description="add croquis to a workflow",
 * tags={"Constateur - Dossier"},
 * security={ {"apiAuth": {} }},
 * @OA\Parameter(
 *     description="ID of Constat",
 *     in="path",
 *     name="id_constat",
 *     required=true,
 *     example="1",
 *     @OA\Schema(
 *       type="integer",
 *       format="int64"
 *     )
 * ),
 * @OA\RequestBody(
 *    required=true,
 *    description="Pass constateur credentials",
 *    @OA\MediaType(
 *       mediaType="multipart/form-data",
 *       @OA\Schema(
 *          type="object",
 *          @OA\Property(
 *              property="croquis",
 *              type="file",
 *              @OA\Items (
 *                  type="string",
 *                  format="binary"
 *              )
 *          )
 *       )
 *    ),
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */

       /****** Get Croquis  *****/
/**
 * @OA\Get(
 * path="/api/v1/cons/dossier/croquis/get/{id_constat}",
 * summary="List croquis",
 * description="List croquis of a specified constat",
 * tags={"Constateur - Dossier"},
 * security={ {"apiAuth": {} }},
 * @OA\Parameter(
 *    description="ID of Constat",
 *    in="path",
 *    name="id_constat",
 *    required=true,
 *    example="1",
 *    @OA\Schema(
 *       type="integer",
 *       format="int64"
 *    )
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */
/************************ Email ****************/

/**
 * @OA\Post(
 * path="/api/v1/cons/dossier/email_constat/{id_constat}",
 * summary="Send Constat ",
 * description="Send Constat to the assurés and the assurances",
 * tags={"Constat - Email"},
 * security={ {"apiAuth": {} }},
 * @OA\Parameter(
 *     description="ID of Constat",
 *     in="path",
 *     name="id_constat",
 *     required=true,
 *     example="1",
 *     @OA\Schema(
 *       type="integer",
 *       format="int64"
 *     )
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */


/************* Recapitulatif **************/

/**
 * @OA\Get(
 * path="/api/v1/cons/dossier/summary/{id_constat}",
 * summary="Constat Summary",
 * description="The Constat Summary",
 * tags={"Constateur - Dossier"},
 * security={ {"apiAuth": {} }},
 * @OA\Parameter(
 *    description="ID of Constat",
 *    in="path",
 *    name="id_constat",
 *    required=true,
 *    example="1",
 *    @OA\Schema(
 *       type="integer",
 *       format="int64"
 *    )
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */


