<?php
/**
 * Created by PhpStorm.
 * User: AZMed
 * Date: 12/10/2020
 * Time: 11:11
 */

/************************ Assistance ****************/

/**
 * @OA\Get(
 * path="/api/v1/ass/dossier/list",
 * summary="List dossiers",
 * description="List of dossiers in BO Assistance",
 * tags={"BO Assistance"},
 * security={ {"apiAuth": {} }},
 * @OA\Response(
 *    response=200,
 *    description="Success"
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */

/**
 * @OA\Get(
 * path="/api/v1/ass/dossier/get/{id}",
 * summary="Details Dossier",
 * description="Details dossier in BO Assistance",
 * tags={"BO Assistance"},
 * security={ {"apiAuth": {} }},
 * @OA\Parameter(
 *    description="ID of dossier",
 *    in="path",
 *    name="id",
 *    required=true,
 *    example="1",
 *    @OA\Schema(
 *       type="integer",
 *       format="int64"
 *    )
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */

/**
 * @OA\Post(
 * path="/api/v1/ass/dossier/search",
 * summary="Search Dossier",
 * description="Search dossier in BO Assistance",
 * tags={"BO Assistance"},
 * security={ {"apiAuth": {} }},
 * @OA\RequestBody(
 *    required=true,
 *    description="Pass user credentials",
 *    @OA\JsonContent(
 *       @OA\Property(property="n_dossier", type="integer", example="1256"),
 *       @OA\Property(property="num_immatriculation", type="string", example="23659"),
 *       @OA\Property(property="perimetre", type="string", example="URBAN"),
 *       @OA\Property(property="date_debut", type="string", format="date-time", example="2020-09-01"),
 *       @OA\Property(property="date_fin", type="string", format="date-time", example="2020-30-11"),
 *    ),
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */

/**
 * @OA\Post(
 * path="/api/v1/ass/dossier/validate/{id}",
 * summary="Search Dossier",
 * description="Search dossier in BO Assistance",
 * tags={"BO Assistance"},
 * security={ {"apiAuth": {} }},
 * @OA\Parameter(
 *    description="ID of dossier",
 *    in="path",
 *    name="id",
 *    required=true,
 *    example="1",
 *    @OA\Schema(
 *       type="integer",
 *       format="int64"
 *    )
 * ),
 * @OA\RequestBody(
 *    required=true,
 *    description="Change dossier status",
 *    @OA\JsonContent(
 *       required={"etat"},
 *       @OA\Property(property="etat", type="string", example="FINISHED"),
 *    ),
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */


/*********************** Assurance **********************/

/**
 * @OA\Get(
 * path="/api/v1/assu/dossier/list",
 * summary="List dossiers",
 * description="List of dossiers in BO Assurance",
 * tags={"BO Assurance"},
 * security={ {"apiAuth": {} }},
 * @OA\Response(
 *    response=200,
 *    description="Success"
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */

/**
 * @OA\Get(
 * path="/api/v1/assu/dossier/get/{id}",
 * summary="Details Dossier",
 * description="Details dossier in BO Assurance",
 * tags={"BO Assurance"},
 * security={ {"apiAuth": {} }},
 * @OA\Parameter(
 *    description="ID of dossier",
 *    in="path",
 *    name="id",
 *    required=true,
 *    example="1",
 *    @OA\Schema(
 *       type="integer",
 *       format="int64"
 *    )
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */

/**
 * @OA\Post(
 * path="/api/v1/assu/dossier/search",
 * summary="Search Dossier",
 * description="Search dossier in BO Assurance",
 * tags={"BO Assurance"},
 * security={ {"apiAuth": {} }},
 * @OA\RequestBody(
 *    required=true,
 *    description="Pass user credentials",
 *    @OA\JsonContent(
 *       @OA\Property(property="n_dossier", type="integer", example="1256"),
 *       @OA\Property(property="num_immatriculation", type="string", example="23659"),
 *       @OA\Property(property="perimetre", type="string", example="URBAN"),
 *       @OA\Property(property="date_debut", type="string", format="date-time", example="2020-09-01"),
 *       @OA\Property(property="date_fin", type="string", format="date-time", example="2020-30-11"),
 *    ),
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */

/**
 * @OA\Post(
 * path="/api/v1/assu/dossier/download/{id}",
 * summary="Download Dossier's Images",
 * description="Download dossier's images in BO Assurance",
 * tags={"BO Assurance"},
 * security={ {"apiAuth": {} }},
 * @OA\Parameter(
 *    description="ID of dossier",
 *    in="path",
 *    name="id",
 *    required=true,
 *    example="1",
 *    @OA\Schema(
 *       type="integer",
 *       format="int64"
 *    )
 * ),
 * @OA\RequestBody(
 *    required=true,
 *    description="Pass user credentials",
 *    @OA\JsonContent(
 *       @OA\Property(property="elem", type="string", example=""),
 *    ),
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */


/************************* Prestataire *****************************/

/**
 * @OA\Get(
 * path="/api/v1/pre/dossier/list",
 * summary="List dossiers",
 * description="List of dossiers in BO Prestataire",
 * tags={"BO Prestataire"},
 * security={ {"apiAuth": {} }},
 * @OA\Response(
 *    response=200,
 *    description="Success"
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */

/**
 * @OA\Get(
 * path="/api/v1/pre/dossier/get/{id}",
 * summary="Details Dossier",
 * description="Details dossier in BO Prestataire",
 * tags={"BO Prestataire"},
 * security={ {"apiAuth": {} }},
 * @OA\Parameter(
 *    description="ID of dossier",
 *    in="path",
 *    name="id",
 *    required=true,
 *    example="1",
 *    @OA\Schema(
 *       type="integer",
 *       format="int64"
 *    )
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */

/**
 * @OA\Post(
 * path="/api/v1/pre/dossier/search",
 * summary="Search Dossier",
 * description="Search dossier in BO Prestataire",
 * tags={"BO Prestataire"},
 * security={ {"apiAuth": {} }},
 * @OA\RequestBody(
 *    required=true,
 *    description="Pass user credentials",
 *    @OA\JsonContent(
 *       @OA\Property(property="n_dossier", type="integer", example="1256"),
 *       @OA\Property(property="num_immatriculation", type="string", example="23659"),
 *       @OA\Property(property="perimetre", type="string", example="URBAN"),
 *       @OA\Property(property="date_debut", type="string", format="date-time", example="2020-09-01"),
 *       @OA\Property(property="date_fin", type="string", format="date-time", example="2020-30-11"),
 *    ),
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */

/**
 * @OA\Post(
 * path="/api/v1/pre/dossier/download/{id}",
 * summary="Download Dossier's Images",
 * description="Download dossier's images in BO Prestataire",
 * tags={"BO Prestataire"},
 * security={ {"apiAuth": {} }},
 * @OA\Parameter(
 *    description="ID of dossier",
 *    in="path",
 *    name="id",
 *    required=true,
 *    example="1",
 *    @OA\Schema(
 *       type="integer",
 *       format="int64"
 *    )
 * ),
 * @OA\RequestBody(
 *    required=true,
 *    description="Pass user credentials",
 *    @OA\JsonContent(
 *       @OA\Property(property="elem", type="string", example=""),
 *    ),
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */









