<?php

namespace App\Modules\Dossier\Http\Controllers\ASSU;

use App\Enums\eConstatStatut;
use App\Enums\ePerimetre;
use App\Http\Controllers\Controller;
use App\Models\Constat;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Enums\eResponseCode;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;

class DossierController extends Controller
{

    /**
     * Display a listing of Dossier
     *
     * @return \Illuminate\Http\Response
     */

    public function all(Request $request,$limit = null)
    {
        if($limit) $constats = Constat::whereHas('assures.assurance' , function($q) use($request){
            $q->where('id_assurance','=',$request->user()->id_assurance);
        })->with(['constateur.prestataire', 'assures.assurance', 'workflow', 'vehicules', 'linked_constats'])->limit($limit)->get();
        else $constats = Constat::whereHas('assures.assurance' , function($q) use($request){
            $q->where('id_assurance','=',$request->user()->id_assurance);
        })->with(['constateur.prestataire', 'assures.assurance'=>function($query) use ($request){
            $query->where('id_assurance','=',$request->user()->id_assurance);
        }, 'workflow', 'vehicules','linked_constats'])->get();

        return $this->resp->ok(eResponseCode::CT_LISTED_200_00, $constats);


    }

    /**
     * Display the specified Dossier.
     *
     * @param Constat $constat
     * @return \DevcorpIt\ResponseCode\JsonResp
     */

    public function get(Request $request,Constat $constat)
    {
        if(!$constat) return $this->resp->ok(eResponseCode::CT_404_NOT_FOUND);

        $constat = $constat->load([
            'constateur.prestataire',
            'workflow',
            'assures.assurance',
            'conducteurs',
            'vehicules',
            'degat',
            'linked_constats.assures.assurance',
            'linked_constats.conducteurs'
        ]);
        $constat['recap'] = $this->verifyExistFile($constat['code'], $constat['id_constat']);
        if($constat['linked_constats']) {
            $constat['linked_constats']->each(function ($item) {
                $item['recap'] = $this->verifyExistFile($item['code'], $item['id_constat']);
            });
        }

        return $this->resp->ok(eResponseCode::CT_GET_200_01, $constat);
    }

    /**
     *
     * @param Request $request
     * @return \DevcorpIt\ResponseCode\JsonResp
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function search(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'code' => 'nullable',
            'ville' => 'nullable',
            'date_etablissement' => 'nullable|array',
            'num_immatriculation' => 'nullable',
        ]);

        if ($validator->fails()){
            return $this->resp->bad_request($validator);
        }

        $constats = Constat::whereHas('assures.assurance' , function($q) use($request){
            $q->where('id_assurance','=',$request->user()->id_assurance);
        })->with(['constateur.prestataire', 'assures.assurance.assistance', 'workflow', 'vehicules'])
            ->when(!empty($request->code),function($q) use($request){
                $q->where('code', $request->code);
            })->when(!empty($request->ville), function($q) use($request){
                $q->whereHas('workflow', function($q) use($request){
                    $q->where('ville', $request->ville);
                });
            })->when(!empty($request->date_etablissement),function($q) use($request){
                if(count($request->date_etablissement) > 1) {
                    $dates = $request->date_etablissement;
                    sort($dates);
                    $q->whereHas('workflow', function($q) use($dates){
                        $q->whereBetween('date_etablissement', $dates);
                    });
                } else {
                    $q->whereHas('workflow', function($q) use($request){
                        $q->where('date_etablissement', $request->date_etablissement);
                    });
                }
            })->when(!empty($request->num_immatriculation),function($q) use($request){
                $q->whereHas('vehicules', function($q) use($request){
                    $q->where('num_immatriculation', $request->num_immatriculation);
                });
            })->when(!empty($request->prestataire),function($q) use($request){
                $q->whereHas('constateur.prestataire', function($q) use($request){
                    $q->where('id_prestataire', $request->prestataire);
                });
            })->get();

        return $this->resp->ok(eResponseCode::CT_LISTED_200_00, $constats);

    }


    /**
     * @param Constat $constat
     * @param Request $request
     * @return $this|\Illuminate\Http\JsonResponse|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function downloadDegats(Constat $constat, Request $request) {
        $validator = Validator::make($request->all(), [
            'elem' => 'string',
        ]);

        if ($validator->fails()){
            return $this->resp->bad_request($validator);
        }
        $file = $request->elem;

        if(in_array($file,json_decode($constat->workflow->photos, true))){
            return response()->download(public_path('storage\\cdn\\constateurs\\degats\\') . $constat->id_constat . '\\' . $file);
        }
    }

    /**
     * @param $code_constat
     * @param $id_constat
     * @return bool
     */
    public function verifyExistFile($code_constat, $id_constat)
    {
        $filename = 'constat-' . $code_constat . '-' . $id_constat . '.pdf';
        $path = (config('cdn.recap.path') . '/' . $filename);
        $pdf_file = public_path() . '/storage' . $path;
        return file_exists($pdf_file);
    }


}
