<?php

namespace App\Modules\Dossier\Http\Controllers\CONSTATEUR;

use Anam\PhantomLinux\Path;
use Anam\PhantomMagick\Converter;
use App\Enums\eConstatStatut;
use App\Enums\ePerimetre;
use App\Enums\eStepConstat;
use App\Enums\eTypeUsage;
use App\Enums\eTypeVehicle;
use App\Http\Controllers\Controller;
use App\Libs\Tools\UploadTrait;
use App\Libs\Traits\ReverseGeoCoding;
use App\Models\Assure;
use App\Models\Constat;
use App\Models\Constateur;
use App\Models\Vehicule;
use App\Models\WorkFlow;
use App\Models\Conducteur;
use App\Rules\TextCsv;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Enums\eResponseCode;
use Illuminate\Validation\Rule;
use Mpdf\Mpdf;
use PDF;
use Illuminate\Support\Facades\Storage;
use Spatie\Browsershot\Browsershot;
use Spatie\Image\Manipulations;
use App\Rules\NumberCsv;
use Notification;
use App\Modules\Template\Services\TemplateNotificationService;

class DossierController extends Controller
{
    use UploadTrait,ReverseGeoCoding;

    public function create(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'date_etablissement' => 'required|date',
            'heure_etablissement' => 'required|date_format:H:i:s',
            'date_accident' => 'required|date',
            'heure_accident' => 'required|date_format:H:i:s',
            'lieu' => 'required|string|max:255',
            'degats_materials' => 'required|boolean',
            'lat'=>'required|string',
            'lng'=>'required|string',
            'code'=>'required|string'
        ]);

        if ($validator->fails()) {
            return $this->resp->bad_request($validator);
        }

        $data = $request->all();
        $data['ville']= $this->reverseGeoCoding($data['lat'],$data['lng']);
        $workflow =new WorkFlow($data);
        $workflow->save();


        if(!empty($data['degats_materials'])){
            $data_constat['etat']= eConstatStatut::CANCELED;
            $data_constat['motif']= "Présence d'autres degats";
        }

        $data_constat['code'] = $data['code'];
        $data_constat['id_workflow'] = $workflow->id_workflow;
        $data_constat['id_constateur'] = $request->user()->id_constateur;

        $constat = new Constat($data_constat);
        $constat->save();

        return $this->resp->ok(eResponseCode::CT_CREATE_200_02, $constat->load('workflow'));
    }


    /**
     * Cancel a constat
     * @param Constat $constat
     * @param Request $request
     * @return $this|\Illuminate\Http\JsonResponse
     */

    public function cancelConstat(Constat $constat,Request $request )
    {
        if(!$constat) return $this->resp->not_found(eResponseCode::CT_404_NOT_FOUND, $constat);

        $validator = Validator::make($request->all(), [
            'motif' => 'required|string',
        ]);

        if ($validator->fails()) {
            return $this->resp->bad_request($validator);
        }
        $data = $request->all();

        if(!empty($data['motif'])){
            $data_constat['etat']= eConstatStatut::CANCELED;
            $data_constat['motif']= $data['motif'];
        }
        $constat->update($data_constat);
        $constat->save();
        return $this->resp->ok(eResponseCode::CT_CANCEL_200_06, $constat);
    }


    /**
     * @param Constat $constat
     * @param Request $request
     * @return $this|\Illuminate\Http\JsonResponse
     */

    public function assignAssure(Constat $constat,Request $request){

        $validator = Validator::make($request->all(), [
            'nom' => 'required|string',
            'prenom' => 'required|string',
            'adresse' => 'required|string',
            'ste_assurance' => 'required|string',
            'n_attestation' => 'required|string',
            'n_police' => 'required|string|max:150',
            'attes_valide_du'=>'required|date',
            'attes_valide_au'=>'required|date',
            'agence'=> 'required|string|max:150',
            'etape' => ['string', 'required', Rule::in(eStepConstat::getAll(eStepConstat::class))],
            //'type_usage' => ['string', 'required', Rule::in(eTypeUsage::getAll(eTypeUsage::class))]
        ]);

        if ($validator->fails()) {
            return $this->resp->bad_request($validator);
        }

        $data = $request->all();
        //$data['attes_valide_du']= \DateTime::createFromFormat('d/m/Y', urldecode($data['attes_valide_du']))->format('Y-m-d');
        //$data['attes_valide_au']= \DateTime::createFromFormat('d/m/Y', urldecode($data['attes_valide_au']))->format('Y-m-d');


        $assure = new Assure($data);
        $assure->id_constat = $constat->id_constat;
        $assure->save();

        if($assure && $request->etape){
            $constat->changeStatus($request->etape);
        }

        return $this->resp->ok(eResponseCode::ASU_CREATE_200_02, $assure);

    }


    /**
     * @param Assure $assure
     * @param Request $request
     * @return $this|\Illuminate\Http\JsonResponse
     */

    public function editAssure(Assure $assure,Request $request){
        $validator = Validator::make($request->all(), [
            'nom' => 'required|string',
            'prenom' => 'required|string',
            'adresse' => 'required|string',
            'ste_assurance' => 'required|string',
            'n_attestation' => 'required|string',
            'n_police' => 'required|string|max:150',
            'attes_valide_du'=>'required|date',
            'attes_valide_au'=>'required|date',
            'agence'=> 'required|string|max:150',
            //'type_usage' => ['string', 'required', Rule::in(eTypeUsage::getAll(eTypeUsage::class))]
        ]);

        if ($validator->fails()) {
            return $this->resp->bad_request($validator);
        }

        $data = $request->all();

        $assure->update($data);
        $assure->save();

        return $this->resp->ok(eResponseCode::ASU_UPDATE_200_03, $assure);

    }

    /**
     * @param Constat $constat
     * @param Request $request
     * @return $this|\Illuminate\Http\JsonResponse
     */

    public function listAssures(Constat $constat, Request $request) {
        if(!$constat) return $this->resp->not_found(eResponseCode::CT_404_NOT_FOUND, $constat);

        $assures = Assure::where('id_constat', $constat->id_constat)->get();
        return $this->resp->ok(eResponseCode::ASU_LISTED_200_00, $assures);
    }


    /**
     * @param Constat $constat
     * @param Request $request
     * @return $this|\Illuminate\Http\JsonResponse
     */

    public function assignConducteur(Constat $constat,Request $request){

        $validator = Validator::make($request->all(), [
            'nom' => 'required|string',
            'prenom' => 'required|string',
            'adresse' => 'required|string',
            'type_permis' => 'required|string',
            'num_permis' => 'required|string',
            'date_delivrance' => 'required|date',
            'prefecture_delivrance'=>'required|string|max:150',
            'delai_validite'=>'required|date',
            'etape' => ['string', 'required', Rule::in(eStepConstat::getAll(eStepConstat::class))]
        ]);

        if ($validator->fails()) {
            return $this->resp->bad_request($validator);
        }

        $data = $request->all();

        $conducteur = new Conducteur($data);
        $conducteur->id_constat = $constat->id_constat;
        $conducteur->save();

        if($conducteur && $request->etape){
            $constat->changeStatus($request->etape);
        }

        return $this->resp->ok(eResponseCode::COND_CREATE_200_02, $conducteur);

    }

    /**
     * @param Conducteur $conducteur
     * @param Request $request
     * @return $this|\Illuminate\Http\JsonResponse
     */

    public function editConducteur(Conducteur $conducteur,Request $request){
        $validator = Validator::make($request->all(), [
            'nom' => 'required|string',
            'prenom' => 'required|string',
            'adresse' => 'required|string',
            'type_permis' => 'required|string',
            'num_permis' => 'required|string',
            'date_delivrance' => 'required|date',
            'prefecture_delivrance'=>'required|string|max:150',
            'delai_validite'=>'required|date',
        ]);

        if ($validator->fails()) {
            return $this->resp->bad_request($validator);
        }

        $data = $request->all();
        $conducteur->update($data);
        $conducteur->save();

        return $this->resp->ok(eResponseCode::COND_UPDATE_200_03, $conducteur);

    }



    /**
     * @param Conducteur $conducteur
     * @param Request $request
     * @return $this|\Illuminate\Http\JsonResponse
     */

    public function signConducteur(Conducteur $conducteur,Request $request){
        if(!$conducteur) return $this->resp->not_found(eResponseCode::COND_404_NOT_FOUND, $conducteur);

        $validator = Validator::make($request->all(), [
            'signature' => 'required|image|mimes:jpeg,png,jpg,gif',
            'etape' => ['string', 'required', Rule::in(eStepConstat::getAll(eStepConstat::class))]
        ]);
        if ($validator->fails()) {
            return $this->resp->bad_request($validator);
        }
        if ($request->hasFile('signature')) {
            $signature=$request->file('signature');
            $upload = $this->uploadOne($signature, config('cdn.signatures.path') . DIRECTORY_SEPARATOR . $conducteur->id_constat . DIRECTORY_SEPARATOR . $conducteur->id_conducteur );
            $image['filepath'] = $upload['file_name'];
            $image['name'] = $signature->getClientOriginalName();
            $data=['signature'=>json_encode($image)];
            $conducteur->update($data);
            $conducteur->save();

            if($conducteur && $request->etape){
                $constat = $conducteur->constat;
                $constat->changeStatus($request->etape);
            }

            return $this->resp->ok(eResponseCode::SIG_CREATE_200_02, $conducteur);
        }

    }


    /**
     * @param Constat $constat
     * @param Request $request
     * @return $this|\Illuminate\Http\JsonResponse
     */

    public function listSignsConducteurs(Constat $constat, Request $request) {
        if(!$constat) return $this->resp->not_found(eResponseCode::CT_404_NOT_FOUND, $constat);

        $signatures = Conducteur::select('id_conducteur','signature')->where('id_constat', $constat->id_constat)->get();
        return $this->resp->ok(eResponseCode::SIG_LISTED_200_00, $signatures);
    }


    /**
     * @param Constat $constat
     * @param Request $request
     * @return $this|\Illuminate\Http\JsonResponse
     */

    public function listConducteurs(Constat $constat, Request $request) {
        if(!$constat) return $this->resp->not_found(eResponseCode::CT_404_NOT_FOUND, $constat);

        $conducteurs = Conducteur::where('id_constat', $constat->id_constat)->get();
        return $this->resp->ok(eResponseCode::COND_LISTED_200_00, $conducteurs);
    }


    /**
     * @param Constat $constat
     * @param Request $request
     * @return $this|\Illuminate\Http\JsonResponse
     */

    public function assignCirconstances(Constat $constat,Request $request){

        $validator = Validator::make($request->all(), [
           'circonstances'=>'required|array|max:2',
            'circonstances.*.id_vehicle'=>'required|integer',
            'circonstances.*.values'=>'required|array',
            'etape' => ['string', 'required', Rule::in(eStepConstat::getAll(eStepConstat::class))]
        ]);

        if ($validator->fails()) {
            return $this->resp->bad_request($validator);
        }
        $data = $request->all();
        $circonstances=json_encode($data['circonstances']);
        $workflow=Workflow::where('id_workflow',$constat->id_workflow)->first();
        $workflow->circonstances_accident =$circonstances;
        $workflow->etape = $request->etape;
        $workflow->save();

        /*if($workflow && $request->etape){
            $constat->changeStatus($request->etape);
        }*/

        return $this->resp->ok(eResponseCode::CIRCO_UPDATE_200_03,$workflow);

    }


    /**
     * @param Constat $constat
     * @param Request $request
     * @return $this|\Illuminate\Http\JsonResponse
     */

    public function listCirconstances(Constat $constat, Request $request) {
        if(!$constat) return $this->resp->not_found(eResponseCode::CT_404_NOT_FOUND, $constat);

        $circonstances = WorkFlow::select('circonstances_accident')->where('id_workflow', $constat->id_workflow)->get();
        return $this->resp->ok(eResponseCode::CIRCO_LISTED_200_00, $circonstances);
    }


    /**
     * @param Constat $constat
     * @param Request $request
     * @return $this|\Illuminate\Http\JsonResponse
     */

    public function assignVehicle(Constat $constat, Request $request)
    {

        $validator = Validator::make($request->all(), [
            'num_immatriculation' => 'required|integer',
            'modele' => 'required|string|max:150',
            'marque' => 'required|string|max:150',
            'venant_de' => 'required|string|max:300',
            'allant_vers' => 'required|string|max:300',
            'etape' => ['string', 'required', Rule::in(eStepConstat::getAll(eStepConstat::class))],
            'type' => ['string', 'nullable', Rule::in(eTypeVehicle::getAll(eTypeVehicle::class))]
        ]);

        if ($validator->fails()) {
            return $this->resp->bad_request($validator);
        }

        $data = $request->all();
        $vehicle = new Vehicule($data);
        $vehicle->id_constat = $constat->id_constat;
        $vehicle->save();

        if($vehicle && $request->etape){
            $constat->changeStatus($request->etape);
        }

        return $this->resp->ok(eResponseCode::V_LISTED_200_00, $vehicle);
    }


    /**
     * @param Vehicule $vehicule
     * @param Request $request
     * @return $this|\Illuminate\Http\JsonResponse
     */
    public function editVehicle(Vehicule $vehicule, Request $request)
    {
        if(!$vehicule) return $this->resp->not_found(eResponseCode::V_404_NOT_FOUND, $vehicule);

        $validator = Validator::make($request->all(), [
            'num_immatriculation' => 'integer',
            'modele' => 'string|max:150',
            'marque' => 'string|max:150',
            'venant_de' => 'string|max:300',
            'allant_vers' => 'string|max:300',
            'type' => ['string', 'nullable', Rule::in(eTypeVehicle::getAll(eTypeVehicle::class))]
        ]);

        if ($validator->fails()) {
            return $this->resp->bad_request($validator);
        }

        $data = $request->all();
        $vehicule->update($data);
        $vehicule->save();

        return $this->resp->ok(eResponseCode::V_UPDATE_200_03, $vehicule);
    }

    /**
     * @param Constat $constat
     * @param Request $request
     * @return $this|\Illuminate\Http\JsonResponse
     */
    public function listVehicles(Constat $constat, Request $request) {
        if(!$constat) return $this->resp->not_found(eResponseCode::CT_404_NOT_FOUND, $constat);

        $vehicles = Vehicule::where('id_constat', $constat->id_constat)->get();
        return $this->resp->ok(eResponseCode::V_LISTED_200_00, $vehicles);
    }

    /**
     * @param Vehicule $vehicule
     * @param Request $request
     * @return $this|\Illuminate\Http\JsonResponse
     */
    public function assignDegats(Vehicule $vehicule, Request $request) {
        if(!$vehicule) return $this->resp->not_found(eResponseCode::V_404_NOT_FOUND, $vehicule);

        $validator = Validator::make($request->all(), [
            'point_choc' => 'string|max:250|nullable',
            'zone_choc' => [new TextCsv(), 'nullable'],
            'autres' => 'string|nullable',
            'etape' => ['string', 'required', Rule::in(eStepConstat::getAll(eStepConstat::class))]
        ]);

        if ($validator->fails()) {
            return $this->resp->bad_request($validator);
        }

        $data = $request->all();
        if(isset($data['zone_choc']) && !empty($data['zone_choc'])) $data['zone_choc'] = json_encode($data['zone_choc']);

        $vehicule->update($data);
        $vehicule->save();

        if($vehicule && $request->etape){
            $v = Vehicule::find($vehicule->id_vehicle);
            $constat = Constat::find($v->id_constat);
            $constat->changeStatus($request->etape);
            //
        }

        return $this->resp->ok(eResponseCode::V_GET_200_01, $vehicule);
    }


    /**
     * @param Constat $constat
     * @param Request $request
     * @return $this|\Illuminate\Http\JsonResponse
     */

    public function uploadCroquis(Constat $constat, Request $request)
    {
        if(!$constat) return $this->resp->not_found(eResponseCode::CT_404_NOT_FOUND, $constat);

        $validator = Validator::make($request->all(), [
            'croquis' => 'image|mimes:jpeg,png,jpg,gif'
        ]);

        if ($validator->fails()) {
            return $this->resp->bad_request($validator);
        }

        if ($request->hasFile('croquis')) {
                 $croquis=$request->file('croquis');
                 $upload = $this->uploadOne($croquis, config('cdn.croquis.path') . DIRECTORY_SEPARATOR . $constat->id_constat);
                 $image['filepath'] = $upload['file_name'];
                 $image['name'] = $croquis->getClientOriginalName();
                 $data = ['croquis' => json_encode($image), 'etape' => eStepConstat::ETAPE_CROQUIS];
                 $constat->workflow->update($data);
                 $constat->save();

            return $this->resp->ok(eResponseCode::CROQ_UPLOAD_200_07);
        }
        return $this->resp->ok(eResponseCode::CROQ_404_NOT_UPLOADED);
    }



    /**
     * @param Constat $constat
     * @param Request $request
     * @return $this|\Illuminate\Http\JsonResponse
     */

    public function getCroquis(Constat $constat, Request $request)
    {
        if(!$constat) return $this->resp->not_found(eResponseCode::CT_404_NOT_FOUND, $constat);

        $croquis = WorkFlow::select('croquis')->where('id_workflow',$constat->id_workflow)->get();
        return $this->resp->ok(eResponseCode::CROQ_GET_200_01,json_decode($croquis));
    }

    /**
     * @param Constat $constat
     * @return string
     */
    public function summaryConstat(Constat $constat)
    {
        $filename = 'constat-' . $constat->code . '-' . $constat->id_constat . '.pdf';
        //var_dump($constat->load(['constateur.prestataire', 'conducteurs', 'assures.assurance.assistance', 'workflow', 'vehicules']));die;
        $data = ['constat' => $constat->load(['constateur.prestataire', 'conducteurs', 'assures.assurance.assistance', 'workflow', 'vehicules'])];
        //$pdf = PDF::loadView('exports.recap.constat', $data);
        //Storage::put('public/cdn/recap/' . $filename, $pdf->output());
        $path = (config('cdn.recap.path') . '/' . $filename);
        $url = config('cdn.recap.url') . $filename;
        $path = public_path() . '/storage' . $path;

        $mpdf = new Mpdf([
            'mode' => 'utf-8',
            'format' => 'A4',
            'margin_left' => 5,
            'margin_right' => 5,
            'margin_top' => 5,
            'margin_bottom' => 5
        ]);
        $mpdf->SetDisplayMode('real');
        $mpdf->pdf_version = '1.7';
        $mpdf->autoScriptToLang = true;
        $mpdf->autoLangToFont = true;
        $mpdf->SetAlpha(0.5);
        $html=\View::make('exports.recap.constat', $data);
        $html=$html->render();

        $mpdf->WriteHTML($html);
        $mpdf->Output($path,'F');

        /*$conv = new Converter();
        //dd(Path::binaryPath());
        $conv->setBinary(Path::binaryPath() . '.exe');
        $conv->addPage($html)->save($path);*/

        return $this->resp->ok(eResponseCode::RC_GET_200_00, $url);
    }

    /**
     * @param Constat $constat
     * @param Request $request
     * @return $this|\Illuminate\Http\JsonResponse
     */

    public function uploadPhotosDegats(Constat $constat, Request $request)
    {
        if(!$constat) return $this->resp->not_found(eResponseCode::CT_404_NOT_FOUND, $constat);

        $validator = Validator::make($request->all(), [
            'photos[]' => 'image|mimes:jpeg,png,jpg,gif'
        ]);

        $data = $request->all();

        if($request->hasFile('photos')) {
            $files = array();
            foreach($request->photos as $file) {
                if($file) {
                    $photos = $this->uploadOne($file, config('cdn.degats.path'). DIRECTORY_SEPARATOR . $constat->id_constat);
                    array_push($files, $photos['file_name']);
                }
            }

            unset($data['photos']);
            $data = array_merge($data, ['photos' => json_encode($files)]);
            $data['etape'] = eStepConstat::ETAPE_PHOTOS;
        }

        $constat->workflow->update($data);
        $constat->save();

        return $this->resp->ok(eResponseCode::D_UPLOAD_200_07, $constat->workflow);
    }


    /**
     * @param Constat $constat
     * @param Request $request
     * @return $this|\Illuminate\Http\JsonResponse
     */

    public function getPhotosDegats(Constat $constat, Request $request)
    {
        if(!$constat) return $this->resp->not_found(eResponseCode::CT_404_NOT_FOUND, $constat);

        $degats = WorkFlow::select('photos')->where('id_workflow',$constat->id_workflow)->get();
        return $this->resp->ok(eResponseCode::D_LISTED_200_00,$degats);
    }



    /**
     * Send Constat Email To Assures
     *
     * @param Constat $constat
     * @param Request $request
     * @return $this|\Illuminate\Http\JsonResponse
     */

    public function sendMailConstat(Constat $constat)
    {
        if(!$constat) return $this->resp->not_found(eResponseCode::CT_404_NOT_FOUND, $constat);

       foreach($constat->assures as $assure ){
           TemplateNotificationService::sendEmail('CONSTAT_SENT',get_class($assure),[$assure]) ;
        }
        return $this->resp->ok(eResponseCode::M_SENT_200_08);
    }

    /**
     * @return $this|\Illuminate\Http\JsonResponse
     */
    public function getListSteps()
    {
        return $this->resp->ok(eResponseCode::STEPS_CONSTAT, eStepConstat::getAll(eStepConstat::class));
    }

    /**
     * @return $this|\Illuminate\Http\JsonResponse
     */
    public function getListTypesVehicle()
    {
        return $this->resp->ok(eResponseCode::TYPES_VEHICLE, eTypeVehicle::getAll(eTypeVehicle::class));
    }

    /**
     * @param Constat $constat
     * @param Request $request
     * @return $this|\Illuminate\Http\JsonResponse
     */
    public function updatePeriod(Constat $constat, Request $request)
    {
        if(!$constat) return $this->resp->not_found(eResponseCode::CT_404_NOT_FOUND, $constat);

        $validator = Validator::make($request->all(), [
            'etat' => ['string', 'required', Rule::in(array("PAUSE" => eConstatStatut::PAUSE, "IN_PROGRESS" => eConstatStatut::IN_PROGRESS))]
        ]);

        if ($validator->fails()) {
            return $this->resp->bad_request($validator);
        }

        $period = $constat->workflow->period_pause ? $constat->workflow->period_pause : 0;
        $now = Carbon::now();
        if($request->etat == 'PAUSE') {
            $constat->workflow->date_pause = $now;
            $constat->workflow->save();
            $constat->etat = $request->etat;
            $constat->save();
        } elseif($request->etat == 'IN_PROGRESS') {
            $dt_pause = Carbon::parse($constat->workflow->date_pause);
            $constat->workflow->date_pause = null;
            $constat->workflow->period_pause = $period + $now->diffInSeconds($dt_pause);
            $constat->workflow->save();
            /*echo $now->toString() . ':' . $dt_pause->toString();
            dd($now->diffInSeconds($dt_pause));*/
            $constat->etat = $request->etat;
            $constat->save();
        }

        return $this->resp->ok(eResponseCode::CT_GET_200_01, $constat);
    }

}
