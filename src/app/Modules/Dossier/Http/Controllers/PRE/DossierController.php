<?php

namespace App\Modules\Dossier\Http\Controllers\PRE;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Enums\eResponseCode;
use App\Models\Constat;
use App\Models\WorkFlow;
use function PHPSTORM_META\map;
use ZipArchive;

class DossierController extends Controller
{

    /**
     * Display a listing of Dossier
     *
     * @return \Illuminate\Http\Response
     */
    public function all(Request $request, $limit = null)
    {
        if($limit)
            $constat = Constat::with([
                'constateur.prestataire' =>function($query) use ($request){
                    $query->where('id_prestataire',$request->user()->id_prestataire);
                },
                'assures.assurance.assistance',
                'workflow' => function($q) {
                    $q->orderByDesc('date_etablissement');
                },
                'vehicules',
                'conducteurs',
                'linked_constats'
            ])->orderByDesc('id_constat')->limit($limit)->get();
        else
            $constat = Constat::with([
                'constateur.prestataire' =>function($query) use ($request){
                    $query->where('id_prestataire',$request->user()->id_prestataire);
                },
                'assures.assurance.assistance',
                'workflow',
                'vehicules',
                'conducteurs',
                'linked_constats'
            ])->get();
        return $this->resp->ok(eResponseCode::CT_LISTED_200_00, $constat);
    }

    /**
     *
     * @param Request $request
     * @return \DevcorpIt\ResponseCode\JsonResp
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */

    public function search(Request $request )
    {
        $validator = Validator::make($request->all(), [
            'code' => 'nullable',
            'ville' => 'nullable',
            'date_etablissement' => 'nullable|array',
            'num_immatriculation' => 'nullable',
            'constateur' => 'nullable'
        ]);

        if ($validator->fails()){
            return $this->resp->bad_request($validator);
        }

        $dossier= Constat::with('constateur:id_constateur,nom,prenom','workflow','assures','vehicules')
            ->when(!empty($request->code),function($q) use($request){
                $q->where('code', $request->code);
            })->when(!empty($request->ville),function($q) use($request){
                $q->whereHas('workflow', function($q) use($request){
                    $q->where('ville', $request->ville);
                });
            })->when(!empty($request->date_etablissement),function($q) use($request){
                if(count($request->date_etablissement) > 1) {
                    $dates = $request->date_etablissement;
                    sort($dates);
                    $q->whereHas('workflow', function($q) use($dates){
                        $q->whereBetween('date_etablissement', $dates);
                    });
                } else {
                    $q->whereHas('workflow', function($q) use($request){
                        $q->where('date_etablissement', $request->date_etablissement);
                    });
                }

            })->when(!empty($request->num_immatriculation),function($q) use($request){
                $q->whereHas('vehicules', function($q) use($request){
                    $q->where('num_immatriculation', $request->num_immatriculation);
                });
            })->when(!empty($request->constateur),function($q) use($request){
                $q->whereHas('Constateur', function($q) use($request){
                    $q->where('id_constateur', $request->constateur);
                });
            })->get();

        return $this->resp->ok(eResponseCode::CT_LISTED_200_00, $dossier);

    }



    public function downloadDegats(Constat $constat,Request $request)
    {
        $validator = Validator::make($request->all(), [
            'elem' => 'string',
        ]);

        if ($validator->fails()){
            return $this->resp->bad_request($validator);
        }
        $file = $request->elem;

        if(in_array($file,json_decode($constat->workflow->photos, true))){
            return response()->download(public_path('storage\\cdn\\constateurs\\degats\\') . $constat->id_constat . '\\' . $file);
        }


       // ;
    }

    /**
     * Display the specified Dossier.
     *
     * @param Dossier $dossier
     * @return \DevcorpIt\ResponseCode\JsonResp
     */

    public function get(Constat $constat)
    {
        if(!$constat)
            return $this->resp->ok(eResponseCode::CT_404_NOT_FOUND);
        $constat = $constat->load(
            'constateur:id_constateur,nom,prenom',
            'workflow',
            'assures.assurance.assistance',
            'conducteurs',
            'vehicules',
            'degat',
            'linked_constats.assures.assurance',
            'linked_constats.conducteurs'
        );
        $constat['recap'] = $this->verifyExistFile($constat['code'], $constat['id_constat']);
        if($constat['linked_constats']) {
            $constat['linked_constats']->each(function ($item) {
                $item['recap'] = $this->verifyExistFile($item['code'], $item['id_constat']);
            });
        }
        return $this->resp->ok(eResponseCode::CT_GET_200_01, $constat);
    }

    public function verifyExistFile($code_constat, $id_constat)
    {
        $filename = 'constat-' . $code_constat . '-' . $id_constat . '.pdf';
        $path = (config('cdn.recap.path') . '/' . $filename);
        $pdf_file = public_path() . '/storage' . $path;
        return file_exists($pdf_file);
    }

}
