<?php

Route::group(['prefix' => '/api/v1/pre/dossier', 'module' => 'Dossier', 'namespace' => 'PRE'], function() {

    Route::middleware(['auth.jwt','auth.user_prestataire','api'])->group(function () {
        Route::get('/list/{limit?}', 'DossierController@all');
        Route::post('/search', 'DossierController@search');
        Route::get('/get/{constat}', 'DossierController@get');
        Route::post('/download/{constat}', 'DossierController@downloadDegats');

        Route::get('/check_pdf/{constat}', 'DossierController@verifyExistFile');
    });
});

Route::group(['prefix' => '/api/v1/ass/dossier', 'module' => 'Dossier', 'namespace' => 'ASS'], function() {

    Route::middleware(['auth.jwt','auth.user_assistance','api'])->group(function () {
        Route::get('/list/{limit?}', 'DossierController@all');
        Route::post('/search', 'DossierController@search');
        Route::get('/get/{constat}', 'DossierController@get');
        Route::post('/validate/{constat}', 'DossierController@validateDossier');
        Route::post('/download/{constat}', 'DossierController@downloadDegats');
        Route::get('/check_pdf/{constat}', 'DossierController@verifyExistFile');
    });
});

Route::group(['prefix' => '/api/v1/assu/dossier', 'module' => 'Dossier', 'namespace' => 'ASSU'], function() {

    Route::middleware(['auth.jwt','auth.user_assurance','api'])->group(function () {
        Route::get('/list/{limit?}', 'DossierController@all');
        Route::post('/search', 'DossierController@search');
        Route::get('/get/{constat}', 'DossierController@get');
        Route::post('/download/{constat}', 'DossierController@downloadDegats');
        Route::get('/check_pdf/{constat}', 'DossierController@verifyExistFile');
    });
});

/****************************** COSTATEUR-CONSTATS**********************/

Route::group(['prefix' => '/api/v1/cons/dossier', 'module' => 'Dossier', 'namespace' => 'CONSTATEUR'], function() {

    Route::middleware(['auth.jwt','auth.constateur','api'])->group(function () {
        Route::post('/create', 'DossierController@create');

        /****** Annulation Constat ******/
        Route::post('/annule/{constat}', 'DossierController@cancelConstat');

        /****** Assurés ******/
        Route::post('/assure/assign/{constat}', 'DossierController@assignAssure');
        Route::post('/assure/edit/{assure}', 'DossierController@editAssure');
        Route::get('/assure/list/{constat}', 'DossierController@listAssures');

        /****** Conducteur ******/
        Route::post('/conducteur/assign/{constat}', 'DossierController@assignConducteur');
        Route::post('/conducteur/edit/{conducteur}', 'DossierController@editConducteur');
        Route::get('/conducteur/list/{constat}', 'DossierController@listConducteurs');

        /****** Conducteur Signature ******/
        Route::post('/conducteur/sign/{conducteur}', 'DossierController@signConducteur');
        Route::get('/conducteur/listSigns/{constat}', 'DossierController@listSignsConducteurs');

        /****** Circonstances Accident ******/
        Route::post('/circonstances/assign/{constat}', 'DossierController@assignCirconstances');
        Route::get('/circonstances/list/{constat}', 'DossierController@listCirconstances');

        /****** Croquis ******/
        Route::post('/croquis/upload/{constat}', 'DossierController@uploadCroquis');
        Route::get('/croquis/get/{constat}','DossierController@getCroquis');

        /****** Vehicles ******/
        Route::post('/vehicle/assign/{constat}', 'DossierController@assignVehicle');
        Route::post('/vehicle/edit/{vehicule}', 'DossierController@editVehicle');
        Route::get('/vehicle/list/{constat}', 'DossierController@listVehicles');

        /****** Dégats ********/
        Route::post('/vehicle/assign_degats/{vehicule}', 'DossierController@assignDegats');

        /******** Summary ********/
        Route::get('/summary/{constat}', 'DossierController@summaryConstat');
        Route::post('/vehicle/upload_degats/{constat}','DossierController@uploadPhotosDegats');
        Route::get('/vehicle/degats_photos/{constat}','DossierController@getPhotosDegats');

        /****** Email Consntat ********/
        Route::post('/email_constat/{constat}','DossierController@sendMailConstat');

        /************* Constat steps ************/
        Route::get('/steps','DossierController@getListSteps');
        Route::get('/types-vehicle','DossierController@getListTypesVehicle');

        /************* Constat chrono ************/
        Route::post('/pause-period/{constat}','DossierController@updatePeriod');
    });
});

