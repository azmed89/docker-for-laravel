<?php
namespace App\Modules\Prestataire\Http\Controllers\BO;


use App\Http\Controllers\Controller;
use App\Enums\eResponseCode;
use App\Models\Prestataire;
use Illuminate\Support\Facades\Validator;
use App\Enums\eTypePrestataire;
use Illuminate\Validation\Rule;
use Illuminate\Http\Request;

class PrestataireController extends Controller
{

    /**
     * Display a listing of prestataires
     *
     * @return \Illuminate\Http\Response
     */
    public function getAllFolder(Request $request)
    {
        $prestataires = Prestataire::all();

        return $this->resp->ok(eResponseCode::P_LISTED_200_00, $prestataires);
    }


    /**
     * Display the specified prestataire.
     *
     * @param User $user
     * @return \DevcorpIt\ResponseCode\JsonResp
     */
    public function get(Prestataire $prestataire)
    {
        if(!$prestataire)

            return $this->resp->ok(eResponseCode::P_404_NOT_FOUND, $prestataire);
        return $this->resp->ok(eResponseCode::P_GET_200_01, $prestataire);
    }


    /**
     * Store a newly created prestataire.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \DevcorpIt\ResponseCode\JsonResp
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email'=> ['required', 'email', Rule::unique('prestataires')],
            'code_groupe'=> 'required|string|min:6',
            'name'=> 'required|string|max:150',
            'adresse'=> 'required|string|max:150',
            'ville'=> 'required|string|max:150',
            'telephone'=> 'required|string|max:20',
            'type_prestataire'=> Rule::in(eTypePrestataire::getAll(eTypePrestataire::class)),
        ]);

        if ($validator->fails()){
            $failedRules = $validator->failed();
            if (isset($failedRules['email']['Unique'])) {
                return $this->resp->not_acceptable(eResponseCode::EMAIL_TAKEN);
            }

            return $this->resp->bad_request($validator);
        }


        $prestataire = Prestataire::create($request->all());
        return $this->resp->ok(eResponseCode::P_CREATE_200_02, $prestataire);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param User $prestataire
     * @return \DevcorpIt\ResponseCode\JsonResp
     */
    public function update(Request $request, Prestataire $prestataire)
    {
        $validator = Validator::make($request->all(), [
            'email'=> ['required', 'email', Rule::unique('prestataires')->ignore($prestataire)],
            'code_groupe'=> 'required|string|min:6',
            'name'=> 'required|string|max:150',
            'adresse'=> 'required|string|max:150',
            'ville'=> 'required|string|max:150',
            'telephone'=> 'required|string|max:20',
            'type_prestataire'=> Rule::in(eTypePrestataire::getAll(eTypePrestataire::class)),
        ]);
        if ($validator->fails()){
            $failedRules = $validator->failed();
            if (isset($failedRules['email']['Unique'])) {
                return $this->resp->not_acceptable(eResponseCode::EMAIL_TAKEN);
            }
            return $this->resp->bad_request($validator);
        }
        $prestataire->update($request->all());

        return $this->resp->ok(eResponseCode::P_UPDATE_200_03, $prestataire);
    }

    public function delete(Prestataire $prestataire)
    {
        $prestataire->delete();
        return $this->resp->ok(eResponseCode::P_DELETE_200_04);
    }


}
