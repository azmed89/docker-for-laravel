<?php

Route::group(['prefix' => '/api/v1/bo/prestataire', 'module' => 'Prestataire', 'namespace' => 'BO'], function() {

    Route::middleware(['auth.jwt','auth.user','api'])->group(function () {
        Route::get('/list', 'PrestataireController@all');
        Route::post('/create', 'PrestataireController@create');
        Route::get('/get/{prestataire}', 'PrestataireController@get');
        Route::post('/update/{prestataire}', 'PrestataireController@update');
        Route::post('/delete/{prestataire}', 'PrestataireController@delete');
        //Route::post('/profil/update', 'UserController@updateUserProfil');
    });
});