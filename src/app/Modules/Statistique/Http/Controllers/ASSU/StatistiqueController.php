<?php

namespace App\Modules\Statistique\Http\Controllers\ASSU;

use App\Enums\eTypeUsage;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Enums\eResponseCode;
use App\Models\Constat;
use Illuminate\Support\Facades\DB;


class StatistiqueController extends Controller
{

    /**
     * Display the module welcome screen
     *
     * @return \Illuminate\Http\Response
     */
    public function opsStatistique(Request $request){

        $constatsTotale=DB::select("SELECT COUNT(DISTINCT constats.id_constat) as nombre_constats
              FROM constats
              INNER JOIN assures ON assures.id_constat=constats.id_constat
              INNER JOIN assurances ON assurances.code_societe=assures.ste_assurance
              where assurances.id_assurance=".$request->user()->id_assurance."
              ");


        $constatsQuartier=DB::select("SELECT COUNT(DISTINCT constats.id_constat) as nombre_constats ,workflows.quartier as quartier
              FROM constats
              INNER JOIN workflows ON workflows.id_workflow=constats.id_workflow
              /*INNER JOIN assures ON assures.id_constat=constats.id_constat
              INNER JOIN assurances ON assurances.code_societe=assures.ste_assurance
              where assurances.id_assurance=".$request->user()->id_assurance."*/
              GROUP BY quartier 
              ");

        $constatsVille=DB::select("SELECT COUNT(DISTINCT constats.id_constat) as nombre_constats ,workflows.ville as ville
              FROM constats
              INNER JOIN workflows ON workflows.id_workflow=constats.id_workflow
              /*INNER JOIN assures ON assures.id_constat=constats.id_constat
              INNER JOIN assurances ON assurances.code_societe=assures.ste_assurance
              where assurances.id_assurance=".$request->user()->id_assurance."*/
              GROUP BY ville 
              ");


        $constatsEtat=DB::select("SELECT COUNT(DISTINCT constats.id_constat) as nombre_constats ,constats.etat as etat
              FROM constats
              INNER JOIN assures ON assures.id_constat=constats.id_constat
              INNER JOIN assurances ON assurances.code_societe=assures.ste_assurance
              where assurances.id_assurance=".$request->user()->id_assurance."
              GROUP BY etat");


        $constatsMonth=DB::select("SELECT COUNT(DISTINCT constats.id_constat) as nombre_constats ,MONTH(workflows.date_etablissement) as value
              FROM constats
              INNER JOIN workflows ON workflows.id_workflow=constats.id_workflow
              INNER JOIN assures ON assures.id_constat=constats.id_constat
              INNER JOIN assurances ON assurances.code_societe=assures.ste_assurance
              where assurances.id_assurance=".$request->user()->id_assurance." AND YEAR(workflows.date_etablissement) = YEAR(now())
              GROUP BY value
              ORDER BY value ");


        $constatsDays=DB::select("SELECT COUNT(DISTINCT constats.id_constat) as nombre_constats ,DAYOFWEEK(workflows.date_etablissement) as value
              FROM constats
              INNER JOIN workflows ON workflows.id_workflow=constats.id_workflow
              INNER JOIN assures ON assures.id_constat=constats.id_constat
              INNER JOIN assurances ON assurances.code_societe=assures.ste_assurance
              where assurances.id_assurance=".$request->user()->id_assurance." AND WEEK(workflows.date_etablissement) = WEEK(now()) AND YEAR(workflows.date_etablissement) = YEAR(now())
              GROUP BY value
              ORDER BY value ");


        //dd($constatsDays);

        $result['constats_par_mois']=$constatsMonth;
        $result['constats_par_semaine']= $constatsDays;

        $ar = array_combine(range(1, 12), array_fill(1,12, 0));
        foreach ($ar as $key => $elem) {
            $ar[$key] = rand(20, 150);
        }
        $ar1 = array_combine(range(1, 7), array_fill(1,7, 0));
        foreach ($ar1 as $key => $elem) {
            $ar1[$key] = rand(20, 150);
        }
        $statistics['constats_par_mois'] = $ar;
        $statistics['constats_par_semaine'] = $ar1;

        $result=collect($result);

        $result = $result->map(function ($collection, $index) use (&$statistics){
            foreach($collection as $key =>$val) {
                $i=$val->value;
                $value=$val->nombre_constats;
                unset($val->value, $val->nombre_constats);
                 $statistics[$index][$i]=$value;
            }
        });

        $statistics['constats_totale']=$constatsTotale;
        $statistics['constats_par_etat']= $constatsEtat;
        $statistics['constats_par_ville']=$constatsVille;
        $statistics['constats_par_quartier']=$constatsQuartier;

        $statistics['validate_constats_per_day'] = $this->validateConstatsPerDay($request->user());
        $statistics['constats_per_day_of_month'] = $this->constatsPerDayOfMonth($request->user());
        $statistics['constats_per_type_usage'] = $this->constatsPerTypeUsage($request->user());

        return $this->resp->ok(eResponseCode::STS_LISTED_200_00, $statistics); // STS200_00

    }

    private function validateConstatsPerDay($user) {
        $validate_constats_per_day = DB::table('workflows')
            ->select(DB::raw("DAYOFWEEK(date_etablissement) the_day"), DB::raw('constats.etat status'), DB::raw('count(*) as total'))
            ->whereMonth('date_etablissement', date('m'))
            ->whereYear('date_etablissement', date('Y'))
            ->join('constats', 'workflows.id_workflow', '=', 'constats.id_workflow')
            ->join('assures', 'constats.id_constat', '=', 'assures.id_constat')
            ->join('assurances', 'assures.ste_assurance', '=', 'assurances.code_societe')
            ->where('assurances.id_assurance', $user->id_assurance)
            ->groupBy('the_day', 'status')
            ->having('status', 'IN_PROGRESS')
            ->orderBy('the_day', 'asc')
            ->get();
        $ar1 = array_combine(range(1, 7), array_fill(1,7, 0));
        foreach ($ar1 as $key => $elem) {
            $ar1[$key] = rand(5, 55);
        }
        $daily_days = $ar1;
        $validate_constats_per_day->map(function($item) use (&$daily_days) {
            $daily_days[$item->the_day] = $item->total;
        });

        return $daily_days;
    }

    private function constatsPerDayOfMonth($user) {
        $constats_per_day = DB::table('workflows')
            ->select(DB::raw("DAYOFMONTH(date_etablissement) the_day"), DB::raw('count(*) as total'))
            ->whereMonth('date_etablissement', date('m'))
            ->whereYear('date_etablissement', date('Y'))
            ->join('constats', 'workflows.id_workflow', '=', 'constats.id_workflow')
            ->join('assures', 'constats.id_constat', '=', 'assures.id_constat')
            ->join('assurances', 'assures.ste_assurance', '=', 'assurances.code_societe')
            ->where('assurances.id_assurance', $user->id_assurance)
            ->groupBy('the_day')
            ->orderBy('the_day', 'asc')
            ->get();
        $count_now = Carbon::today()->daysInMonth;

        $ar1 = array_combine(range(1, $count_now), array_fill(1, $count_now, 0));
        foreach ($ar1 as $key => $elem) {
            $ar1[$key] = rand(20, 150);
        }

        $month_days = $ar1;
        $constats_per_day->map(function($item) use (&$month_days) {
            $month_days[$item->the_day] = $item->total;
        });

        return $month_days;
    }

    private function constatsPerTypeUsage($user) {

        $sub = DB::table('constats')
            ->selectRaw('assures.type_usage as type_usage, assures.id_constat as constat')
            ->join('assures', 'constats.id_constat', '=', 'assures.id_constat')
            ->join('assurances', 'assures.ste_assurance', '=', 'assurances.code_societe')
            ->where('assurances.id_assurance', $user->id_assurance)
            ->groupBy('type_usage', 'constat');

        $constats_per_type_usage = DB::table( DB::raw("({$sub->toSql()}) as sub") )
            ->mergeBindings($sub)
            ->selectRaw('COUNT(constat) AS total, type_usage')
            ->groupBy('type_usage')
            ->get();

        /*$constats_per_type_usage = DB::table('constats')
            ->select(DB::raw('assures.type_usage type_usage'), DB::raw('count(*) as total'))
            ->join('assures', 'constats.id_constat', '=', 'assures.id_constat')
            ->join('assurances', 'assures.ste_assurance', '=', 'assurances.code_societe')
            ->where('assurances.id_assurance', $user->id_assurance)
            ->groupBy('type_usage')
            ->get();
        dd($constats_per_type_usage);*/

        $types_usage = eTypeUsage::getAll(eTypeUsage::class);
        $totals_usage = array_fill_keys(array_keys($types_usage), 0);
        $constats_per_type_usage->map(function($item) use (&$totals_usage) {
            $totals_usage[$item->type_usage] = $item->total;
        });

        return $totals_usage;
    }
}
