<?php

namespace App\Modules\Statistique\Http\Controllers\PRE;

use App\Enums\eTypeUsage;
use App\Http\Controllers\Controller;
use App\Models\WorkFlow;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Enums\eResponseCode;
use Illuminate\Support\Facades\DB;


class StatistiqueController extends Controller
{
    public function opsStatistics(Request $request)
    {
        $stats = [];

        $list_constats_per_month = $this->constatsPerMonth();
        $stats['constats_per_month'] = $list_constats_per_month;

        $list_constats_per_day = $this->constatsPerDay();
        $stats['constats_per_day'] = $list_constats_per_day;

        $list_validate_constats_per_day = $this->validateConstatsPerDay();
        $stats['validate_constats_per_day'] = $list_validate_constats_per_day;

        $count_constats = $this->countConstats($request);
        $stats['count_constats'] = $count_constats;

        $list_validate_constats_per_status = $this->constatsPerStatus($request);
        $stats['validate_constats_per_status'] = $list_validate_constats_per_status;

        $stats['constats_per_city'] = $this->constatsPerCity($request);
        $stats['constats_per_district'] = $this->constatsPerDistrict($request);

        $list_constats_per_status_per_month = $this->constatsByStatusPerMonth();
        $stats['constats_per_status_per_month'] = $list_constats_per_status_per_month;

        $stats['constats_per_type_usage'] = $this->constatsPerTypeUsage($request->user());
        $stats['constats_per_day_of_month'] = $this->constatsPerDayOfMonth($request->user());

        $stats['constats_per_agent'] = $this->constatsPerStatusAgent($request->user());

        //dd($stats['validate_constats_per_city']);

        return $this->resp->ok(eResponseCode::STS_LISTED_200_00, $stats);
    }

    private function constatsPerMonth() {
        $constats_per_month = DB::table('workflows')
            ->select(DB::raw("MONTH(date_etablissement) date_e"), DB::raw('count(*) as total'))
            ->whereYear('date_etablissement', date('Y'))
            ->groupBy('date_e')
            ->orderBy('date_e', 'asc')
            ->get();
        $months = array_combine(range(1, 12), array_fill(1, 12, 0));
        foreach ($months as $key => $elem) {
            $months[$key] = rand(20, 150);
        }
        $constats_per_month->map(function($item) use (&$months) {
            $months[$item->date_e] = $item->total;
        });

        return $months;
    }

    private function constatsByStatusPerMonth() {
        $constats_per_month = DB::table('workflows')
            ->select(DB::raw("MONTH(date_etablissement) date_e"), DB::raw('constats.etat status'), DB::raw('count(*) as total'))
            ->whereYear('date_etablissement', date('Y'))
            ->join('constats', 'workflows.id_workflow', '=', 'constats.id_workflow')
            ->groupBy('date_e', 'status')
            ->orderBy('date_e', 'asc')
            ->get();
        //dd($constats_per_month);
        $months_status = array(
            'CREATED' => array_combine(range(1, 12), array_fill(1, 12, 0)),
            'CANCELED' => array_combine(range(1, 12), array_fill(1, 12, 0)),
            'FINISHED' => array_combine(range(1, 12), array_fill(1, 12, 0)),
            'IN_PROGRESS' => array_combine(range(1, 12), array_fill(1, 12, 0)),
            'PAUSE' => array_combine(range(1, 12), array_fill(1, 12, 0)),
        );
        $constats_per_month->map(function($item) use (&$months_status) {
            $months_status[$item->status][$item->date_e] = $item->total;
        });

        return $months_status;
    }

    private function constatsPerDay() {
        $constats_per_day = DB::table('workflows')
            ->select(DB::raw("DAYOFWEEK(date_etablissement) the_day"), DB::raw('count(*) as total'))
            ->whereMonth('date_etablissement', date('m'))
            ->whereYear('date_etablissement', date('Y'))
            ->groupBy('the_day')
            ->orderBy('the_day', 'asc')
            ->get();
        $week_days = array_combine(range(1, 7), array_fill(1, 7, 0));
        foreach ($week_days as $key => $elem) {
            $week_days[$key] = rand(20, 150);
        }
        $constats_per_day->map(function($item) use (&$week_days) {
            $week_days[$item->the_day] = $item->total;
        });

        return $week_days;
    }

    private function validateConstatsPerDay() {
        $validate_constats_per_day = DB::table('workflows')
            ->select(DB::raw("DAYOFWEEK(date_etablissement) the_day"), DB::raw('constats.etat status'), DB::raw('count(*) as total'))
            ->whereMonth('date_etablissement', date('m'))
            ->whereYear('date_etablissement', date('Y'))
            ->join('constats', 'workflows.id_workflow', '=', 'constats.id_workflow')
            ->groupBy('the_day', 'status')
            ->having('status', 'CREATED')
            ->orderBy('the_day', 'asc')
            ->get();
        $daily_days = array_combine(range(1, 7), array_fill(1, 7, 0));
        $validate_constats_per_day->map(function($item) use (&$daily_days) {
            $daily_days[$item->the_day] = $item->total;
        });

        return $daily_days;
    }

    private function constatsPerStatus($request) {
        $count_constats = DB::table('constats')
            ->select(DB::raw('count(*) as total'), DB::raw('constats.etat as status'))
            ->join('constateurs', 'constats.id_constateur', '=','constateurs.id_constateur')
            ->join('prestataires', 'constateurs.id_prestataire', '=','prestataires.id_prestataire')
            ->where('prestataires.id_prestataire','=', $request->user()->id_prestataire)
            ->groupBy('status')
            ->get();

        return $count_constats;
    }

    private function constatsPerCity($request) {
        $count_constats = DB::table('constats')
            ->select(DB::raw('count(*) as total'), DB::raw('workflows.ville as city'))
            ->join('workflows','constats.id_workflow','=','workflows.id_workflow')
            ->join('constateurs', 'constats.id_constateur', '=','constateurs.id_constateur')
            ->join('prestataires', 'constateurs.id_prestataire', '=','prestataires.id_prestataire')
            ->where('prestataires.id_prestataire','=', $request->user()->id_prestataire)
            ->groupBy('city')
            ->get();

        return $count_constats;
    }

    private function constatsPerDistrict($request) {
        $count_constats = DB::table('constats')
            ->select(DB::raw('count(*) as total'), DB::raw('workflows.quartier as quartier'))
            ->join('workflows','constats.id_workflow','=','workflows.id_workflow')
            ->join('constateurs', 'constats.id_constateur', '=','constateurs.id_constateur')
            ->join('prestataires', 'constateurs.id_prestataire', '=','prestataires.id_prestataire')
            ->where('prestataires.id_prestataire','=', $request->user()->id_prestataire)
            ->groupBy('quartier')
            ->get();

        return $count_constats;
    }

    private function countConstats($request) {
        $count_constats = DB::table('constats')
            ->join('constateurs', 'constats.id_constateur', 'constateurs.id_constateur')
            ->join('prestataires', 'constateurs.id_prestataire', 'prestataires.id_prestataire')
            ->join('user_prestataires', 'prestataires.id_prestataire', 'user_prestataires.id_prestataire')
            ->where('user_prestataires.id', $request->user()->id)
            ->count();

        return $count_constats;
    }

    private function constatsPerTypeUsage($user) {

        $sub = DB::table('constats')
            ->selectRaw('assures.type_usage as type_usage, assures.id_constat as constat')
            ->join('assures', 'constats.id_constat', '=', 'assures.id_constat')
            ->join('constateurs', 'constats.id_constateur', '=', 'constateurs.id_constateur')
            ->join('prestataires', 'constateurs.id_prestataire', '=', 'prestataires.id_prestataire')
            ->where('prestataires.id_prestataire', $user->id_prestataire)
            ->groupBy('type_usage', 'constat');

        $constats_per_type_usage = DB::table( DB::raw("({$sub->toSql()}) as sub") )
            ->mergeBindings($sub)
            ->selectRaw('COUNT(constat) AS total, type_usage')
            ->groupBy('type_usage')
            ->get();

        /*$constats_per_type_usage = DB::table('constats')
            ->select(DB::raw('assures.type_usage type_usage'), DB::raw('count(*) as total'))
            ->join('assures', 'constats.id_constat', '=', 'assures.id_constat')
            ->join('assurances', 'assures.ste_assurance', '=', 'assurances.code_societe')
            ->where('assurances.id_assurance', $user->id_assurance)
            ->groupBy('type_usage')
            ->get();
        dd($constats_per_type_usage);*/

        $types_usage = eTypeUsage::getAll(eTypeUsage::class);
        $totals_usage = array_fill_keys(array_keys($types_usage), 0);
        $constats_per_type_usage->map(function($item) use (&$totals_usage) {
            $totals_usage[$item->type_usage] = $item->total;
        });

        return $totals_usage;
    }

    private function constatsPerDayOfMonth($user) {
        $constats_per_day = DB::table('workflows')
            ->select(DB::raw("DAYOFMONTH(date_etablissement) the_day"), DB::raw('count(*) as total'))
            ->whereMonth('date_etablissement', date('m'))
            ->whereYear('date_etablissement', date('Y'))
            ->join('constats', 'workflows.id_workflow', '=', 'constats.id_workflow')
            ->join('assures', 'constats.id_constat', '=', 'assures.id_constat')
            ->join('constateurs', 'constats.id_constateur', '=', 'constateurs.id_constateur')
            ->join('prestataires', 'constateurs.id_prestataire', '=', 'prestataires.id_prestataire')
            ->where('prestataires.id_prestataire', $user->id_prestataire)
            ->groupBy('the_day')
            ->orderBy('the_day', 'asc')
            ->get();
        $count_now = Carbon::today()->daysInMonth;

        $ar1 = array_combine(range(1, $count_now), array_fill(1, $count_now, 0));
        foreach ($ar1 as $key => $elem) {
            $ar1[$key] = rand(20, 150);
        }

        $month_days = $ar1;
        $constats_per_day->map(function($item) use (&$month_days) {
            $month_days[$item->the_day] = $item->total;
        });

        return $month_days;
    }

    private function constatsPerStatusAgent($user) {
        $constats_per_agent = DB::table('constats')
            ->select(DB::raw('count(*) as total'), DB::raw('constats.etat as status'), DB::raw('constats.id_constateur as agent'), DB::raw('constateurs.nom as name'))
            ->join('constateurs', 'constats.id_constateur', '=','constateurs.id_constateur')
            ->join('prestataires', 'constateurs.id_prestataire', '=','prestataires.id_prestataire')
            //->where('prestataires.id_prestataire','=', $user->id_prestataire)
            ->groupBy('status', 'agent', 'constateurs.nom')
            ->get();

        $keys = $constats_per_agent->pluck( 'agent')->toArray();
        $agents = array_unique($keys);
        $list_per_agent = array_combine(array_values($agents), array_fill(1, count($agents), array()));

        $constats_per_agent->map(function ($item) use (&$list_per_agent) {
            array_push($list_per_agent[$item->agent], $item);
        });

        return $list_per_agent;
    }
}
