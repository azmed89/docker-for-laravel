<?php
/**
 * Created by PhpStorm.
 * User: abderrahim akhdar
 * Date: 08/12/2020
 * Time: 14:08
 */

/*********************** Statistiques **************************/


/**
 * @OA\Get(
 * path="/api/v1/ass/statistique/ops",
 * summary="Constats Statistiques",
 * description="Constats Statistiques in BO Assistance",
 * tags={"BO Assistance"},
 * security={ {"apiAuth": {} }},
 * @OA\Response(
 *    response=200,
 *    description="Success"
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */

/**
 * @OA\Get(
 * path="/api/v1/assu/statistique/ops",
 * summary="Constats Statistiques",
 * description="Constats Statistiques in BO Assurance",
 * tags={"BO Assurance"},
 * security={ {"apiAuth": {} }},
 * @OA\Response(
 *    response=200,
 *    description="Success"
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */

/**
 * @OA\Post(
 * path="/api/v1/pre/statistic/ops",
 * summary="Constats Statistiques",
 * description="Constats Statistiques in BO Prestataire",
 * tags={"BO Prestataire"},
 * security={ {"apiAuth": {} }},
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */