<?php


Route::group(['prefix' => '/api/v1/ass/statistique', 'module' => 'Statistique', 'namespace' => 'ASS'], function() {
    Route::middleware(['auth.jwt','auth.user_assistance','api'])->group(function () {
        Route::get('/ops', 'StatistiqueController@opsStatistique');
    });
});

Route::group(['prefix' => '/api/v1/pre/statistic', 'module' => 'Statistique', 'namespace' => 'PRE'], function() {
    Route::middleware(['auth.jwt','auth.user_prestataire','api'])->group(function () {
        Route::post('/ops', 'StatistiqueController@opsStatistics');

    });
});

Route::group(['prefix' => '/api/v1/assu/statistique', 'module' => 'Statistique', 'namespace' => 'ASSU'], function() {
    Route::middleware(['auth.jwt','auth.user_assurance','api'])->group(function () {
        Route::post('/ops', 'StatistiqueController@opsStatistique');
    });
});
