<?php
/**
 * Created by PhpStorm.
 * User: AZMed
 * Date: 13/10/2020
 * Time: 14:56
 */

/*********************** Users **************************/

/**
 * @OA\Get(
 * path="/api/v1/admin/user/list",
 * summary="List Utlisateurs",
 * description="List of Users in Administration",
 * tags={"Administration - Users"},
 * security={ {"apiAuth": {} }},
 * @OA\Response(
 *    response=200,
 *    description="Success"
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */
/**
 * @OA\Get(
 * path="/api/v1/admin/user/get/{id}",
 * summary="Details Users",
 * description="Details User in Administration",
 * tags={"Administration - Users"},
 * security={ {"apiAuth": {} }},
 * @OA\Parameter(
 *    description="ID of User",
 *    in="path",
 *    name="id",
 *    required=true,
 *    example="1",
 *    @OA\Schema(
 *       type="integer",
 *       format="int64"
 *    )
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */
/**
 * @OA\Post(
 * path="/api/v1/admin/user/create",
 * summary="Create User",
 * description="Create User in Administration",
 * tags={"Administration - Users"},
 * security={ {"apiAuth": {} }},
 * @OA\RequestBody(
 *    required=true,
 *    description="Pass user credentials",
 *    @OA\JsonContent(
 *       @OA\Property(property="nom", type="string", example="Manager"),
 *       @OA\Property(property="prenom", type="string", example="Manager"),
 *       @OA\Property(property="email", type="string", example="manager@manager.com"),
 *       @OA\Property(property="password", type="string", example=" "),
 *       @OA\Property(property="phone", type="string", example="06-********"),
 *       @OA\Property(property="avatar", type="string", example=""),
 *    ),
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */

/**
 * @OA\Post(
 * path="/api/v1/admin/user/update/{id}",
 * summary="Update User",
 * description="Update User in Administration",
 * tags={"Administration - Users"},
 * security={ {"apiAuth": {} }},
 * @OA\Parameter(
 *    description="ID of User",
 *    in="path",
 *    name="id",
 *    required=true,
 *    example="1",
 *    @OA\Schema(
 *       type="integer",
 *       format="int64"
 *    )
 * ),
 * @OA\RequestBody(
 *    required=true,
 *    description="Pass user credentials",
 *    @OA\JsonContent(
 *       @OA\Property(property="nom", type="string", example="Manager"),
 *       @OA\Property(property="prenom", type="string", example="Manager"),
 *       @OA\Property(property="email", type="string", example="manager@manager.com"),
 *       @OA\Property(property="password", type="string", example="password"),
 *       @OA\Property(property="phone", type="string", example="06-********"),
 *       @OA\Property(property="avatar", type="string", example=""),
 *    ),
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */

/**
 * @OA\Post(
 * path="/api/v1/admin/user/delete/{id}",
 * summary="Update User",
 * description="Update User in Administration",
 * tags={"Administration - Users"},
 * security={ {"apiAuth": {} }},
 * @OA\Parameter(
 *    description="ID of User",
 *    in="path",
 *    name="id",
 *    required=true,
 *    example="1",
 *    @OA\Schema(
 *       type="integer",
 *       format="int64"
 *    )
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */

/**
 * @OA\Post(
 * path="/api/v1/admin/user/profil/update",
 * summary="Update User Profile",
 * description="Update User Profile in Administration",
 * tags={"Administration - Users"},
 * security={ {"apiAuth": {} }},
 * @OA\RequestBody(
 *    required=true,
 *    description="Pass user credentials",
 *    @OA\JsonContent(
 *       @OA\Property(property="nom", type="string", example="Manager"),
 *       @OA\Property(property="prenom", type="string", example="Manager"),
 *       @OA\Property(property="email", type="string", example="manager@manager.com"),
 *       @OA\Property(property="password", type="string", example="password"),
 *       @OA\Property(property="phone", type="string", example="06-********"),
 *       @OA\Property(property="avatar", type="string", example=""),
 *    ),
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */

/*********************** Prestataires **************************/

/**
 * @OA\Get(
 * path="/api/v1/admin/pre/list",
 * summary="List Prestataires",
 * description="List of Prestataires in Administration",
 * tags={"Administration - Prestataires"},
 * security={ {"apiAuth": {} }},
 * @OA\Response(
 *    response=200,
 *    description="Success"
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */

/**
 * @OA\Get(
 * path="/api/v1/admin/pre/get/{id}",
 * summary="Details Prestatire",
 * description="Details Prestatire in Administration",
 * tags={"Administration - Prestataires"},
 * security={ {"apiAuth": {} }},
 * @OA\Parameter(
 *    description="ID of Prestataire",
 *    in="path",
 *    name="id",
 *    required=true,
 *    example="1",
 *    @OA\Schema(
 *       type="integer",
 *       format="int64"
 *    )
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */
/**
 * @OA\Post(
 * path="/api/v1/admin/pre/create",
 * summary="Create Prestataire",
 * description="Create Presrtataire in Administration",
 * tags={"Administration - Prestataires"},
 * security={ {"apiAuth": {} }},
 * @OA\RequestBody(
 *    required=true,
 *    description="Pass user credentials",
 *    @OA\JsonContent(
 *       @OA\Property(property="name", type="string", example="Presta1"),
 *       @OA\Property(property="email", type="string", example="prestataire@prestataire.com"),
 *       @OA\Property(property="code_groupe", type="string", example="P789"),
 *       @OA\Property(property="telephone", type="string", example="06-********"),
 *       @OA\Property(property="adresse", type="string", example=""),
 *       @OA\Property(property="ville", type="string", example="Meknes"),
 *       @OA\Property(property="type_prestataire", type="string", example="DEPANEUSES"),
 *    ),
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */
/**
 * @OA\Post(
 * path="/api/v1/admin/pre/update/{id}",
 * summary="Update Prestataire",
 * description="Update Prestataire in Administration",
 * tags={"Administration - Prestataires"},
 * security={ {"apiAuth": {} }},
 * @OA\Parameter(
 *    description="ID of Prestataire",
 *    in="path",
 *    name="id",
 *    required=true,
 *    example="1",
 *    @OA\Schema(
 *       type="integer",
 *       format="int64"
 *    )
 * ),
 * @OA\RequestBody(
 *    required=true,
 *    description="Pass user credentials",
 *    @OA\JsonContent(
 *       @OA\Property(property="name", type="string", example="Presta1"),
 *       @OA\Property(property="email", type="string", example="prestataire@prestataire.com"),
 *       @OA\Property(property="code_groupe", type="string", example="P789"),
 *       @OA\Property(property="telephone", type="string", example="06-********"),
 *       @OA\Property(property="adresse", type="string", example=""),
 *       @OA\Property(property="ville", type="string", example="Meknes"),
 *       @OA\Property(property="type_prestataire", type="string", example="DEPANEUSES"),
 *    ),
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */
/**
 * @OA\Post(
 * path="/api/v1/admin/pre/delete/{id}",
 * summary="Delete Prestataire",
 * description="Update Prestataire in Administration",
 * tags={"Administration - Prestataires"},
 * security={ {"apiAuth": {} }},
 * @OA\Parameter(
 *    description="ID of Prestataire",
 *    in="path",
 *    name="id",
 *    required=true,
 *    example="1",
 *    @OA\Schema(
 *       type="integer",
 *       format="int64"
 *    )
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */


/*********************** Assurances **************************/

/**
 * @OA\Get(
 * path="/api/v1/admin/assu/list",
 * summary="List Assurances",
 * description="List of Assurances in Administration",
 * tags={"Administration - Assurances"},
 * security={ {"apiAuth": {} }},
 * @OA\Response(
 *    response=200,
 *    description="Success"
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */

/**
 * @OA\Get(
 * path="/api/v1/admin/assu/get/{id}",
 * summary="Details Assurance",
 * description="Details Assurance in Administration",
 * tags={"Administration - Assurances"},
 * security={ {"apiAuth": {} }},
 * @OA\Parameter(
 *    description="ID of Assurance",
 *    in="path",
 *    name="id",
 *    required=true,
 *    example="1",
 *    @OA\Schema(
 *       type="integer",
 *       format="int64"
 *    )
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */
/**
 * @OA\Post(
 * path="/api/v1/admin/assu/create",
 * summary="Create Assurances",
 * description="Create Assurance in Administration",
 * tags={"Administration - Assurances"},
 * security={ {"apiAuth": {} }},
 * @OA\RequestBody(
 *    required=true,
 *    description="Pass user credentials",
 *    @OA\JsonContent(
 *       @OA\Property(property="nom", type="string", example="Assur1"),
 *       @OA\Property(property="email", type="string", example="assurance@assurance.com"),
 *       @OA\Property(property="code_societe", type="string", example="ASU789"),
 *       @OA\Property(property="id_assistance", type="string", example=""),
 *    ),
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */
/**
 * @OA\Post(
 * path="/api/v1/admin/assu/update/{id}",
 * summary="Update Assurance",
 * description="Update Assurance in Administration",
 * tags={"Administration - Assurances"},
 * security={ {"apiAuth": {} }},
 * @OA\Parameter(
 *    description="ID of Assurance",
 *    in="path",
 *    name="id",
 *    required=true,
 *    example="1",
 *    @OA\Schema(
 *       type="integer",
 *       format="int64"
 *    )
 * ),
 * @OA\RequestBody(
 *    required=true,
 *    description="Pass user credentials",
 *    @OA\JsonContent(
 *      @OA\Property(property="nom", type="string", example="Assur1"),
 *       @OA\Property(property="email", type="string", example="assurance@assurance.com"),
 *       @OA\Property(property="code_societe", type="string", example="ASU789"),
 *       @OA\Property(property="id_assistance", type="string", example=""),
 *    ),
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */
/**
 * @OA\Post(
 * path="/api/v1/admin/assu/delete/{id}",
 * summary="Delete Assurance",
 * description="Update Assurance in Administration",
 * tags={"Administration - Assurances"},
 * security={ {"apiAuth": {} }},
 * @OA\Parameter(
 *    description="ID of Assurance",
 *    in="path",
 *    name="id",
 *    required=true,
 *    example="1",
 *    @OA\Schema(
 *       type="integer",
 *       format="int64"
 *    )
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */


/*********************** Assistances **************************/

/**
 * @OA\Get(
 * path="/api/v1/admin/ass/list",
 * summary="List Assistances",
 * description="List of Assistances in Administration",
 * tags={"Administration - Assistances"},
 * security={ {"apiAuth": {} }},
 * @OA\Response(
 *    response=200,
 *    description="Success"
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */

/**
 * @OA\Get(
 * path="/api/v1/admin/ass/get/{id}",
 * summary="Details Assistance",
 * description="Details Assistance in Administration",
 * tags={"Administration - Assistances"},
 * security={ {"apiAuth": {} }},
 * @OA\Parameter(
 *    description="ID of Assistance",
 *    in="path",
 *    name="id",
 *    required=true,
 *    example="1",
 *    @OA\Schema(
 *       type="integer",
 *       format="int64"
 *    )
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */
/**
 * @OA\Post(
 * path="/api/v1/admin/ass/create",
 * summary="Create Assistances",
 * description="Create Assistance in Administration",
 * tags={"Administration - Assistances"},
 * security={ {"apiAuth": {} }},
 * @OA\RequestBody(
 *    required=true,
 *    description="Pass user credentials",
 *    @OA\JsonContent(
 *       @OA\Property(property="nom", type="string", example="Assi1"),
 *       @OA\Property(property="email", type="string", example="assistnace@Assistance.com"),
 *       @OA\Property(property="telephone", type="string", example="06-********"),
 *     ),
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */
/**
 * @OA\Post(
 * path="/api/v1/admin/ass/update/{id}",
 * summary="Update Assistance",
 * description="Update Assistance in Administration",
 * tags={"Administration - Assistances"},
 * security={ {"apiAuth": {} }},
 * @OA\Parameter(
 *    description="ID of Assistance",
 *    in="path",
 *    name="id",
 *    required=true,
 *    example="1",
 *    @OA\Schema(
 *       type="integer",
 *       format="int64"
 *    )
 * ),
 * @OA\RequestBody(
 *    required=true,
 *    description="Pass user credentials",
 *    @OA\JsonContent(
 *       @OA\Property(property="nom", type="string", example="Assi1"),
 *       @OA\Property(property="email", type="string", example="assistnace@Assistance.com"),
 *       @OA\Property(property="telephone", type="string", example="06-********"),
 *    ),
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */
/**
 * @OA\Post(
 * path="/api/v1/admin/ass/delete/{id}",
 * summary="Delete Assistance",
 * description="Update Assistance in Administration",
 * tags={"Administration - Assistances"},
 * security={ {"apiAuth": {} }},
 * @OA\Parameter(
 *    description="ID of Assistance",
 *    in="path",
 *    name="id",
 *    required=true,
 *    example="1",
 *    @OA\Schema(
 *       type="integer",
 *       format="int64"
 *    )
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */