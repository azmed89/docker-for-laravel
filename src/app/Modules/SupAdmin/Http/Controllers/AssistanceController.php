<?php
/**
 * Created by PhpStorm.
 * User: abderrahim akhdar
 * Date: 13/10/2020
 * Time: 18:41
 */


namespace App\Modules\SupAdmin\Http\Controllers;

use App\Enums\eResponseCode;
use App\Http\Controllers\Controller;
use App\Libs\Tools\UploadTrait;
use App\Models\Assistance;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

class AssistanceController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \DevcorpIt\ResponseCode\JsonResp
     */

    public function all(Request $request)
    {
        $assistances = Assistance::all();
        return $this->resp->ok(eResponseCode::AS_LISTED_200_00, $assistances);
    }

    public function get(Assistance $assistance)
    {
        if (!$assistance)
            return $this->resp->ok(eResponseCode::AS_404_NOT_FOUND);
        return $this->resp->ok(eResponseCode::AS_GET_200_01, $assistance);
    }


    /**
     * Store a newly created pres.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \DevcorpIt\ResponseCode\JsonResp
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => ['required', 'email', Rule::unique('assistances')],
            'nom' => 'required|string|max:150',
            'telephone'=>'required|string|max:20'
        ]);

        if ($validator->fails()) {
            $failedRules = $validator->failed();
            if (isset($failedRules['email']['Unique'])) {
                return $this->resp->not_acceptable(eResponseCode::EMAIL_TAKEN);
            }

            return $this->resp->bad_request($validator);
        }

        $data = $request->all();

        $assistance = Assistance::create($data);
        return $this->resp->ok(eResponseCode::AS_CREATE_200_02, $assistance);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Assistance $assistance
     * @return \DevcorpIt\ResponseCode\JsonResp
     */
    public function update(Request $request, Assistance $assistance)
    {
        $validator = Validator::make($request->all(), [
            'email' => ['email', Rule::unique('assistances')->ignore($assistance)],
            'nom' => 'required|string|max:150',
            'telephone'=>'required|string|max:20'
        ]);
        if ($validator->fails()) {
            $failedRules = $validator->failed();
            if (isset($failedRules['email']['Unique'])) {
                return $this->resp->not_acceptable(eResponseCode::EMAIL_TAKEN);
            }
            return $this->resp->bad_request($validator);
        }
        $assistance->update($request->all());

        return $this->resp->ok(eResponseCode::AS_UPDATE_200_03, $assistance);
    }


    /**
     * Delete a specified resource in storage.
     *
     * @param Assistance $assistance
     * @return \DevcorpIt\ResponseCode\JsonResp
     */
    public function delete(Assistance $assistance)
    {
        if (empty($assistance)) {
            return $this->resp->not_found(eResponseCode::AS_404_NOT_FOUND);
        }
        $assistance->delete();
        return $this->resp->ok(eResponseCode::AS_DELETE_200_04);
    }

}