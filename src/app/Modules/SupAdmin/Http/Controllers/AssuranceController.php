<?php
/**
 * Created by PhpStorm.
 * User: abderrahim akhdar
 * Date: 13/10/2020
 * Time: 17:39
 */


namespace App\Modules\SupAdmin\Http\Controllers;

use App\Enums\eResponseCode;
use App\Http\Controllers\Controller;
use App\Libs\Tools\UploadTrait;
use App\Models\Assurance;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

class AssuranceController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \DevcorpIt\ResponseCode\JsonResp
     */

    public function all(Request $request)
    {
        $assurances = Assurance::all();
        return $this->resp->ok(eResponseCode::A_LISTED_200_00, $assurances);
    }

    public function get(Assurance $assurance)
    {
        if (!$assurance)
            return $this->resp->ok(eResponseCode::A_404_NOT_FOUND);
        return $this->resp->ok(eResponseCode::A_GET_200_01, $assurance);
    }


    /**
     * Store a newly created pres.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \DevcorpIt\ResponseCode\JsonResp
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => ['required', 'email', Rule::unique('assurances')],
            'code_societe' => 'required|string|max:150',
            'nom' => 'required|string|max:150',
            'id_assistance' => 'required|integer|max:20',
        ]);

        if ($validator->fails()) {
            $failedRules = $validator->failed();
            if (isset($failedRules['email']['Unique'])) {
                return $this->resp->not_acceptable(eResponseCode::EMAIL_TAKEN);
            }

            return $this->resp->bad_request($validator);
        }

        $data = $request->all();

        $assurance = Assurance::create($data);
        return $this->resp->ok(eResponseCode::A_CREATE_200_02, $assurance);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Prestataire $prestataire
     * @return \DevcorpIt\ResponseCode\JsonResp
     */
    public function update(Request $request, Assurance $assurance)
    {
        $validator = Validator::make($request->all(), [
            'email' => ['email', Rule::unique('assurances')->ignore($assurance)],
            'code_societe' => 'required|string|max:150',
            'nom' => 'required|string|max:150',
            'id_assistance' => 'required|integer|max:20',
        ]);
        if ($validator->fails()) {
            $failedRules = $validator->failed();
            if (isset($failedRules['email']['Unique'])) {
                return $this->resp->not_acceptable(eResponseCode::EMAIL_TAKEN);
            }
            return $this->resp->bad_request($validator);
        }
        $assurance->update($request->all());

        return $this->resp->ok(eResponseCode::A_UPDATE_200_03, $assurance);
    }


    /**
     * Delete a specified resource in storage.
     *
     * @param Prestataire $prestataire
     * @return \DevcorpIt\ResponseCode\JsonResp
     */
    public function delete(Assurance $assurance)
    {
        if (empty($assurance)) {
            return $this->resp->not_found(eResponseCode::A_404_NOT_FOUND);
        }
        $assurance->delete();
        return $this->resp->ok(eResponseCode::A_DELETE_200_04);
    }

}

