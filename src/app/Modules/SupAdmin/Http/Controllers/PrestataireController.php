<?php
/**
 * Created by PhpStorm.
 * User: abderrahim akhdar
 * Date: 13/10/2020
 * Time: 15:33
 */

namespace App\Modules\SupAdmin\Http\Controllers;

use App\Enums\eResponseCode;
use App\Http\Controllers\Controller;
use App\Libs\Tools\UploadTrait;
use App\Models\Prestataire;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use App\Enums\eTypePrestataire;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Notification;
use App\Notifications\ActivationCompte;

class PrestataireController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \DevcorpIt\ResponseCode\JsonResp
     */

    public function all(Request $request)
    {
        $prestataires = Prestataire::all();
        return $this->resp->ok(eResponseCode::P_LISTED_200_00, $prestataires);
    }

    public function get(Prestataire $prestataire)
    {
        if(!$prestataire)
            return $this->resp->ok(eResponseCode::P_404_NOT_FOUND);
        return $this->resp->ok(eResponseCode::P_GET_200_01, $prestataire);
    }


    /**
     * Store a newly created pres.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \DevcorpIt\ResponseCode\JsonResp
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email'=> ['required', 'email', Rule::unique('prestataires')],
            'code_groupe'=> 'required|string|max:150',
            'name'=> 'required|string|max:150',
            'adresse'=>'required|string|max:255',
            'ville'=> 'required|string|max:150',
            'telephone'=> 'required|string|max:20',
            'type_prestataire'=> Rule::in(eTypePrestataire::getAll(eTypePrestataire::class)),
        ]);

        if ($validator->fails()){
            $failedRules = $validator->failed();
            if (isset($failedRules['email']['Unique'])) {
                return $this->resp->not_acceptable(eResponseCode::EMAIL_TAKEN);
            }

            return $this->resp->bad_request($validator);
        }

        $data = $request->all();

        $user = Prestataire::create($data);
        return $this->resp->ok(eResponseCode::P_CREATE_200_02, $user);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Prestataire $prestataire
     * @return \DevcorpIt\ResponseCode\JsonResp
     */
    public function update(Request $request, Prestataire $prestataire)
    {
        $validator = Validator::make($request->all(), [
            'email'=> ['email', Rule::unique('prestataires')->ignore($prestataire)],
            'code_groupe'=> 'string|max:150',
            'nom'=> 'string|max:150',
            'adresse'=>'string|max:255',
            'ville'=> 'string|max:150',
            'telephone'=> 'required|string|max:20',
            'role'=> Rule::in(eTypePrestataire::getAll(eTypePrestataire::class)),
        ]);
        if ($validator->fails()){
            $failedRules = $validator->failed();
            if (isset($failedRules['email']['Unique'])) {
                return $this->resp->not_acceptable(eResponseCode::EMAIL_TAKEN);
            }
            return $this->resp->bad_request($validator);
        }
        $prestataire->update($request->all());

        return $this->resp->ok(eResponseCode::P_UPDATE_200_03, $prestataire);
    }


    /**
     * Delete a specified resource in storage.
     *
     * @param Prestataire $prestataire
     * @return \DevcorpIt\ResponseCode\JsonResp
     */
    public function delete(Prestataire $prestataire)
    {
        if(empty($prestataire)) {
            return $this->resp->not_found(eResponseCode::P_404_NOT_FOUND);
        }
        $prestataire->delete();
        return $this->resp->ok(eResponseCode::P_DELETE_200_04);
    }



























}