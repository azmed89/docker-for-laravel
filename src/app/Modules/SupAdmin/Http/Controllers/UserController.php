<?php
/**
 * Created by PhpStorm.
 * User: AZMed
 * Date: 13/10/2020
 * Time: 11:50
 */

namespace App\Modules\SupAdmin\Http\Controllers;

use App\Enums\eResponseCode;
use App\Http\Controllers\Controller;
use App\Libs\Tools\UploadTrait;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

class UserController extends Controller
{

    use UploadTraitrait;


    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \DevcorpIt\ResponseCode\JsonResp
     */

    public function all(Request $request)
    {
        $users = User::all();
        return $this->resp->ok(eResponseCode::U_LISTED_200_00, $users);
    }


    /**
     * Display the specified user.
     *
     * @param User $user
     * @return \DevcorpIt\ResponseCode\JsonResp
     */
    public function get(User $user)
    {
        if(!$user)
            return $this->resp->ok(eResponseCode::U_404_NOT_FOUND, $user);
        return $this->resp->ok(eResponseCode::U_GET_200_01, $user);
    }


    /**
     * Store a newly created user assistance.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \DevcorpIt\ResponseCode\JsonResp
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email'=> ['required', 'email', Rule::unique('users')],
            'prenom'=> 'required|string|max:150',
            'nom'=> 'required|string|max:150',
            'phone'=> 'required|string|max:20'
        ]);

        if ($validator->fails()){
            $failedRules = $validator->failed();
            if (isset($failedRules['email']['Unique'])) {
                return $this->resp->not_acceptable(eResponseCode::EMAIL_TAKEN);
            }

            return $this->resp->bad_request($validator);
        }

        $data = $request->all();
        $pass = Str::random(8);

        $password = Hash::make($pass);
        $data['password'] = $password;

        $user = User::create($data);
        return $this->resp->ok(eResponseCode::U_CREATE_200_02, $user);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param User $user
     * @return \DevcorpIt\ResponseCode\JsonResp
     */
    public function update(Request $request, User $user)
    {
        $validator = Validator::make($request->all(), [
            'email'=> ['email', Rule::unique('users')->ignore($user)],
            'nom'=> 'string|max:150',
            'prenom'=> 'string|max:150',
            'telephone'=> 'required|string|max:20'
        ]);
        if ($validator->fails()){
            $failedRules = $validator->failed();
            if (isset($failedRules['email']['Unique'])) {
                return $this->resp->not_acceptable(eResponseCode::EMAIL_TAKEN);
            }
            return $this->resp->bad_request($validator);
        }
        $user->update($request->all());

        return $this->resp->ok(eResponseCode::U_UPDATE_200_03, $user);
    }


    /**
     * Delete a specified resource in storage.
     *
     * @param User $user
     * @return \DevcorpIt\ResponseCode\JsonResp
     */
    public function delete(User $user)
    {
        if(empty($user)) {
            return $this->resp->not_found(eResponseCode::U_404_NOT_FOUND);
        }
        $user->delete();
        return $this->resp->ok(eResponseCode::U_DELETE_200_04);
    }


    /**
     * Update user profile.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \DevcorpIt\ResponseCode\JsonResp
     */
    public function updateUserProfil(Request $request)
    {
        $user = $request->user();

        $validator = Validator::make($request->all(), [
            'email'=> ['email', Rule::unique('users')->ignore($user)],
            'password'=> 'sometimes|nullable|string|min:6',
            'nom'=> 'string|max:150',
            'prenom'=> 'string|max:150',
            'telephone'=> 'string|max:20'
        ]);

        if ($validator->fails()){
            $failedRules = $validator->failed();
            if (isset($failedRules['email']['Unique'])) {
                return $this->resp->not_acceptable(eResponseCode::EMAIL_TAKEN);
            }

            return $this->resp->bad_request($validator);
        }

        $data = $request->all();

        if(empty($data['password'])){
            unset($data['password']);
        } else {
            $data['password'] = Hash::make($data['password']);
        }
        $user->update($data);

        return $this->resp->ok(eResponseCode::U_UPDATE_200_03, $user);
    }
}
