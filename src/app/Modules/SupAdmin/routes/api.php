<?php
/**
 * Created by PhpStorm.
 * User: AZMed
 * Date: 13/10/2020
 * Time: 11:50
 */

Route::group(['prefix' => '/api/v1/admin/user', 'module' => 'SupAdmin'], function() {

    Route::middleware(['auth.jwt', 'auth.user', 'api'])->group(function () {
        Route::get('/list', 'UserController@all');
        Route::get('/get/{user}', 'UserController@get');
        Route::post('/create', 'UserController@create');
        Route::post('/update/{user}', 'UserController@update');
        Route::post('/delete/{user}', 'UserController@delete');
        Route::post('/profil/update', 'UserController@updateUserProfil');
    });
});

Route::group(['prefix' => '/api/v1/admin/pre', 'module' => 'SupAdmin'], function() {

    Route::middleware(['auth.jwt', 'auth.user', 'api'])->group(function () {
        Route::get('/list', 'PrestataireController@all');
        Route::get('/get/{prestataire}', 'PrestataireController@get');
        Route::post('/create', 'PrestataireController@create');
        Route::post('/update/{prestataire}', 'PrestataireController@update');
        Route::post('/delete/{prestataire}', 'PrestataireController@delete');
    });
});

Route::group(['prefix' => '/api/v1/admin/assu', 'module' => 'SupAdmin'], function() {

    Route::middleware(['auth.jwt', 'auth.user', 'api'])->group(function () {
        Route::get('/list', 'AssuranceController@all');
        Route::get('/get/{assurance}', 'AssuranceController@get');
        Route::post('/create', 'AssuranceController@create');
        Route::post('/update/{assurance}', 'AssuranceController@update');
        Route::post('/delete/{assurance}', 'AssuranceController@delete');
    });
});

Route::group(['prefix' => '/api/v1/admin/ass', 'module' => 'SupAdmin'], function() {

    Route::middleware(['auth.jwt', 'auth.user', 'api'])->group(function () {
        Route::get('/list', 'AssistanceController@all');
        Route::get('/get/{assistance}', 'AssistanceController@get');
        Route::post('/create', 'AssistanceController@create');
        Route::post('/update/{assistance}', 'AssistanceController@update');
        Route::post('/delete/{assistance}', 'AssistanceController@delete');
    });
});