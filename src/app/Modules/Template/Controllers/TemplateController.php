<?php

namespace App\Modules\Template\Controllers;

use App\Enum\eResponseCode;
use App\Http\Controllers\Controller;
use App\Modules\Template\Models\Template;
use App\Modules\Template\Models\TemplateBody;
use App\Modules\Template\Services\TemplateNotificationService;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Validator;

class TemplateController extends Controller
{
    const LANG = 'FR';

    public function listTemplates(Request $request)
    {
        //if(!$request->user()->can('list', Template::class))
        //    return $this->resp->forbidden(eResponseCode::_403_NOT_AUTHORIZED);

        $templates = Template::paginate($this->row_number()*2);

        return $this->resp->ok(eResponseCode::TM_LISTED_200_00,$templates);
    }

    public function getTemplates(Request $request)
    {
        //if(!$request->user()->can('view', Template::class))
        //    return $this->resp->forbidden(eResponseCode::_403_NOT_AUTHORIZED);

        $validator = Validator::make($request->all(),  [
            'code' => 'required|string',
            //'lang' => 'required|string'
        ]);

        if ($validator->fails()) {

            return $this->resp->bad_request($validator);
        }

        $template = Template::where('code', '=', $request->code)->join('templates_body', function ($join) use ($request) {
            $join->on('templates.id_template', '=', 'templates_body.id_template')
                ->where('templates_body.lang', '=', self::LANG);
        })->get();

        if (empty($template)) {
            return $this->resp->not_found(eResponseCode::TM_404_NOT_FOUND);
        }

        return $this->resp->ok(eResponseCode::TM_GET_200_01, $template);
    }

    public function createTemplateBody(Request $request)
    {
        /*if(!$request->user()->can('create', User::class))
            return $this->resp->guessResponse(eResponseCode::_403_NOT_AUTHORIZED);*/

        $validator = Validator::make($request->all(), [
            'code' => 'required|string',
            'subject' => 'required|string',
            'body' => 'required|string',
        ]);

        if ($validator->fails()) {
            return $this->resp->bad_request($validator);
        }

        $template_id = Template::where('code', $request->code)->value('id_template');

        if (empty($template_id)) {
            return $this->resp->ok(eResponseCode::TM_404_NOT_FOUND);
        }

        $template_body = TemplateBody::where('id_template', $template_id)->first();

        if(!empty($template_body)){
            // T200_40
            return $this->resp->ok(eResponseCode::TM_DUPLICATE_200_04);
        }

        $template_body = new TemplateBody();
        $template_body->subject = $request->subject;
        $template_body->body = $request->body;
        $template_body->lang = self::LANG;
        $template_body->id_template =  $template_id;

        if (!$template_body->save()) {
            return $this->resp->internal_error();
        }

        return $this->resp->ok(eResponseCode::TM_CREATED_200_02, $template_body);
    }

    public function setTemplate(Request $request)
    {
        //if(!$request->user()->can('update', Template::class))
         //   return $this->resp->forbidden(eResponseCode::_403_NOT_AUTHORIZED);

        $validator = Validator::make($request->all(), [
            'code' => 'required|string',
            //'lang' => 'required|string',
            'mode' => 'required|string',
            'enabled' => 'nullable|boolean',
            'subject' => 'nullable|string',
            'body' => 'nullable|string',
        ]);

        if ($validator->fails()) {
            return $this->resp->bad_request($validator);
        }

        $template = Template::where('code', $request->code)->where('mode', $request->mode)->join('templates_body',
            function ($join) use ($request) {
                $join->on('templates.id_template', 'templates_body.id_template')
                    ->where('templates_body.lang', self::LANG);
            })->first();

        if (empty($template)) {
            return $this->resp->not_found(eResponseCode::TM_404_NOT_FOUND);
        }

        if ($request->filled('enabled')) {
            $template->enabled = $request->enabled;

            if (!$template->save()) {
                return $this->resp->internal_error();
            }
        }

        if (!empty($template->bodies)) {
            $template_body = $template->bodies->first();
            if ($request->filled('subject')) {
                $template->subject = $request->subject;
                $template_body->subject = $request->subject;
            }

            if ($request->filled('body')) {
                $template->body = $request->body;
                $template_body->body = $request->body;
            }

            if (!$template_body->save()) {
                return $this->resp->internal_error();
            }
        }

        unset($template->bodies);

        return $this->resp->ok(eResponseCode::TM_CREATED_200_02, $template);
    }
}
