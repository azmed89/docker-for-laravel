<?php
/**
 * Created by PhpStorm.
 * User: ABDows
 * Date: 22/06/2018
 * Time: 17:51
 */
namespace App\Modules\Template\Libs;

use App\Modules\Template\Models\Template;

class PlaceHolderResolver
{

    public static function resolve(Template $template)
    {

        if (is_null($template)) return null;
        $prepareKeys = self::prepareKeys();

        $pattern = config('templates.pattern');
        //dd($pattern);
        preg_match_all($pattern, $template->getSubject(), $object_matches, PREG_SET_ORDER);
        preg_match_all($pattern, $template->getBody(), $body_matches, PREG_SET_ORDER);

        return self::processPlaceHolders(array_merge($object_matches,$body_matches),$prepareKeys);
    }

    protected static function prepareKeys(){
        $keys = [];
        foreach (config('templates.place_holders') as $object => $properties){
           // dd($object);
            foreach ($properties as $key => $name){

                if(is_array($name)){
                    $name = array_values($name)[0];
                }
                if(is_numeric($key)){

                    $keys["$object:$name"] = "$object:$name";
                }else{
                    $keys["$object:$key"] = "$object:$name";
                }
            }
        }
        return $keys;
    }

    protected static function processPlaceHolders($matches, $preparedKeys){
        $place_holders = [];

        $prepared_keys = array_keys($preparedKeys);
        foreach ($matches as $item){
            if(in_array($item[1],$prepared_keys)){
                $current_value = $preparedKeys[$item[1]];
                $type = ($current_value == $item[1]) ? 'attribute' : 'function';
                list($alias_name,$property_name) = explode(':',$current_value);
                //dd($alias_name,$property_name);
                $place_holders[$item[0]] = ['type' => $type, 'name' => $property_name, 'alias' => $alias_name , 'class_name' => config("templates.alias.$alias_name")];
            }
        }
        return $place_holders;
    }



    /*
     * Array
    (
    [0] => Array
        (
            [0] => @{guest:first_name}
            [1] => guest:first_name
        )

    [1] => Array
        (
            [0] => @{tontine:name}
            [1] => tontine:name
        )

    )
    */

}
