<?php
/**
 * Created by PhpStorm.
 * User: ABDows
 * Date: 12/07/2018
 * Time: 18:08
 */
namespace App\Modules\Template\Models;

class Entity
{
    private static $_instance = null;

    public static function getInstance() {

        if(is_null(self::$_instance)) {
            self::$_instance = new Entity();
        }

        return self::$_instance;
    }

    private function __construct()
    {
        $entity_attributs = config('templates.entity');
        if(is_array($entity_attributs)){
            foreach ($entity_attributs as $key => $value){
                $this->$key = $value;
            }
        }
    }
}
