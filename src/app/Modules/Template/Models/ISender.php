<?php
/**
 * Created by PhpStorm.
 * User: devco
 * Date: 18/05/2020
 * Time: 00:50
 */

namespace App\Modules\Template\Models;


interface ISender
{
    public static function sendEmail($code,$notifiable_model,array $modelsArg,$lang);
    public static function sendSms($code,array $modelsArg,$lang);
}
