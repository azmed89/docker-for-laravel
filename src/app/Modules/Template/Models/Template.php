<?php
/**
 * Created by PhpStorm.
 * User: ABDows
 * Date: 22/06/2018
 * Time: 15:08
 */

namespace App\Modules\Template\Models;

use App\Modules\Template\Libs\PlaceHolderResolver;
use App\Notifications\EmailNotification;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Log;
use App\Libs\Sms\SmsSender;

class Template extends Model
{
    use Notifiable;

    protected $primaryKey = 'id_template';

    protected $table = 'templates';

    public $timestamps = false;

    public static $entity = null;

    protected $subject = null;

    protected $body = null;

    protected $email,$telephone = null;

    public function sendNotification($notifiable_model,array $models){

        $this->resolve($notifiable_model,$models);
        $this->notify(new EmailNotification($this->subject,$this->body));
    }

    public function getSubject(){return $this->subject;}
    public function setSubject($subject){$this->subject = $subject;}
    public function getBody(){return $this->body;}
    public function setBody($body){$this->body = $body;}

    public function bodies()
    {
        return $this->hasMany(TemplateBody::class,'id_template');
    }

    public static function get($code, $lang, $mode = null, $enabled = null){
        $template = Template::where('code', $code);
        if (!is_null($mode)) {
            $template = $template->whereIn('mode', $mode);
        }
        if (!is_null($enabled)) {
            $template = $template->where('enabled', $enabled);
        }
        return $template->with(['bodies' => function($query) use ($lang) {$query->where('lang', $lang);}])->get();
    }

    public function resolve($notifiable_model,array $modelsArg)
    {

        $this->fillNotifiableFields($notifiable_model,$modelsArg);
        $this->fillTransactionalFields();

        $placeholders = PlaceHolderResolver::resolve($this);
//       $models = $this->getModels($modelsArg);
       // dd($placeholders);

        $models = $modelsArg;
      //dd($models);
        foreach ($placeholders as $place_holder => $bind) {

            //dd($placeholders);
            if (empty($bind['class_name']) || !array_key_exists($bind['class_name'], $models)) {
                continue;
            }
            
            $_model = $models[$bind['class_name']];
            $property = $bind['name'];

            if ($bind['type'] == 'function') {
                $value = $_model->{$property}();
            } else {
                $value = $_model->{$property};
            }

            $this->body = str_replace($place_holder, $value, $this->body);

            if(!is_null($this->subject)){
                $this->subject = str_replace($place_holder, $value, $this->subject);
            }
        }

        return true;
    }

    public function fillTransactionalFields(){
        $data = $this->bodies()->first();
        if(!is_null($data)){
            $this->body = $data->body;
            $this->subject = $data->subject;
            return true;
        }

        return false;
    }

    private function fillNotifiableFields($notifiable_model,$models){


        $notifiable_model = $models[$notifiable_model];
        $this->email = $notifiable_model->email;
        $this->telephone = $notifiable_model->telephone;

        return true;
    }

    public function textTo($phone){
        return SmsSender::confirm($phone,$this->body,config('sms.default_phone_code'));
    }
}
