<?php
/**
 * Created by PhpStorm.
 * User: ABDows
 * Date: 22/06/2018
 * Time: 15:08
 */

namespace App\Modules\Template\Models;

use Illuminate\Database\Eloquent\Model;

class TemplateBody extends Model
{

    protected $primaryKey = 'id_tbody';

    protected $table = 'templates_body';

    public $timestamps = false;

    public function template()
    {
        return $this->hasOne(Template::class,'id_template','id_template');
    }
}
