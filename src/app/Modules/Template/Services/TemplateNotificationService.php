<?php
/**
 * Created by PhpStorm.
 * User: Anas
 * Date: 29/05/2020
 * Time: 12:15
 */

namespace App\Modules\Template\Services;

use App\Modules\Template\Models\Entity;
use App\Modules\Template\Models\ISender;
use App\Modules\Template\Models\Template;
use Illuminate\Support\Facades\Log;

class TemplateNotificationService extends \Monolog\Handler\SwiftMailerHandler implements ISender
{
    public static $entity = null;

    public static function sendEmail($code,$notifiable_model,array $modelsArg ,$lang ='FR'){

        $models = self::checkIfContainsNotifiableModel($notifiable_model,$modelsArg);
       // dd($models);

        if($models){
            $mode = 'MAIL';
            $template = Template ::where('code', $code)->where('mode', $mode)->with(['bodies' => function($q) use ($lang) {
                $q->where('lang', $lang);
            }])->first();
            //dd($template);
            if(empty($template)){
               Log::info('Template with Code : '.$code.' & Mode : '.$mode.' Not Found');

               return false;
            }

            try {
               $template->sendNotification($notifiable_model,$models);
            } catch(\Exception $e) {
                dd($e->getMessage());
              Log::critical($e->getMessage());
            }   

            return true;
        }

        return false;
    }

    public static function sendSms($code,array $modelsArg,$lang = 'FR'){
        return self::checkIfContainsNotifiableModel();
    }

    private static function checkIfContainsNotifiableModel($notifiable_model,$modelsArg)
    {
        $models = self::getModels($modelsArg);
       // dd($modelsArg,$models);

        $notifiable_model_name = $notifiable_model;


        if(array_key_exists( $notifiable_model_name, $models)){
            $notifiable_model = $models[$notifiable_model_name];
            //dd($notifiable_model);
            if(empty($notifiable_model->email) or empty($notifiable_model->telephone )){
                return false;
            }
        } else {
           return false;
        }
        //dd($models);
        return $models;
    }

    private static function getModels($modelsArg)
    {

        if(is_null(self::$entity)){
            self::$entity = Entity::getInstance();
        }


        $models = [get_class(self::$entity) => self::$entity];

        foreach ($modelsArg as $mdl) {
            $models[get_class($mdl)] = $mdl;

        }
        return $models;
    }
}
