<?php
/**
 * Created by PhpStorm.
 * User: Anas
 * Date: 26/05/2020
 * Time: 17:28
 */
Route::group(['module' => 'Template', 'prefix' => 'api/bo/template', 'namespace' => '\App\Modules\Template\Controllers'], function () {
    Route::match(['GET', 'POST'], 'list', 'TemplateController@listTemplates');
    Route::post('get', 'TemplateController@getTemplates');
    Route::post('create', 'TemplateController@createTemplateBody');
    Route::post('update', 'TemplateController@setTemplate');
});
