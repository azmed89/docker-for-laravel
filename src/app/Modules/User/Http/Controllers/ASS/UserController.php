<?php



namespace App\Modules\User\Http\Controllers\ASS;

use App\Enums\eRoleAssistance;
use App\Http\Controllers\Controller;
use App\Models\UserAssistance;
use Illuminate\Http\Request;
use App\Enums\eResponseCode;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use App\Libs\Tools\UploadTrait;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Notification;
use App\Notifications\ActivationCompte;

class UserController extends Controller
{

    use UploadTrait;


    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \DevcorpIt\ResponseCode\JsonResp
     */

    public function all(Request $request)
    {
        $users = UserAssistance::all();
        return $this->resp->ok(eResponseCode::U_LISTED_200_00, $users);
    }


    /**
     * Display the specified user.
     *
     * @param User $user
     * @return \DevcorpIt\ResponseCode\JsonResp
     */

    public function get(UserAssistance $user)
    {
        if(!$user)
            return $this->resp->ok(eResponseCode::U_404_NOT_FOUND, $user);
        return $this->resp->ok(eResponseCode::U_GET_200_01, $user);
    }


    /**
     * Store a newly created user assistance.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \DevcorpIt\ResponseCode\JsonResp
     */

    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email'=> ['required', 'email', Rule::unique('user_assistances')],
            'prenom'=> 'required|string|max:150',
            'nom'=> 'required|string|max:150',
            'password' => 'required|string|max:150',
            'telephone'=> 'required|string|max:150',
            'avatar'=> 'nullable|file|image',
            'role'=> Rule::in(eRoleAssistance::getAll(eRoleAssistance::class)),
        ]);

        if ($validator->fails()){
            $failedRules = $validator->failed();
            if (isset($failedRules['email']['Unique'])) {
                return $this->resp->not_acceptable(eResponseCode::EMAIL_TAKEN);
            }

            return $this->resp->bad_request($validator);
        }

        $data = $request->all();
        $data['password'] = Hash::make($data['password']);
        $data['id_assistance'] = $request->user()->id_assistance;
        if ($request->hasFile('avatar')) {
            $avatar=$request->file('avatar');
            $upload = $this->uploadOne($avatar, config('cdn.avatars.path'));
            $data['avatar'] = $upload['file_name'];
        }

        $user = UserAssistance::create($data);
        $reset_token=$user->generateToken($user);
        $user->notify(new ActivationCompte($reset_token));

        return $this->resp->ok(eResponseCode::U_CREATE_200_02, $user);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param UserAssistance $user
     * @return \DevcorpIt\ResponseCode\JsonResp
     */
    public function update(Request $request, UserAssistance $user)
    {
        $validator = Validator::make($request->all(), [
            'email'=> ['email', Rule::unique('user_assistances')->ignore($user)],
            'nom'=> 'string|max:150',
            'prenom'=> 'string|max:150',
            'telephone'=> 'required|string|max:20',
            'role'=> Rule::in(eRoleAssistance::getAll(eRoleAssistance::class)),
        ]);
        if ($validator->fails()){
            $failedRules = $validator->failed();
            if (isset($failedRules['email']['Unique'])) {
                return $this->resp->not_acceptable(eResponseCode::EMAIL_TAKEN);
            }
            return $this->resp->bad_request($validator);
        }

        $data = $request->all();

        $data['id_assistance']= $request->user()->id_assistance ;

        if(!isset($data['password']) || empty($data['password'])) unset($data['password']);
        else $data['password'] = Hash::make($data['password']);

        if ($request->hasFile('avatar')) {
            $avatar=$request->file('avatar');
            $upload = $this->uploadOne($avatar, config('cdn.avatars.path'));
            $data['avatar'] = $upload['file_name'];
        }

        $user->update($data);

        return $this->resp->ok(eResponseCode::U_UPDATE_200_03, $user);
    }


    /**
     * Delete a specified resource in storage.
     *
     * @param UserAssistance $user
     * @return \DevcorpIt\ResponseCode\JsonResp
     */
    public function delete(UserAssistance $user)
    {
        if(empty($user)) {
            return $this->resp->not_found(eResponseCode::U_404_NOT_FOUND);
        }
        $user->delete();
        return $this->resp->ok(eResponseCode::U_DELETE_200_04);
    }


    /**
     * Update user profile.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \DevcorpIt\ResponseCode\JsonResp
     */
    public function updateUserProfil(Request $request)
    {
        $user = $request->user();

        $validator = Validator::make($request->all(), [
            'email'=> ['email', Rule::unique('user_assistances')->ignore($user)],
            'password'=> 'sometimes|nullable|string|min:6',
            'nom'=> 'string|max:150',
            'prenom'=> 'string|max:150',
            'telephone'=> 'string|max:20'
        ]);

        if ($validator->fails()){
            $failedRules = $validator->failed();
            if (isset($failedRules['email']['Unique'])) {
                return $this->resp->not_acceptable(eResponseCode::EMAIL_TAKEN);
            }

            return $this->resp->bad_request($validator);
        }

        $data = $request->all();

        if(empty($data['password'])){
            unset($data['password']);
        } else {
            $data['password'] = Hash::make($data['password']);
        }
        $user->update($data);

        return $this->resp->ok(eResponseCode::U_UPDATE_200_03, $user);
    }

    public function search(Request $request )
    {

        $validator = Validator::make($request->all(), [
            'search'=> 'nullable|string',
        ]);

        if ($validator->fails()){
            return $this->resp->bad_request($validator);
        }
        $keyword = $request->get('search');
        if($keyword!=''){
            $users = UserAssistance::
            where("nom", "LIKE","%$keyword%")
            ->orWhere("prenom","LIKE","%$keyword%")
            ->orWhere("email","LIKE","%$keyword%")
            ->where('id_assistance',$request->user()->id_assistance)
            ->get();
            return $this->resp->ok(eResponseCode::U_LISTED_200_00, $users);
        }
        $users = UserAssistance::where('id_assistance',$request->user()->id_assistance)->get();

        return $this->resp->ok(eResponseCode::U_LISTED_200_00,$users );

    }











}
