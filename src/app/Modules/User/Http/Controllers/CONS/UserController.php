<?php



namespace App\Modules\User\Http\Controllers\CONS;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Models\Constateur;
use App\Enums\eResponseCode;
use App\Enums\eRole;
use App\Enums\eRolePrestataire;

use App\Libs\Tools\UploadTrait;

class UserController extends Controller
{

    use UploadTrait;

    /**
     * Update constateur profile.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \DevcorpIt\ResponseCode\JsonResp
     */
    public function updateUserProfil(Request $request)
    {
        $user = $request->user();

        $validator = Validator::make($request->all(), [
            'email'=> ['email', Rule::unique('constateurs')->ignore($user)],
            'password'=> 'sometimes|nullable|string|min:6',
            'nom'=> 'string|max:150',
            'prenom'=> 'string|max:150',
            'telephone'=> 'string|max:20',
            'avatar' => 'image|mimes:jpeg,png,jpg,gif',
            'adresse' => 'string|max:500',
            'fonction' => 'string|max:250',
            'num_agent' => 'string|max:150',
            'acces' => 'string|max:150',
            'identifiant' => Rule::unique('constateurs', 'identifiant')->ignore($user)
        ]);

        if ($validator->fails()){
            $failedRules = $validator->failed();
            if (isset($failedRules['email']['Unique'])) {
                return $this->resp->not_acceptable(eResponseCode::EMAIL_TAKEN);
            }
            if (isset($failedRules['identifiant']['Unique'])) {
                return $this->resp->not_acceptable(eResponseCode::C_IDENTIFIANT_TAKEN);
            }

            return $this->resp->bad_request($validator);
        }

        $data = $request->all();

        if ($request->hasFile('avatar')) {
            // Upload new file
            $image_info = $this->uploadOne($request->avatar, config('cdn.constateur.path'));

            // Refactoring request with a proper name for image column
            $data = array_merge($request->except(['avatar']), ['avatar' => $image_info['file_name']]);
        }

        if(empty($data['password'])){
            unset($data['password']);
            $user->update($data);

        } else {
            $user->update($data);
        }

        return $this->resp->ok(eResponseCode::U_UPDATE_200_03, $user);
    }













}
