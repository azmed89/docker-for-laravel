<?php



namespace App\Modules\User\Http\Controllers\PRE;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Models\Constateur;
use App\Enums\eResponseCode;
use App\Enums\eRole;
use App\Enums\eRolePrestataire;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Notification;
use App\Notifications\ActivationCompte;

use App\Libs\Tools\UploadTrait;

class UserConstateurController extends Controller
{

    use UploadTrait;


    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \DevcorpIt\ResponseCode\JsonResp
     */

    public function all(Request $request)
    {
        $constateur= Constateur::where('id_prestataire',$request->user()->id_prestataire)->get();
        return $this->resp->ok(eResponseCode::U_LISTED_200_00, $constateur);
    }



    /**
     * Display the specified Constateur.
     *
     * @param Constateur $constateur
     * @return \DevcorpIt\ResponseCode\JsonResp
     */
    public function get(Constateur $constateur)
    {
        if(!$constateur)
            return $this->resp->ok(eResponseCode::U_404_NOT_FOUND, $constateur);
        return $this->resp->ok(eResponseCode::U_GET_200_01, $constateur);
    }




    /**
     * Store a newly created Constateur/Gestionnaire.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \DevcorpIt\ResponseCode\JsonResp
     */
    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
           'nom'=> 'required|string|max:150',
           'prenom'=> 'required|string|max:150',
           'email'=> ['required', 'email',Rule::unique('constateurs')],
           'password'=>'required|string|max:20',
           'telephone'=> 'required|string|max:20',
           'identifiant' => 'string|max:20',
           'adresse'=>'string|max:255',
           'num_agent'=>'string',
           'acces'=>'string|max:150',
           'id_prestataire'=>'integer',
           'avatar'=> 'nullable|file|image',
           'fonction'=> Rule::in(eRolePrestataire::getAll(eRolePrestataire::class)),
        ]);

        if ($validator->fails()){
           $failedRules = $validator->failed();
           if (isset($failedRules['email']['Unique'])) {
               return $this->resp->not_acceptable(eResponseCode::EMAIL_TAKEN);
           }

           return $this->resp->bad_request($validator);
        }

        $data = $request->all();

        if ($request->hasFile('avatar')) {
            $avatar = $request->file('avatar');
            $upload = $this->uploadOne($avatar, config('cdn.avatars.path'));
            $data['avatar'] = $upload['file_name'];
        }
        $data['password'] = Hash::make($data['password']);
        $data['id_prestataire']= $request->user()->id;

        $user = Constateur::create($data);

        $reset_token=$user->generateToken($user);

        $user->notify(new ActivationCompte($reset_token));

        //dd($token);
        return $this->resp->ok(eResponseCode::C_CREATE_200_02, $user);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Constateur $constateur
     * @return \DevcorpIt\ResponseCode\JsonResp
     */
    public function update(Request $request, Constateur $constateur)
    {
        $validator = Validator::make($request->all(), [
            'nom'=> 'required|string|max:150',
            'prenom'=> 'required|string|max:150',
            'email'=> ['email', Rule::unique('constateurs')->ignore($constateur)],
            'password'=>'string|max:20',
            'telephone'=> 'required|string|max:20',
            'identifiant' => 'string|max:20',
            'adresse'=>'string|max:255',
            'num_agent'=>'string',
            'acces'=>'string|max:150',
            'id_prestataire'=>'integer',
            'avatar'=> 'file|image',
            'fonction'=> Rule::in(eRolePrestataire::getAll(eRolePrestataire::class)),
        ]);
        if ($validator->fails()){
            $failedRules = $validator->failed();
            if (isset($failedRules['email']['Unique'])) {
                return $this->resp->not_acceptable(eResponseCode::EMAIL_TAKEN);
            }
            return $this->resp->bad_request($validator);
        }
        $data = $request->all();
        if ($request->hasFile('avatar')) {
            $avatar = $request->file('avatar');
            $upload = $this->uploadOne($avatar, config('cdn.avatars.path'));
            $data['avatar'] = $upload['file_name'];
        }
        if(!isset($data['password']) || empty($data['password'])) unset($data['password']);
        else $data['password'] = Hash::make($data['password']);
        $constateur->update($data);

        return $this->resp->ok(eResponseCode::C_UPDATE_200_03, $constateur);
    }


    /**
     * Delete a specified resource in storage.
     *
     * @param Constateur $constateur
     * @return \DevcorpIt\ResponseCode\JsonResp
     */
    public function delete(Constateur $constateur)
    {
        if(empty($constateur)) {
            return $this->resp->not_found(eResponseCode::C_404_NOT_FOUND);
        }
        $constateur->delete();
        return $this->resp->ok(eResponseCode::C_DELETE_200_04);
    }





    /**
     * Update constateur profile.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \DevcorpIt\ResponseCode\JsonResp
     */
    public function updateUserProfil(Request $request)
    {
        $user = $request->user();

        $validator = Validator::make($request->all(), [
            'email'=> ['email', Rule::unique('constateurs')->ignore($user)],
            'password'=> 'sometimes|nullable|string|min:6',
            'nom'=> 'string|max:150',
            'prenom'=> 'string|max:150',
            'telephone'=> 'string|max:20',
            'avatar' => 'image|mimes:jpeg,png,jpg,gif'
        ]);

        if ($validator->fails()){
            $failedRules = $validator->failed();
            if (isset($failedRules['email']['Unique'])) {
                return $this->resp->not_acceptable(eResponseCode::EMAIL_TAKEN);
            }

            return $this->resp->bad_request($validator);
        }

        $data = $request->all();

      /*  if ($request->hasFile('avatar')) {
            // Upload new file
            $image_info = $this->uploadOne($request->avatar, config('cdn.users.path'));

            // Refactoring request with a proper name for image column
            $data = array_merge($request->except(['avatar']), ['avatar' => $image_info['file_name']]);
        }*/

        if(empty($data['password'])){
            unset($data['password']);
            $user->update($data);

        } else {
            $user->update($data);
        }

        return $this->resp->ok(eResponseCode::U_UPDATE_200_03, $user);
    }













}
