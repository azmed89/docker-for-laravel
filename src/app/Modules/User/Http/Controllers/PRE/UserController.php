<?php



namespace App\Modules\User\Http\Controllers\PRE;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Models\UserPrestataire;
use App\Models\Constateur;
use App\Enums\eResponseCode;
use App\Enums\eRole;
use App\Enums\eRolePrestataire;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Notification;
use App\Notifications\ActivationCompte;


use App\Libs\Tools\UploadTrait;

class UserController extends Controller
{

    use UploadTrait;


    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return \DevcorpIt\ResponseCode\JsonResp
     */

    public function all(Request $request)
    {
        $user = UserPrestataire::where('id_prestataire',$request->user()->id_prestataire)->get();
        return $this->resp->ok(eResponseCode::U_LISTED_200_00, $user);
    }


    /**
     * Display the specified user.
     *
     * @param User $user
     * @return \DevcorpIt\ResponseCode\JsonResp
     */
    public function get(UserPrestataire $user)
    {
        if(!$user)
            return $this->resp->ok(eResponseCode::U_404_NOT_FOUND, $user);
        return $this->resp->ok(eResponseCode::U_GET_200_01, $user);
    }

    /**
     * Display the specified Constateur.
     *
     * @param Constateur $constateur
     * @return \DevcorpIt\ResponseCode\JsonResp
     */



    /**
     * Store a newly created Constateur/Gestionnaire.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \DevcorpIt\ResponseCode\JsonResp
     */
    public function create(Request $request)
    {
        //dd($request);
           $validator = Validator::make($request->all(), [
               'nom'=> 'required|string|max:150',
               'prenom'=> 'required|string|max:150',
               'email'=> ['required', 'email',Rule::unique('user_prestataires')],
               'password'=>'required|string|max:20',
               'phone'=> 'required|string|max:20',
               'identifiant' => 'string|max:20',
               'avatar'=> 'nullable|file|image',
               'fonction'=> Rule::in(eRolePrestataire::getAll(eRolePrestataire::class)),
           ]);

           if ($validator->fails()){
               $failedRules = $validator->failed();
               if (isset($failedRules['email']['Unique'])) {
                   return $this->resp->not_acceptable(eResponseCode::EMAIL_TAKEN);
               }
              //dd($this->resp->bad_request($validator));
               return $this->resp->bad_request($validator);
           }

           $data = $request->all();
           $data['password'] = Hash::make($data['password']);

           $data['id_prestataire']= $request->user()->id_prestataire ;

            if ($request->hasFile('avatar')) {
                $avatar=$request->file('avatar');
                $upload = $this->uploadOne($avatar, config('cdn.avatars.path'));
                $data['avatar'] = $upload['file_name'];
            }

           if($user = UserPrestataire::create($data))
           {
               $reset_token=$user->generateToken($user);
               $user->notify(new ActivationCompte($reset_token));
           }


           return $this->resp->ok(eResponseCode::U_CREATE_200_02, $user);


    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param User $user
     * @return \DevcorpIt\ResponseCode\JsonResp
     */
    public function update(Request $request, UserPrestataire $user)
    {
        $validator = Validator::make($request->all(), [
            'email'=> ['email', Rule::unique('user_prestataires')->ignore($user)],
            'password'=> 'sometimes|nullable|string|min:6',
            'nom'=> 'string|max:150',
            'prenom'=> 'string|max:150',
            'phone'=> 'required|string|max:20',
            'identifiant' => 'string|max:20',
            'avatar'=> 'nullable|file|image',
            'role'=> Rule::in(eRolePrestataire::getAll(eRolePrestataire::class)),
        ]);
        if ($validator->fails()){
            $failedRules = $validator->failed();
            if (isset($failedRules['email']['Unique'])) {
                return $this->resp->not_acceptable(eResponseCode::EMAIL_TAKEN);
            }
            return $this->resp->bad_request($validator);
        }
        $data = $request->all();
        if(!isset($data['password']) || empty($data['password'])) unset($data['password']);
        else $data['password'] = Hash::make($data['password']);


        $data['id_prestataire']= $request->user()->id_prestataire ;

        if ($request->hasFile('avatar')) {
            $avatar=$request->file('avatar');
            $upload = $this->uploadOne($avatar, config('cdn.avatars.path'));
            $data['avatar'] = $upload['file_name'];
        }

        $user->update($data);

        return $this->resp->ok(eResponseCode::U_UPDATE_200_03, $user);
    }


    /**
     * Delete a specified resource in storage.
     *
     * @param UserPrestataire $user
     * @return \DevcorpIt\ResponseCode\JsonResp
     */
    public function delete(UserPrestataire $user)
    {

        if(empty($user)) {
            return $this->resp->not_found(eResponseCode::U_404_NOT_FOUND);
        }
        if($user->avatar){
            $this->deleteOne($user->avatar, config('cdn.avatars.path'));
        }
        $user->delete();
        return $this->resp->ok(eResponseCode::U_DELETE_200_04);
    }

    /**
     *
     * @param Request $request
     * @return \DevcorpIt\ResponseCode\JsonResp
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */

    public function search(Request $request )
    {

        $validator = Validator::make($request->all(), [
            'search'=> 'nullable|string',
        ]);

        if ($validator->fails()){
            return $this->resp->bad_request($validator);
        }
        $keyword = $request->get('search');
         if($keyword!=''){
             $users = UserPrestataire::
             where("nom", "LIKE","%$keyword%")
                ->orWhere("prenom","LIKE","%$keyword%")
                 ->orWhere("email","LIKE","%$keyword%")
        ->get();
             return $this->resp->ok(eResponseCode::U_LISTED_200_00, $users);
         }
        $users = UserPrestataire::where('id_prestataire',$request->user()->id_prestataire)->get();

        return $this->resp->ok(eResponseCode::U_LISTED_200_00,$users );

    }





    /**
     * Update user profile.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \DevcorpIt\ResponseCode\JsonResp
     */
    public function updateUserProfil(Request $request)
    {
        $user = $request->user();

        $validator = Validator::make($request->all(), [
            'email'=> ['email', Rule::unique('users')->ignore($user)],
            'password'=> 'sometimes|nullable|string|min:6',
            'nom'=> 'string|max:150',
            'prenom'=> 'string|max:150',
            'telephone'=> 'string|max:20',
            'avatar' => 'image|mimes:jpeg,png,jpg,gif'
        ]);

        if ($validator->fails()){
            $failedRules = $validator->failed();
            if (isset($failedRules['email']['Unique'])) {
                return $this->resp->not_acceptable(eResponseCode::EMAIL_TAKEN);
            }

            return $this->resp->bad_request($validator);
        }

        $data = $request->all();

      /*  if ($request->hasFile('avatar')) {
            // Upload new file
            $image_info = $this->uploadOne($request->avatar, config('cdn.users.path'));

            // Refactoring request with a proper name for image column
            $data = array_merge($request->except(['avatar']), ['avatar' => $image_info['file_name']]);
        }*/

        if(empty($data['password'])){
            unset($data['password']);
            $user->update($data);

        } else {
            $user->update($data);
        }

        return $this->resp->ok(eResponseCode::U_UPDATE_200_03, $user);
    }













}
