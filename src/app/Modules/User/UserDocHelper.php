<?php
/**
 * Created by PhpStorm.
 * User: abderrahim akhdar
 * Date: 12/10/2020
 * Time: 17:30
 */

/*********************** Prestataire - Users **************************/

/**
 * @OA\Get(
 * path="/api/v1/pre/user/list",
 * summary="List Utlisateurs",
 * description="List of Users in BO Prestataire",
 * tags={"BO Prestataire - gestion utilisateurs"},
 * security={ {"apiAuth": {} }},
 * @OA\Response(
 *    response=200,
 *    description="Success"
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */
/**
 * @OA\Get(
 * path="/api/v1/pre/user/get/{id}",
 * summary="Details Users",
 * description="Details User in BO Prestataire",
 * tags={"BO Prestataire - gestion utilisateurs"},
 * security={ {"apiAuth": {} }},
 * @OA\Parameter(
 *    description="ID of User",
 *    in="path",
 *    name="id",
 *    required=true,
 *    example="1",
 *    @OA\Schema(
 *       type="integer",
 *       format="int64"
 *    )
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */
/**
 * @OA\Post(
 * path="/api/v1/pre/user/search",
 * summary="Search Users",
 * description="Search User in BO Prestataire",
 * tags={"BO Prestataire - gestion utilisateurs"},
 * security={ {"apiAuth": {} }},
 * @OA\RequestBody(
 *    required=false,
 *    description="Pass user credentials",
 *    @OA\JsonContent(
 *       @OA\Property(property="search", type="string", example="akhdar"),
 *    ),
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */
/**
 * @OA\Post(
 * path="/api/v1/pre/user/create",
 * summary="Create User",
 * description="Create User in BO Prestataire",
 * tags={"BO Prestataire - gestion utilisateurs"},
 * security={ {"apiAuth": {} }},
 * @OA\RequestBody(
 *    required=true,
 *    description="Pass user credentials",
 *    @OA\JsonContent(
 *       @OA\Property(property="nom", type="string", example="akhdar"),
 *       @OA\Property(property="prenom", type="string", example="aabderrahim"),
 *       @OA\Property(property="email", type="string", example="aabderrahim@gmail.com"),
 *       @OA\Property(property="password", type="string", example=" "),
 *       @OA\Property(property="telephone", type="string", example="06-********"),
 *       @OA\Property(property="fonction", type="string", example="agent"),
 *    ),
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */

/**
 * @OA\Post(
 * path="/api/v1/pre/user/update/{id}",
 * summary="Update User",
 * description="Update User in BO Prestataire",
 * tags={"BO Prestataire - gestion utilisateurs"},
 * security={ {"apiAuth": {} }},
 * @OA\Parameter(
 *    description="ID of User",
 *    in="path",
 *    name="id",
 *    required=true,
 *    example="1",
 *    @OA\Schema(
 *       type="integer",
 *       format="int64"
 *    )
 * ),
 * @OA\RequestBody(
 *    required=true,
 *    description="Pass user credentials",
 *    @OA\JsonContent(
 *       @OA\Property(property="nom", type="string", example="akhdar"),
 *       @OA\Property(property="prenom", type="string", example="aabderrahim"),
 *       @OA\Property(property="email", type="string", example="aabderrahim@gmail.com"),
 *       @OA\Property(property="password", type="string", example=" "),
 *       @OA\Property(property="telephone", type="string", example="06-********"),
 *       @OA\Property(property="fonction", type="string", example="agent"),
 *    ),
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */

/**
 * @OA\Post(
 * path="/api/v1/pre/user/delete/{id}",
 * summary="Update User",
 * description="Update User in BO Prestataire",
 * tags={"BO Prestataire - gestion utilisateurs"},
 * security={ {"apiAuth": {} }},
 * @OA\Parameter(
 *    description="ID of User",
 *    in="path",
 *    name="id",
 *    required=true,
 *    example="1",
 *    @OA\Schema(
 *       type="integer",
 *       format="int64"
 *    )
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */

/**
 * @OA\Post(
 * path="/api/v1/pre/user/profil/update",
 * summary="Update User Profile",
 * description="Update User Profile in BO Prestataire",
 * tags={"BO Prestataire - gestion utilisateurs"},
 * security={ {"apiAuth": {} }},
 * @OA\RequestBody(
 *    required=true,
 *    description="Pass user credentials",
 *    @OA\JsonContent(
 *       @OA\Property(property="nom", type="string", example="akhdar"),
 *       @OA\Property(property="prenom", type="string", example="aabderrahim"),
 *       @OA\Property(property="email", type="string", example="aabderrahim@gmail.com"),
 *       @OA\Property(property="password", type="string", example=" "),
 *       @OA\Property(property="telephone", type="string", example="06-********"),
 *       @OA\Property(property="avatar", type="string", example=""),
 *    ),
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */

/*********************** Prestataire - Agents **************************/

/**
 * @OA\Get(
 * path="/api/v1/pre/user/listconst",
 * summary="List Agents Constateur ",
 * description="List of  Agents in BO Prestataire",
 * tags={"BO Prestataire - gestion utilisateurs(Constateur)"},
 * security={ {"apiAuth": {} }},
 * @OA\Response(
 *    response=200,
 *    description="Success"
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */
/**
 * @OA\Get(
 * path="/api/v1/pre/user/getconst/{id}",
 * summary="Details Constateurs Agents",
 * description="Details Agents in BO Prestataire",
 * tags={"BO Prestataire - gestion utilisateurs(Constateur)"},
 * security={ {"apiAuth": {} }},
 * @OA\Parameter(
 *    description="ID of Agent",
 *    in="path",
 *    name="id",
 *    required=true,
 *    example="1",
 *    @OA\Schema(
 *       type="integer",
 *       format="int64"
 *    )
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */
/**
 * @OA\Post(
 * path="/api/v1/pre/user/createconst",
 * summary="Create User",
 * description="Create Agent in BO Prestataire",
 * tags={"BO Prestataire - gestion utilisateurs(Constateur)"},
 * security={ {"apiAuth": {} }},
 * @OA\RequestBody(
 *    required=true,
 *    description="Pass user credentials",
 *    @OA\JsonContent(
 *       @OA\Property(property="nom", type="string", example="akhdar"),
 *       @OA\Property(property="prenom", type="string", example="aabderrahim"),
 *       @OA\Property(property="email", type="string", example="aabderrahim@gmail.com"),
 *       @OA\Property(property="password", type="string", example=" "),
 *       @OA\Property(property="telephone", type="string", example="06-********"),
 *       @OA\Property(property="fonction", type="string", example="agent"),
 *    ),
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */
/**
 * @OA\Post(
 * path="/api/v1/pre/user/updateconst/{id}",
 * summary="Update Agent",
 * description="Update Agent in BO Prestataire",
 * tags={"BO Prestataire - gestion utilisateurs(Constateur)"},
 * security={ {"apiAuth": {} }},
 * @OA\Parameter(
 *    description="ID of User",
 *    in="path",
 *    name="id",
 *    required=true,
 *    example="1",
 *    @OA\Schema(
 *       type="integer",
 *       format="int64"
 *    )
 * ),
 * @OA\RequestBody(
 *    required=true,
 *    description="Pass user credentials",
 *    @OA\JsonContent(
 *       @OA\Property(property="nom", type="string", example="akhdar"),
 *       @OA\Property(property="prenom", type="string", example="aabderrahim"),
 *       @OA\Property(property="email", type="string", example="aabderrahim@gmail.com"),
 *       @OA\Property(property="password", type="string", example=" "),
 *       @OA\Property(property="telephone", type="string", example="06-********"),
 *       @OA\Property(property="fonction", type="string", example="agent"),
 *    ),
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */
/**
 * @OA\Post(
 * path="/api/v1/pre/user/deleteconst/{id}",
 * summary="Update User",
 * description="Delete Agent in BO Prestataire",
 * tags={"BO Prestataire - gestion utilisateurs(Constateur)"},
 * security={ {"apiAuth": {} }},
 * @OA\Parameter(
 *    description="ID of User",
 *    in="path",
 *    name="id",
 *    required=true,
 *    example="1",
 *    @OA\Schema(
 *       type="integer",
 *       format="int64"
 *    )
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */
/**
 * @OA\Post(
 * path="/api/v1/pre/user/profilconst/update",
 * summary="Update Agent Profile",
 * description="Update Agent Profile in BO Prestataire",
 * tags={"BO Prestataire - gestion utilisateurs(Constateur)"},
 * security={ {"apiAuth": {} }},
 * @OA\RequestBody(
 *    required=true,
 *    description="Pass user credentials",
 *    @OA\JsonContent(
 *       @OA\Property(property="nom", type="string", example="akhdar"),
 *       @OA\Property(property="prenom", type="string", example="aabderrahim"),
 *       @OA\Property(property="email", type="string", example="aabderrahim@gmail.com"),
 *       @OA\Property(property="password", type="string", example=" "),
 *       @OA\Property(property="telephone", type="string", example="06-********"),
 *       @OA\Property(property="avatar", type="string", example=""),
 *    ),
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */



/*********************** Assurance - Users **************************/

/**
 * @OA\Get(
 * path="/api/v1/assu/user/list",
 * summary="List Users ",
 * description="List of  Users in BO Campagnie Assurance",
 * tags={"BO Campagnie Assurance - gestion utilisateurs"},
 * security={ {"apiAuth": {} }},
 * @OA\Response(
 *    response=200,
 *    description="Success"
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */

/**
 * @OA\Get(
 * path="/api/v1/assu/user/get/{id}",
 * summary="Details User",
 * description="Details User in BO Prestataire",
 * tags={"BO Campagnie Assurance - gestion utilisateurs"},
 * security={ {"apiAuth": {} }},
 * @OA\Parameter(
 *    description="ID of User",
 *    in="path",
 *    name="id",
 *    required=true,
 *    example="1",
 *    @OA\Schema(
 *       type="integer",
 *       format="int64"
 *    )
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */

/**
 * @OA\Post(
 * path="/api/v1/assu/user/create",
 * summary="Create User",
 * description="Create User in BO Campagnie Assurance",
 * tags={"BO Campagnie Assurance - gestion utilisateurs"},
 * security={ {"apiAuth": {} }},
 * @OA\RequestBody(
 *    required=true,
 *    description="Pass user credentials",
 *    @OA\JsonContent(
 *       @OA\Property(property="nom", type="string", example="akhdar"),
 *       @OA\Property(property="prenom", type="string", example="aabderrahim"),
 *       @OA\Property(property="email", type="string", example="aabderrahim@gmail.com"),
 *       @OA\Property(property="password", type="string", example=" "),
 *       @OA\Property(property="telephone", type="string", example="06-********"),
 *       @OA\Property(property="role", type="string", example="GESTINNAIRE_CA"),
 *    ),
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */

/**
 * @OA\Post(
 * path="/api/v1/assu/user/update/{id}",
 * summary="Update User",
 * description="Update User in BO Campagnie Assurance",
 * tags={"BO Campagnie Assurance - gestion utilisateurs"},
 * security={ {"apiAuth": {} }},
 * @OA\Parameter(
 *    description="ID of User",
 *    in="path",
 *    name="id",
 *    required=true,
 *    example="1",
 *    @OA\Schema(
 *       type="integer",
 *       format="int64"
 *    )
 * ),
 * @OA\RequestBody(
 *    required=true,
 *    description="Pass user credentials",
 *    @OA\JsonContent(
 *       @OA\Property(property="nom", type="string", example="akhdar"),
 *       @OA\Property(property="prenom", type="string", example="aabderrahim"),
 *       @OA\Property(property="email", type="string", example="aabderrahim@gmail.com"),
 *       @OA\Property(property="password", type="string", example=" "),
 *       @OA\Property(property="telephone", type="string", example="06-********"),
 *       @OA\Property(property="role", type="string", example="GESTINNAIRE_CA"),
 *    ),
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */

/**
 * @OA\Post(
 * path="/api/v1/assu/user/delete/{id}",
 * summary="Delete User",
 * description="Delete User in BO Campagnie Assurance",
 * tags={"BO Campagnie Assurance - gestion utilisateurs"},
 * security={ {"apiAuth": {} }},
 * @OA\Parameter(
 *    description="ID of User",
 *    in="path",
 *    name="id",
 *    required=true,
 *    example="1",
 *    @OA\Schema(
 *       type="integer",
 *       format="int64"
 *    )
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */

/**
 * @OA\Post(
 * path="/api/v1/assu/user/profil/update",
 * summary="Update User Profile",
 * description="Update User Profile in BO Campagnie Assurance",
 * tags={"BO Campagnie Assurance - gestion utilisateurs"},
 * security={ {"apiAuth": {} }},
 * @OA\RequestBody(
 *    required=true,
 *    description="Pass user credentials",
 *    @OA\JsonContent(
 *       @OA\Property(property="nom", type="string", example="akhdar"),
 *       @OA\Property(property="prenom", type="string", example="aabderrahim"),
 *       @OA\Property(property="email", type="string", example="aabderrahim@gmail.com"),
 *       @OA\Property(property="password", type="string", example=" "),
 *       @OA\Property(property="telephone", type="string", example="06-********"),
 *       @OA\Property(property="avatar", type="string", example=""),
 *    ),
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */


/************************ Constateur **********************************/

/**
 * @OA\Post(
 * path="/api/v1/cons/user/profile/update",
 * summary="Update My Profile",
 * description="Update Constateur Profile",
 * tags={"Auth App Constateur"},
 * security={ {"apiAuth": {} }},
 * @OA\RequestBody(
 *    required=true,
 *    description="Pass user credentials",
 *    @OA\JsonContent(
 *       @OA\Property(property="nom", type="string", example="Ayoub"),
 *       @OA\Property(property="prenom", type="string", example="Elhassani"),
 *       @OA\Property(property="email", type="string", example="ayoub.elhassani@it.devcorp.fr"),
 *       @OA\Property(property="password", type="string", example="123456"),
 *       @OA\Property(property="telephone", type="string", example="06-23659874"),
 *       @OA\Property(property="avatar", type="string", format="base64", example="data:image/jpeg;base64"),
 *       @OA\Property(property="adresse", type="string", example="Adresse"),
 *       @OA\Property(property="fonction", type="string", example="Fonction"),
 *       @OA\Property(property="num_agent", type="string", example="Numéro d'agent"),
 *       @OA\Property(property="acces", type="string", example="acces"),
 *       @OA\Property(property="identifiant", type="string", example="5243"),
 *    ),
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */

/*********************** Assistance - Users **************************/

/**
 * @OA\Get(
 * path="/api/v1/ass/user/list",
 * summary="List Utlisateurs",
 * description="List of Users in BO Assistance",
 * tags={"BO Assistance - gestion utilisateurs"},
 * security={ {"apiAuth": {} }},
 * @OA\Response(
 *    response=200,
 *    description="Success"
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */
/**
 * @OA\Get(
 * path="/api/v1/ass/user/get/{id}",
 * summary="Details Users",
 * description="Details User in BO Assistance",
 * tags={"BO Assistance - gestion utilisateurs"},
 * security={ {"apiAuth": {} }},
 * @OA\Parameter(
 *    description="ID of User",
 *    in="path",
 *    name="id",
 *    required=true,
 *    example="1",
 *    @OA\Schema(
 *       type="integer",
 *       format="int64"
 *    )
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */
/**
 * @OA\Post(
 * path="/api/v1/ass/user/create",
 * summary="Create User",
 * description="Create User in BO Assistance",
 * tags={"BO Assistance - gestion utilisateurs"},
 * security={ {"apiAuth": {} }},
 * @OA\RequestBody(
 *    required=true,
 *    description="Pass user credentials",
 *    @OA\JsonContent(
 *       @OA\Property(property="nom", type="string", example="akhdar"),
 *       @OA\Property(property="prenom", type="string", example="aabderrahim"),
 *       @OA\Property(property="email", type="string", example="aabderrahim@gmail.com"),
 *       @OA\Property(property="password", type="string", example=" "),
 *       @OA\Property(property="telephone", type="string", example="06-********"),
 *       @OA\Property(property="role", type="string", example="EXPERT"),
 *    ),
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */

/**
 * @OA\Post(
 * path="/api/v1/ass/user/update/{id}",
 * summary="Update User",
 * description="Update User in BO Assistance",
 * tags={"BO Assistance - gestion utilisateurs"},
 * security={ {"apiAuth": {} }},
 * @OA\Parameter(
 *    description="ID of User",
 *    in="path",
 *    name="id",
 *    required=true,
 *    example="1",
 *    @OA\Schema(
 *       type="integer",
 *       format="int64"
 *    )
 * ),
 * @OA\RequestBody(
 *    required=true,
 *    description="Pass user credentials",
 *    @OA\JsonContent(
 *       @OA\Property(property="nom", type="string", example="akhdar"),
 *       @OA\Property(property="prenom", type="string", example="aabderrahim"),
 *       @OA\Property(property="email", type="string", example="aabderrahim@gmail.com"),
 *       @OA\Property(property="password", type="string", example=" "),
 *       @OA\Property(property="telephone", type="string", example="06-********"),
 *       @OA\Property(property="role", type="string", example="EXPERT"),
 *    ),
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */

/**
 * @OA\Post(
 * path="/api/v1/ass/user/delete/{id}",
 * summary="Update User",
 * description="Update User in BO Assistance",
 * tags={"BO Assistance - gestion utilisateurs"},
 * security={ {"apiAuth": {} }},
 * @OA\Parameter(
 *    description="ID of User",
 *    in="path",
 *    name="id",
 *    required=true,
 *    example="1",
 *    @OA\Schema(
 *       type="integer",
 *       format="int64"
 *    )
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */

/**
 * @OA\Post(
 * path="/api/v1/ass/user/profil/update",
 * summary="Update User Profile",
 * description="Update User Profile in BO Assistance",
 * tags={"BO Assistance - gestion utilisateurs"},
 * security={ {"apiAuth": {} }},
 * @OA\RequestBody(
 *    required=true,
 *    description="Pass user credentials",
 *    @OA\JsonContent(
 *       @OA\Property(property="nom", type="string", example="akhdar"),
 *       @OA\Property(property="prenom", type="string", example="aabderrahim"),
 *       @OA\Property(property="email", type="string", example="aabderrahim@gmail.com"),
 *       @OA\Property(property="password", type="string", example=" "),
 *       @OA\Property(property="telephone", type="string", example="06-********"),
 *       @OA\Property(property="avatar", type="string", example=""),
 *    ),
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */
/**
 * @OA\Post(
 * path="/api/v1/ass/user/search",
 * summary="Search Users",
 * description="Search User in BO Assistance",
 * tags={"BO Assistance - gestion utilisateurs"},
 * security={ {"apiAuth": {} }},
 * @OA\RequestBody(
 *    required=false,
 *    description="Pass user credentials",
 *    @OA\JsonContent(
 *       @OA\Property(property="search", type="string", example="akhdar"),
 *    ),
 * ),
 * @OA\Response(
 *    response=401,
 *    description="Not authenticated",
 *    @OA\JsonContent(
 *       @OA\Property(property="message", type="string", example="Not authorized"),
 *    )
 * )
 * )
 */
