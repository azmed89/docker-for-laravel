<?php

Route::group(['prefix' => '/api/v1/pre/user', 'module' => 'User', 'namespace' => 'PRE'], function() {

    Route::middleware(['auth.jwt','auth.user_prestataire','api'])->group(function () {
        Route::get('/list', 'UserController@all');
        Route::get('/get/{user}', 'UserController@get');
        Route::post('/create', 'UserController@create');
        Route::post('/update/{user}', 'UserController@update');
        Route::post('/delete/{user}', 'UserController@delete');
        Route::post('/search', 'UserController@search');
        Route::post('/profil/update', 'UserController@updateUserProfil');

      /*---------------- constateur Users -------------*/

        Route::get('/listconst', 'UserConstateurController@all');
        Route::get('/getconst/{constateur}', 'UserConstateurController@get');
        Route::post('/createconst', 'UserConstateurController@create');
        Route::post('/updateconst/{constateur}', 'UserConstateurController@update');
        Route::post('/deleteconst/{constateur}', 'UserConstateurController@delete');
        Route::post('/profilconst/update', 'UserConstateurController@updateUserProfil');
    });
});

Route::group(['prefix' => '/api/v1/ass/user', 'module' => 'User', 'namespace' => 'ASS'], function() {


    Route::middleware(['auth.jwt', 'auth.user_assistance', 'api'])->group(function () {
        Route::get('/list', 'UserController@all');
        Route::get('/get/{user}', 'UserController@get');
        Route::post('/create', 'UserController@create');
        Route::post('/update/{user}', 'UserController@update');
        Route::post('/delete/{user}', 'UserController@delete');
        Route::post('/search', 'UserController@search');
        Route::post('/profil/update', 'UserController@updateUserProfil');
    });
});


Route::group(['prefix' => '/api/v1/assu/user', 'module' => 'User', 'namespace' => 'ASSU'], function () {


    Route::middleware(['auth.jwt', 'auth.user_assurance', 'api'])->group(function () {
        Route::get('/list', 'UserController@all');
        Route::get('/get/{user}', 'UserController@get');
        Route::post('/create', 'UserController@create');
        Route::post('/search', 'UserController@search');
        Route::post('/update/{user}', 'UserController@update');
        Route::post('/delete/{user}', 'UserController@delete');
        Route::post('/profil/update', 'UserController@updateUserProfil');
    });
});


Route::group(['prefix' => '/api/v1/cons/user', 'module' => 'User', 'namespace' => 'CONS'], function() {

    Route::middleware(['auth.jwt','auth.constateur','api'])->group(function () {
        Route::post('/profile/update', 'UserController@updateUserProfil');
    });
});
