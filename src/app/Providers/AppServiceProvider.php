<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Config;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Change Model used in login
        if (request()->is('api/v1/ass/*')) {
            Config::set('auth.providers.users.model', \App\Models\UserAssistance::class);
        } else if (request()->is('api/v1/cons/*')) {
            Config::set('auth.providers.users.model', \App\Models\Constateur::class);
        }else if(request()->is ('api/v1/assu/*')){
            Config::set('auth.providers.users.model', \App\Models\UserAssurance::class);
        }else if(request()->is ('api/v1/pre/*')){
            Config::set('auth.providers.users.model', \App\Models\UserPrestataire::class);
        }
    }
}
