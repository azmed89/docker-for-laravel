<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class StringCsv implements Rule
{
    public $limit;
    public $message = 'The :attribute must be a list of words separated by comma.';

    /**
     * Create a new rule instance.
     *
     * @param null $limit
     */
    public function __construct($limit = null)
    {
        $this->limit = $limit;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if($this->limit == null){
            return preg_match('!^\w+(,\w+)*$!',$value);
        } else {
            $data = explode(',', $value);
            if(count($data) <= $this->limit) {
                return preg_match('!^\w+(,\w+)*$!', $value);
            }
            else {
                $this->message = 'The :attribute max is '.$this->limit.' words.';
            }
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->message;
    }
}
