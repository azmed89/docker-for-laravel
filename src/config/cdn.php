<?php
/**
 * Created by PhpStorm.
 * User: AZMed
 * Date: 14/10/2020
 * Time: 17:32
 */

return [
    'constateurs' => [
        'path' =>  '/cdn/constateurs',
        'url' => env('APP_URL') . '/cdn/constateurs/'
    ],
    'degats' => [
        'path' =>  '/cdn/constateurs/degats',
        'url' => env('APP_URL') . '/cdn/constateurs/degats/'
    ],
    'croquis' => [
        'path' =>  '/cdn/constateurs/croquis',
        'url' => env('APP_URL') . '/cdn/constateurs/croquis/'
    ],
    'avatars' => [
        'path' =>  '/cdn/avatars',
        'url' => env('APP_URL') . '/cdn/avatars/'
    ],
    'signatures'=>[
        'path' => '/cdn/signatures',
        'url' => env('APP_URL') . '/cdn/signatures/'
    ],
    'signatures'=>[
        'path' => '/cdn/constateurs/signatures',
        'url' => env('APP_URL') . '/cdn/constateurs/signatures/'
    ],

    'recap' => [
        'path' =>  '/cdn/recap',
        'url' => env('APP_URL') . '/cdn/recap/'
    ],

    'recap' => [
        'path' =>  '/cdn/recap',
        'url' => env('APP_URL') . '/cdn/recap/'
    ],
];
