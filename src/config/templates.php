<?php


   return [
       'pattern' => '/@{(.*?)}/',

       'place_holders' => [
           'assure' => ['nom', 'prenom'],
           'assure' => ['get_id_constat'=>'getIdConstat','nom_complet'=>'nomComplet'],
           'app' => ['signature'],
           'constateur' =>['get_id_constateur'=>'getIdConstateur','nom_complet'=>'nomComplet'],
           'prestataire' =>['name'],
           'prestataire' =>['get_id_prestataire'=>'getIdPrestataire','get_name'=>'getName'],
       ],

       'alias' => [
           'assure' => \App\Models\Assure::class,
           'app' => \App\Modules\Template\Models\Entity::class,
           'constateur' => \App\Models\Constateur::class,
           'prestataire' => \App\Models\Prestataire::class,
       ],
       'entity' => [
           'signature' => 'E-constat',
       ]

];
