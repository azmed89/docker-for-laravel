<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(\App\Models\Assistance::class, function (Faker $faker) {
    return [
        'nom' => $faker->company,
        'email' => $faker->companyEmail,
        'telephone' => $faker->e164PhoneNumber
    ];
});
