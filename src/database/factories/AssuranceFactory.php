<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(\App\Models\Assurance::class, function (Faker $faker) {
    $assistances = \App\Models\Assistance::all()->pluck('id_assistance')->toArray();

    return [
        'nom' => $faker->company,
        'code_societe' => $faker->company,
        'email' => $faker->companyEmail,
        'id_assistance' => function (array $assurance) use ($assistances) {
            $id = array_rand($assistances, 1);
            return \App\Models\Assistance::find($assistances[$id])->id_assistance;
        }
    ];
});
