<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(\App\Models\Assure::class, function (Faker $faker) {
    $assurances = \App\Models\Assurance::all()->pluck('id_assurance')->toArray();
    $constats = \App\Models\Constat::all()->pluck('id_constat')->toArray();
    $types = \App\Enums\eTypeUsage::getAll(\App\Enums\eTypeUsage::class);
    return [
        'nom' => $faker->name,
        'prenom' => $faker->name,
        'adresse' => $faker->address,
        'n_attestation' => $faker->randomNumber(5, false),
        'n_police' => $faker->randomNumber(5, false),
        'email' => $faker->safeEmail,
        'validite_attestation' => $faker->boolean,
        'agence' => $faker->company,
        'attes_valide_du' => $faker->date('Y-m-d'),
        'attes_valide_au' => $faker->date('Y-m-d'),
        'id_constat' => 11,
        //'ste_assurance' => factory(\App\Models\Assurance::class),
        'ste_assurance' => function (array $assure) use ($assurances) {
            $id = array_rand($assurances, 1);
            return \App\Models\Assurance::find($assurances[$id])->code_societe;
        },
        /*'id_constat' => function (array $assure) use ($constats) {
            $id = array_rand($constats, 1);
            return \App\Models\Constat::find($constats[$id])->id_constat;
        },*/
        'type_usage' => function (array $assure) use ($types) {
            $type = array_rand($types, 1);
            return $type;
        }
    ];
});
