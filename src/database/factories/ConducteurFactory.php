<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(\App\Models\Conducteur::class, function (Faker $faker) {
    $constats = \App\Models\Constat::all()->pluck('id_constat')->toArray();
    $cities = ['Marrakech', 'Agadir', 'Tetouan', 'Meknes', 'Wajda', 'Elhoceima', 'Rabat', 'Casablanca', 'Fes', 'Mohammedia'];
    $types = \App\Enums\eTypePermis::getAll(\App\Enums\eTypePermis::class);
    return [
        'nom' => $faker->name,
        'prenom' => $faker->name,
        'adresse' => $faker->address,
        'num_permis' => $faker->randomNumber(5, false),
        'signature' => '{"filepath":"sXlHVVnHWtCllh3GNoNLPfRxS.jpg","name":"sign_b.png"}',
        'prefecture_delivrance' => function (array $conducteur) use ($cities) {
            $index = array_rand($cities, 1);
            return $cities[$index];
        },
        'date_delivrance' => $faker->date('Y-m-d'),
        'delai_validite' => $faker->date('Y-m-d'),
        'id_constat' => 11,
        'type_permis' => function (array $conducteur) use ($types) {
            $type = array_rand($types, 1);
            return $type;
        }
    ];
});
