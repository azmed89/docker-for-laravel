<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(\App\Models\UserAssurance::class, function (Faker $faker) {
    $roles = \App\Enums\eRoleAssurance::getAll(\App\Enums\eRoleAssurance::class);
    return [
        'nom' => $faker->name,
        'prenom' => $faker->name,
        'telephone' => $faker->e164PhoneNumber,
        'email' => $faker->unique()->safeEmail,
        'role' => function (array $user) use ($roles) {
            $role = array_rand($roles, 1);
            return $role;
        },
        'id_assurance' => 1,
        'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
        'avatar' => 'Cv9A3XZdxpJWNAcbp1rzUQcPK.jpg',
    ];
});
