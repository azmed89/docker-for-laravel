<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(\App\Models\Vehicule::class, function (Faker $faker) {
    $constats = \App\Models\Constat::all()->pluck('id_constat')->toArray();
    $marques = ['Toyota' => 'Yaris', 'Clio' => 'Campus', 'Fiat' => 'Punto', 'Dacia' => 'Logan', 'Peugeot' => '3008', 'Mercedes' => 'classe a', 'Ford' => 'Ecosport', 'Alfa Romeo' => 'Stelvio', 'Mini' => 'Countryman', 'Audi' => 'Q5'];
    $types = \App\Enums\eTypeVehicle::getAll(\App\Enums\eTypeVehicle::class);
    $index = array_rand($marques, 1);
    $m = $marques[$index];
    return [
        'num_immatriculation' => $faker->randomNumber(6, false),
        'marque' => function (array $vehicle) use ($index) {
            return $index;
        },
        'modele' => function (array $vehicle) use ($m) {
            return $m;
        },
        'venant_de' => $faker->streetName,
        'allant_vers' => $faker->streetName,
        'id_constat' => 11,
        'type' => function (array $conducteur) use ($types) {
            $type = array_rand($types, 1);
            return $type;
        }
    ];
});
