<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTemplatesBodyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('templates_body', function (Blueprint $table) {
            $table->bigIncrements('id_tbody');
            $table->string('subject');
            $table->text('body');
            $table->text('lang');
            $table->unsignedBigInteger('id_template');

            $table->foreign('id_template')->references('id_template')->on('templates');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('templates_body');
    }
}
