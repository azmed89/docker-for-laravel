<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Enums\eTypePrestataire;

class CreatePrestatairesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prestataires', function (Blueprint $table) {
            $table->bigIncrements('id_prestataire');
            $table->string('code_groupe',150)->nullable();
            $table->string('name',150)->nullable();
            $table->string('adresse',150)->nullable();
            $table->string('ville',150)->nullable();
            $table->string('telephone',150)-> nullable();
            $table->string('email',150)->unique();
            $table->enum('type_prestataire', eTypePrestataire::getAll(eTypePrestataire::class))->default(eTypePrestataire::CONSTATEURS);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prestataires');
    }
}
