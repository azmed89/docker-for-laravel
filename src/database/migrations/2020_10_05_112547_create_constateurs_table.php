<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConstateursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('constateurs', function (Blueprint $table) {
            $table->bigIncrements('id_constateur');
            $table->string('email',50)->unique();
            $table->string('password',150);
            $table->string('nom',150)->nullable();
            $table->string('prenom',150)->nullable();
            $table->string('adresse',150)->nullable();
            $table->string('fonction',150)->nullable();
            $table->string('telephone',150)-> nullable();
            $table->string('num_agent',150);
            $table->string('acces',150);
            $table->string('avatar',500);
            $table->string('identifiant',50)->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('constateurs');
    }
}
