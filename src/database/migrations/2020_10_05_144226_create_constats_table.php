<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Enums\eConstatStatut;

class CreateConstatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('constats', function (Blueprint $table) {
            $table->bigIncrements('id_constat');
            $table->string('type_mission', 150)->nullable();
            $table->string('code', 30)->nullable();
            $table->string('num_immatriculation', 30)->nullable();
            $table->enum('etat', eConstatStatut::getAll(eConstatStatut::class))->default(eConstatStatut::CREATED);
            $table->integer('num_contrat')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('constats');
    }
}
