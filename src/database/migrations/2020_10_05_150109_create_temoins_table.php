<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTemoinsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temoins', function (Blueprint $table) {
            $table->bigIncrements('id_temoin');
            $table->string('nom',150)->nullable();
            $table->string('prenom',150)->nullable();
            $table->string('email',150)->nullable();
            $table->string('adresse',150)->nullable();
            $table->string('ville',150)->nullable();
            $table->string('telephone',150)-> nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temoins');
    }
}
