<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssuresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assures', function (Blueprint $table) {
            $table->bigIncrements('id_assure');
            $table->string('nom',150)->nullable();
            $table->string('prenom',150)->nullable();
            $table->string('adresse',150)->nullable();
            $table->string('n_attestation',150)->nullable();
            $table->string('n_police',150)->nullable();
            $table->string('ste_assurance',150)-> nullable();
            $table->boolean('validite_attestation');
            $table->string('agence',150)-> nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assures');
    }
}
