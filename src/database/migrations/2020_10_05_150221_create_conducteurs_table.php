<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConducteursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('conducteurs', function (Blueprint $table) {
            $table->bigIncrements('id_conducteur');
            $table->string('nom',150)->nullable();
            $table->string('prenom',150)->nullable();
            $table->string('adresse',150)->nullable();
            $table->string('num_permis',150)->nullable();
            $table->string('date_delivrance',150)->nullable();
            $table->string('prefecture_delivrance',150)-> nullable();
            $table->string('signature', 150)->nullable();
            $table->integer('delai_validite')-> nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('conducteurs');
    }
}
