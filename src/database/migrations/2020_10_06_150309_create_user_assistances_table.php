<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Enums\eRoleAssistance;

class CreateUserAssistancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_assistances', function (Blueprint $table) {
            $table->bigIncrements('id_user_ass');
            $table->string('nom', 150)->nullable();
            $table->string('prenom', 150)->nullable();
            $table->string('telephone', 50)->nullable();
            $table->string('email', 150)->unique()->nullable();
            $table->string('password');
            $table->enum('role', eRoleAssistance::getAll(eRoleAssistance::class))->default(eRoleAssistance::GESTIONNAIRE_CA);
            $table->unsignedBigInteger('id_assistance')->nullable();
            $table->foreign('id_assistance')->references('id_assistance')->on('assistances')->onDelete('set null');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_assistances');
    }
}
