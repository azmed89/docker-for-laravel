<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssistancePrestatairesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assistance_prestataires', function (Blueprint $table) {
            $table->unsignedBigInteger('id_assistance')->index();
            $table->unsignedBigInteger('id_prestataire')->index();
            $table->primary(['id_assistance', 'id_prestataire']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assistance_prestataires');
    }
}
