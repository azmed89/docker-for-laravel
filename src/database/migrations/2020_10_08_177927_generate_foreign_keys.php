<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class GenerateForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('constateurs', function (Blueprint $table) {
            $table->unsignedBigInteger('id_prestataire')->nullable();
            $table->foreign('id_prestataire')->references('id_prestataire')->on('prestataires')->onDelete('set null');
        });

        Schema::table('constats', function (Blueprint $table) {
            $table->unsignedBigInteger('liee_a')->nullable();
            $table->foreign('liee_a')->references('id_constat')->on('constats')->onDelete('set null');
            $table->unsignedBigInteger('id_workflow')->nullable();
            $table->foreign('id_workflow')->references('id_workflow')->on('workflows')->onDelete('set null');
            $table->unsignedBigInteger('id_constateur')->nullable();
            $table->foreign('id_constateur')->references('id_constateur')->on('constateurs')->onDelete('set null');
        });

        Schema::table('temoins', function (Blueprint $table) {
            $table->unsignedBigInteger('id_constat')->nullable();
            $table->foreign('id_constat')->references('id_constat')->on('constats')->onDelete('set null');
        });

        Schema::table('assures', function (Blueprint $table) {
            $table->unsignedBigInteger('id_constat')->nullable();
            $table->foreign('id_constat')->references('id_constat')->on('constats')->onDelete('set null');
        });

        Schema::table('conducteurs', function (Blueprint $table) {
            $table->unsignedBigInteger('id_constat')->nullable();
            $table->foreign('id_constat')->references('id_constat')->on('constats')->onDelete('set null');
        });

        Schema::table('vehicles', function (Blueprint $table) {
            $table->unsignedBigInteger('id_constat')->nullable();
            $table->foreign('id_constat')->references('id_constat')->on('constats')->onDelete('set null');
        });

        Schema::table('assistance_prestataires', function (Blueprint $table) {
            $table->foreign('id_assistance')->references('id_assistance')->on('assistances')->onDelete('cascade');
            $table->foreign('id_prestataire')->references('id_prestataire')->on('prestataires')->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
