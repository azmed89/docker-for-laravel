<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignKeyIdConstatToDegatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('degats', function (Blueprint $table) {
            $table->unsignedBigInteger('id_constat')->nullable();
            $table->foreign('id_constat')->references('id_constat')->on('constats')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('degats', function (Blueprint $table) {
            //
        });
    }
}
