<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Enums\eRoleAssurance;

class CreateUserAssurancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_assurances', function (Blueprint $table) {
            $table->bigIncrements('id_user_assu');
            $table->string('nom', 150)->nullable();
            $table->string('prenom', 150)->nullable();
            $table->string('telephone', 50)->nullable();
            $table->string('email', 150)->unique()->nullable();
            $table->string('password');
            $table->enum('role', eRoleAssurance::getAll(eRoleAssurance::class))->default(eRoleAssurance::GESTIONNAIRE_CA);
            $table->unsignedBigInteger('id_assurance')->nullable();
            $table->foreign('id_assurance')->references('id_assurance')->on('assurances')->onDelete('set null');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_assurances');
    }
}
