<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Enums\ePerimetre;

class AlterAddMotifPerimetreConstatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('constats', function (Blueprint $table) {
            $table->text('motif')->nullable();
            $table->enum('perimetre', ePerimetre::getAll(ePerimetre::class))->default(ePerimetre::URBAN);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('constats', function (Blueprint $table) {
            //
        });
    }
}
