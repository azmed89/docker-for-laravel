<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Enums\eRolePrestataire;

class CreateUserPrestatairesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_prestataires', function (Blueprint $table) {
            $table->id();
            $table->string('nom', 150)->nullable();
            $table->string('prenom', 150)->nullable();
            $table->string('phone', 50)->nullable();
            $table->string('email', 150)->unique();
            $table->string('identifier')->nullable();
            $table->string('password');
            $table->string('avatar')->nullable();
            $table->enum('role', eRolePrestataire::getAll(eRolePrestataire::class))->default(eRolePrestataire::GESTIONNAIRE_P);
            $table->unsignedBigInteger('id_prestataire')->nullable();
            $table->foreign('id_prestataire')->references('id_prestataire')->on('prestataires')->onDelete('set null');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_prestataires');
    }
}
