<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterRemoveFieldsInUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::table('users', function (Blueprint $table) {
            if (Schema::hasColumn('users', 'identifier')) {
                $table->dropColumn(['identifier']);
            }
            if (Schema::hasColumn('users', 'role')) {
                $table->dropColumn(['role']);
            }
            $index_exists = collect(DB::select("SHOW INDEXES FROM users"))->pluck('Key_name')->contains('users_id_prestataire_foreign');
            if ($index_exists) {
                $table->dropForeign('users_id_prestataire_foreign');
                $table->dropColumn(['id_prestataire']);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
