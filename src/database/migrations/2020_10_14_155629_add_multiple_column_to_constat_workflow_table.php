<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMultipleColumnToConstatWorkflowTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('workflows', function (Blueprint $table) {
            //
            $table->date('date_etablissement')->nullable();
            $table->time('heure_etablissement')->nullable();
            $table->date('date_accident')->nullable();
            $table->time('heure_accident')->nullable();
            $table->string('lieu')->nullable();
            $table->boolean('autres_degats')->nullable();
            $table->json('circonstances_accident')->nullable();
            $table->string('croquis')->nullable();
            $table->integer('constateur')->nullable();
            $table->time('heure_missionnement')->nullable();
            $table->time('heure_depart')->nullable();
            $table->time('heure_arrivée')->nullable();
            $table->time('heure_fin')->nullable();




        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('workflows', function (Blueprint $table) {
            $table->dropColumn(['date_etablissement',  'heure_etablissement', 'date_accident', 'heure_accident', 'lieu', 'autres_degats', 'circonstances_accident', 'croquis', 'constateur', 'heure_missionnement', 'heure_depart', 'heure_arrivée', 'heure_fin']);
        });
    }
}
