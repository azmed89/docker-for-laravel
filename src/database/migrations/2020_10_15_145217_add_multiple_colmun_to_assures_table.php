<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMultipleColmunToAssuresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('assures', function (Blueprint $table) {
            $table->date('attes_valide_du')->nullable();
            $table->date('attes_valide_au')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('assures', function (Blueprint $table) {
            $table->dropColumn('attes_valide_du');
            $table->dropColumn('attes_valide_au');
            $table->dropColumn('validite_attestation');
        });
    }
}
