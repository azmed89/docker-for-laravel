<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Enums\eTypePermis;

class AddTypePermisInConducteursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('conducteurs', function (Blueprint $table) {
            $table->enum('type_permis', eTypePermis::getAll(eTypePermis::class))->default(eTypePermis::B);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('conducteurs', function (Blueprint $table) {
           $table->dropColumn('type_permis');
        });
    }
}
