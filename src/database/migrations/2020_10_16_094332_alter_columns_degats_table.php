<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterColumnsDegatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('degats', function (Blueprint $table) {
            if (Schema::hasColumn('degats', 'point_choc')) {
                $table->string('point_choc', 250)->nullable()->change();
            }
            if (Schema::hasColumn('degats', 'zone_choc')) {
                $table->json('zone_choc')->nullable()->change();
            }
            if (Schema::hasColumn('photos', 'photos')) {
                $table->json('photos')->nullable()->change();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('degats', function (Blueprint $table) {
            //
        });
    }
}
