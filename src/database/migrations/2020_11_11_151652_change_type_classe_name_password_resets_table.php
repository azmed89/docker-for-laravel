<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Enums\eModel;

class ChangeTypeClasseNamePasswordResetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('password_resets', function (Blueprint $table) {

                $table->dropColumn('class_name');
            });

        Schema::table('password_resets', function (Blueprint $table)
        {
            $table->enum('class_name',eModel::getAll(eModel::class));
        });
        }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('password_resets', function (Blueprint $table) {
            $table->dropColumn('class_name');
        });
    }
}
