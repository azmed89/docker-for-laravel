<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToWorkflowsAndConstatsTbales extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('constats', function(Blueprint $table) {
            $table->Integer('id_constateur_rejoignant')->nullable();
            $table->String('code_assistance_rejoignante')->nullable();
        });
        Schema::table('workflows', function(Blueprint $table) {
            $table->String('quartier')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('constats', function (Blueprint $table) {
            $table->dropColumn('id_constateur_rejoignant');
            $table->dropColumn('code_assistance_rejoignante');
        });

        Schema::table('workflows', function (Blueprint $table) {
            $table->dropColumn('quartier');
        });
    }
}
