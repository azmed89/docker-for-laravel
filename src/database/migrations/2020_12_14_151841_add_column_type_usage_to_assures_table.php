<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnTypeUsageToAssuresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('assures', function (Blueprint $table) {
            $table->enum('type_usage', \App\Enums\eTypeUsage::getAll(\App\Enums\eTypeUsage::class))->default(\App\Enums\eTypeUsage::PARTICULIER);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('assures', function (Blueprint $table) {
            //
        });
    }
}
