<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableChangeEtapeWorkflowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('workflows', function (Blueprint $table) {
            //$table->enum('etape', \App\Enums\eStepConstat::getAll(\App\Enums\eStepConstat::class))->nullable()->change();
            if (Schema::hasColumn('workflows', 'etape')) {
                $status = \App\Enums\eStepConstat::getAll(\App\Enums\eStepConstat::class);
                $result = join( ', ', array_map(function( $s ){ return sprintf("'%s'", $s); }, $status) );
                DB::statement("ALTER TABLE `workflows` CHANGE COLUMN `etape` `etape` enum({$result})");
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('workflows', function (Blueprint $table) {
            //
        });
    }
}
