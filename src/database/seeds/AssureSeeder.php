<?php

use Illuminate\Database\Seeder;
use App\Models\Assure;

class AssureSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /*factory('App\Models\Assure', 2)->create()->each(function ($assure) {
            //$assure->assurance()->save(factory(\App\Models\Assurance::class)->make());
            //$assure->constat()->save(factory(\App\Models\Constat::class)->make());
        });*/
        factory('App\Models\Assure', 2)->create();
    }
}
