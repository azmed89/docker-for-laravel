<?php

use Illuminate\Database\Seeder;

use App\Models\Assure;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Database\Eloquent\Model::unguard();

        $this->call([
            AssureSeeder::class
        ]);

        \Illuminate\Database\Eloquent\Model::reguard();
    }
}
