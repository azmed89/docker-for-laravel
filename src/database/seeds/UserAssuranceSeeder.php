<?php

use Illuminate\Database\Seeder;

class UserAssuranceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory('App\Models\UserAssurance', 15)->create();
    }
}
