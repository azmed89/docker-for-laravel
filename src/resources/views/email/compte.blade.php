@extends('layouts.email')

@section('body')
    <p>
        <strong>Bienvenue cher(e) client(e),</strong>
    </p>
    <p>
        Vous recevez cet e-mail avec un lien pour definir le mot de passe de votre compte.
    </p>
    <p>
        <a href="{{ $reset_token }}">{{ $reset_token }}</a>
    </p>
@endsection