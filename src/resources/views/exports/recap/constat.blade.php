<!doctype html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" href="{{$_SERVER['DOCUMENT_ROOT'].'/css/constat.css'}}" type="text/css">
</head>
<body>
<div class="my-constat" style="border: 1px solid #cccccc; height: 100%;">
    <table>
        <tbody>
        <tr>
            <td width="100%">
                <table class="bloc-head bloc-tbl">
                    <tbody>
                    <tr class="m-5">
                        <td width="50%"><p><span class="infos-head">Constat amiable d'accident automobile<br />Exclusivement matériel.</span></p></td>
                        <td width="50%" class="text-right ar">
                            <p dir="rtl">
                                <span class="infos-head ars" style="font-size: 22px">
                                معاينة ودية لحادث اصطدام
                                </span>
                            </p>
                        </td>
                    </tr>
                    <tr class="m-5">
                        <td width="50%">
                            <p>
                                <span class="infos-head" style="font-size: 10px">
                                    A signer obligatoirement par les deux constateurs : Ne consiste pas une reconnaissance de responsabilité, mais un relevé des identités et des faits
                                </span>
                            </p>
                        </td>
                        <td width="50%" class="text-right">
                            <p dir="rtl">
                                <span class="infos-head ars" style="font-size: 10px">
                                توقع هذه المعاينة إجباريا من طرف السائقين معا، ولا تشكل اعترافا بالمسؤولية بل كشفا بالتعارف والوقائع
                                </span>
                            </p>
                        </td>
                    </tr>
                    <tr class="m-5">
                        <td width="50%">
                            <p>
                                <span class="infos-head">Date de l'accident, le</span>
                                <span class="txt-title-head">
                                    {{ $constat->workflow->date_etablissement }}
                                    <span class="sep-fill">heure:</span> {{ $constat->workflow->heure_etablissement }}
                                </span>
                            </p>
                        </td>
                        <td dir="rtl" width="50%" class="text-right rtl">
                            <p><span class="infos-head ars">تاريخ الحادثة، في</span>&nbsp;<span class="txt-title-head">{{ $constat->workflow->date_etablissement }}&nbsp;<span class="sep-fill">الساعة:</span>&nbsp;{{ $constat->workflow->heure_etablissement }}</span></p>
                        </td>
                    </tr>
                    <tr class="m-5">
                        <td width="50%"><p><span class="infos-head">Lieu précis </span> <span class="txt-title-head">{{ $constat->workflow->lieu }}</span></p></td>
                        <td dir="rtl" width="50%" class="text-right rtl"><p><span class="infos-head ars">المكان بالضبط</span><span class="txt-title-head"> {{ $constat->workflow->lieu }} </span></p></td>
                    </tr>
                    <tr class="m-5">
                        <td width="50%"><p><span class="infos-head">Dégâts matériels autres qu'aux véhicules A et B</span></p></td>
                        <!--<td width="30%">
                            <table>
                                <tbody>
                                <tr>
                                    <td></td>
                                </tr>
                                </tbody>
                            </table>
                        </td>-->
                        <td width="50%" class="text-right"><p dir="rtl"><span class="infos-head ars">الخسائر المادية اللاحقة بغير السيارتين ا م ب</span></p></td>
                    </tr>
                    <tr class="m-5">
                        <td width="50%"><p><span class="infos-head">Témoins (s'il s'agit de passagers d'un véhicule, préciser duquel) nom et adresses: </span></p></td>
                        <td width="50%" class="text-right"><p dir="rtl"><span class="infos-head ars">الشهود (إذا تعلق الأمر بمسافرين في إحدى السيارتين، بين أيهما) الأسماء والعناوين</span></p></td>
                    </tr>
                    <tr class="m-5">
                        <td width="100%" align="center"><p><span class="txt-title-head"></span></p></td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>

    <table>
        <tbody>
        <tr class="bloc-intos">
            <td width="38%" class="" valign="top" colspan="2">
                <table class="bloc-tbl">
                    <tbody>
                    <tr>
                        <td height="50">

                        </td>
                    </tr>
                    <tr>
                        <td height="30" valign="top">
                            <p><span class="title">Véhicule</span>&nbsp;&nbsp;<span class="txt-title">@if($constat->vehicules){{ $constat->vehicules[0]->modele }}@endif</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td height="19">
                            <p><span class="infos">Marque Type</span>&nbsp;&nbsp;<span class="txt-title">{{ $constat->vehicules[0]->marque }}</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td height="19">
                            <p><span class="infos">N° d'immatricul (ou n° de moteur)</span>&nbsp;&nbsp;<span class="txt-title">{{ $constat->vehicules[0]->num_immatriculation }}</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td height="19">
                            <p><span class="infos">Venant de</span>&nbsp;&nbsp;<span class="txt-title">{{ $constat->vehicules[0]->venant_de }}</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td height="19">
                            <p><span class="infos">Allant vers</span>&nbsp;&nbsp;<span class="txt-title">{{ $constat->vehicules[0]->allant_vers }}</span></p>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <table class="bloc-tbl">
                    <tbody>
                    <tr>
                        <td height="40" valign="top">
                            <p><span class="title">Assuré souscripteur<br>(voir attestation d'assuré)</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td height="19">
                            <p><span class="infos">Nom</span>&nbsp;&nbsp;<span class="txt-title">{{ $constat->assures[0]->nom }}</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td height="19">
                            <p><span class="infos">Prénom</span>&nbsp;&nbsp;<span class="txt-title">{{ $constat->assures[0]->prenom }}</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td height="19">
                            <p><span class="infos">Adresse</span>&nbsp;&nbsp;<span class="txt-title">{{ $constat->assures[0]->adresse }}</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td height="19">
                            <p><span class="infos">Sté d'assurance</span>&nbsp;&nbsp;<span class="txt-title">{{ $constat->assures[0]->ste_assurance }}</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td height="19">
                            <p><span class="infos">N° d'attestation</span>&nbsp;&nbsp;<span class="txt-title">{{ $constat->assures[0]->n_attestation }}</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td height="19">
                            <p><span class="infos">N° de police</span>&nbsp;&nbsp;<span class="txt-title">{{ $constat->assures[0]->n_police }}</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td height="19">
                            <p><span class="infos">N° de carte verte (pour les étrangers)</span>&nbsp;&nbsp;<span class="txt-title">{{ $constat->assures[0]->n_carte_verte }}</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td height="19">
                            <p><span class="infos">Attestation valable du</span>&nbsp;&nbsp;<span class="txt-title">{{ Carbon\Carbon::parse($constat->assures[0]->attes_valide_du)->format('d/m/Y') }}<span class="sep-fill"> au </span>{{ Carbon\Carbon::parse($constat->assures[0]->attes_valide_au)->format('d/m/Y') }}</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td height="19">
                            <p><span class="infos">Agence, bureau ou courtier</span>&nbsp;&nbsp;<span class="txt-title">{{ $constat->assures[0]->agence }}</span></p>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <table class="bloc-tbl">
                    <tbody>
                    <tr>
                        <td height="30" valign="top">
                            <p><span class="title">Conducteur<br>(voir permis de conduire)</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td height="19">
                            <p><span class="infos">Nom</span>&nbsp;&nbsp;<span class="txt-title">@if(count($constat->conducteurs) > 0){{ $constat->conducteurs[0]->nom }}@endif</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td height="19">
                            <p><span class="infos">Prénom</span>&nbsp;&nbsp;<span class="txt-title">@if(count($constat->conducteurs) > 0){{ $constat->conducteurs[0]->prenom }}@endif</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td height="19">
                            <p><span class="infos">Adresse</span>&nbsp;&nbsp;<span class="txt-title">@if(count($constat->conducteurs) > 0){{ $constat->conducteurs[0]->adresse }}@endif</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td height="19">
                            <p class="mb-0"><span class="infos">Permis de conduire n°</span>&nbsp;&nbsp;<span class="txt-title">@if(count($constat->conducteurs) > 0){{ $constat->conducteurs[0]->num_permis }}@endif</span></p>
                            <p><span class="infos">A1 B C D E F</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td height="40">
                            <p><span class="infos text-right">Délivré le </span><span class="txt-title">@if(count($constat->conducteurs) > 0){{ date('d/m/Y', strtotime($constat->conducteurs[0]->date_delivrance)) }}@endif</span></p>
                            <p><span class="infos">Par la préfecture de </span><span class="txt-title">@if(count($constat->conducteurs) > 0){{ $constat->conducteurs[0]->prefecture_delivrance }}@endif</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td height="40">
                            <p class="mb-0"><span class="infos">Permis valable jusqu'au  </span><span class="txt-title">@if(count($constat->conducteurs) > 0){{ date('d/m/Y', strtotime($constat->conducteurs[0]->delai_validite)) }}@endif</span></p>
                            <p><span class="infos">(pour les catégories COE et les taxis)</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td height="40" valign="top">
                            <p><span class="infos">Indiquer par une flèche &rarr; le point de choc initial</span><span class="txt-title"></span></p>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top">
                            <img width="180px" class="img" src="{{$_SERVER['DOCUMENT_ROOT'].'/img/vehicles/' . $constat->vehicules[0]->type . '_A.png'}}" />
                        </td>
                    </tr>
                    </tbody>
                </table>
                <table class="bloc-tbl">
                    <tbody>
                    <tr>
                        <td height="20">
                            <p><span class="title">Dégâts apparents</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p><span class="txt-title">{{ json_decode($constat->vehicules[0]->zone_choc) }}</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p><span class="txt-title"></span></p>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <table class="bloc-tbl">
                    <tbody>
                    <tr>
                        <td height="20">
                            <p><span class="title">Observations</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p><span class="txt-title" style="color: #ffffff;">--- -------- -------- ------ --- -------- ------ -------- --------------</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p><span class="txt-title" style="color: #ffffff;">--- -------- -------- ------ --- -------- ------ -------- --------------</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p><span class="txt-title" style="color: #ffffff;">--- -------- -------- ------ --- -------- ------ -------- --------------</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p><span class="txt-title" style="color: #ffffff;">--- -------- -------- ------ --- -------- ------ -------- --------------</span></p>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <table class="bloc-tbl">
                    <tbody>
                        <tr>
                            <td>
                                <span class="space">&nbsp;&nbsp;</span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="space">&nbsp;&nbsp;</span>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
            <td width="24%" class="center" valign="bottom">
                <img width="360px" class="img" src="{{$_SERVER['DOCUMENT_ROOT'].'/storage/cdn/constateurs/croquis/'.$constat->id_constat.'/'.json_decode($constat->workflow->croquis)->filepath }}" />
                <div><br><br><br><br></div>
            </td>
            <td width="38%" class="right rtl" dir="rtl" colspan="2">
                <table class="bloc-tbl">
                    <tbody>
                    <tr>
                        <td height="50">

                        </td>
                    </tr>
                    <tr>
                        <td height="30" valign="top">
                            <p><span class="title">سيارة</span>&nbsp;&nbsp;<span class="txt-title">{{ $constat->vehicules[1]->modele }}</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td height="19">
                            <p><span class="infos">نموذج، نوع</span>&nbsp;&nbsp;<span class="txt-title">{{ $constat->vehicules[1]->marque }}</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td height="19">
                            <p><span class="infos">رقم التسجيل (ورقم المحرك)</span>&nbsp;&nbsp;<span class="txt-title">{{ $constat->vehicules[1]->num_immatriculation }}</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td height="19">
                            <p><span class="infos">القادمة من</span>&nbsp;&nbsp;<span class="txt-title">{{ $constat->vehicules[1]->venant_de }}</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td height="19">
                            <p><span class="infos">المتجهة نحو</span>&nbsp;&nbsp;<span class="txt-title">{{ $constat->vehicules[1]->allant_vers }}</span></p>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <table class="bloc-tbl">
                    <tbody>
                    <tr>
                        <td height="40" valign="top">
                            <p><span class="title">المؤمن : المكتتب (انظر شهادة المؤمن)</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td height="19">
                            <p><span class="infos">الإسم العائلي</span>&nbsp;&nbsp;<span class="txt-title">@if(count($constat->assures) > 0){{ $constat->assures[1]->nom }}@endif</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td height="19">
                            <p><span class="infos">الإسم الشخصي</span>&nbsp;&nbsp;<span class="txt-title">@if(count($constat->assures) > 0){{ $constat->assures[1]->prenom }}@endif</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td height="19">
                            <p style="padding-left: 30px;"><span class="infos">العنوان</span>&nbsp;&nbsp;<span class="txt-title">@if(count($constat->assures) > 0){{ $constat->assures[1]->adresse }}@endif</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td height="19">
                            <p><span class="infos">شركة التأمين</span>&nbsp;&nbsp;<span class="txt-title">@if(count($constat->assures) > 0){{ $constat->assures[1]->ste_assurance }}@endif</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td height="19">
                            <p><span class="infos">رقم الرخصة</span>&nbsp;&nbsp;<span class="txt-title">@if(count($constat->assures) > 0){{ $constat->assures[1]->n_attestation }}@endif</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td height="19">
                            <p><span class="infos">عنوان مركز الشرطة</span>&nbsp;&nbsp;<span class="txt-title">@if(count($constat->assures) > 0){{ $constat->assures[1]->n_police }}@endif</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td height="19">
                            <p><span class="infos">رقم البطاقة الخضراء (بالنسبة للأجانب)</span>&nbsp;&nbsp;<span class="txt-title">@if(count($constat->assures) > 0){{ $constat->assures[1]->n_carte_verte }}@endif</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td height="19">
                            <p><span class="infos">شهادة صالحة من</span>&nbsp;&nbsp;<span class="txt-title">@if(count($constat->assures) > 0){{ Carbon\Carbon::parse($constat->assures[1]->attes_valide_du)->format('d/m/Y') }}@endif<span class="sep-fill"> إلى </span>@if(count($constat->conducteurs) > 0){{ Carbon\Carbon::parse($constat->assures[1]->attes_valide_au)->format('d/m/Y') }}@endif</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td height="19">
                            <p><span class="infos">وكالة، مكتب أو وسيط</span>&nbsp;&nbsp;<span class="txt-title">@if(count($constat->assures) > 0){{ $constat->assures[1]->agence }}@endif</span></p>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <table class="bloc-tbl">
                    <tbody>
                    <tr>
                        <td height="40" valign="top">
                            <p><span class="title">سائق السيارة (أنظر رخصة القيادة)</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td height="19">
                            <p><span class="infos">الإسم العائلي</span>&nbsp;&nbsp;<span class="txt-title">@if(count($constat->conducteurs) > 0){{ $constat->conducteurs[1]->nom }}@endif</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td height="19">
                            <p><span class="infos">الإسم الشخصي</span>&nbsp;&nbsp;<span class="txt-title">@if(count($constat->conducteurs) > 0){{ $constat->conducteurs[1]->prenom }}@endif</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td height="19">
                            <p><span class="infos">العنوان</span>&nbsp;&nbsp;<span class="txt-title">@if(count($constat->conducteurs) > 0){{ $constat->conducteurs[1]->adresse }}@endif</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td height="40">
                            <p class="mb-0"><span class="infos">رخصة القيادة رقم</span>&nbsp;&nbsp;<span class="txt-title">@if(count($constat->conducteurs) > 0){{ $constat->conducteurs[1]->num_permis }}@endif</span></p>
                            <p><span class="infos">أ1 ب س د ف</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td height="45">
                            <p><span class="infos text-right">المسلمة في</span>&nbsp;&nbsp;<span class="txt-title">@if(count($constat->conducteurs) > 0){{ date('d/m/Y', strtotime($constat->conducteurs[1]->date_delivrance)) }}@endif</span></p>
                            <p><span class="infos">من عمالة</span>&nbsp;&nbsp;<span class="txt-title">@if(count($constat->conducteurs) > 0){{ $constat->conducteurs[1]->prefecture_delivrance }}@endif</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td height="45">
                            <p class="mb-0"><span class="infos">رخصة صالحة لغاية</span>&nbsp;&nbsp;<span class="txt-title">@if(count($constat->conducteurs) > 0){{ date('d/m/Y', strtotime($constat->conducteurs[1]->delai_validite)) }}@endif</span></p>
                            <p><span class="infos">(خاصة بالتصنيفات س د أ وسيارات الأجرة)</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td height="40" valign="top">
                            <p><span class="infos"> بينوا بواسطة سهم <span>&larr;</span> نقطة الإصطدام الأولية </span></p>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="top">
                            <img width="180px" class="img" src="{{$_SERVER['DOCUMENT_ROOT'].'/img/vehicles/' . $constat->vehicules[1]->type . '_B.png'}}" />
                        </td>
                    </tr>
                    </tbody>
                </table>
                <table class="bloc-tbl">
                    <tbody>
                    <tr>
                        <td height="20">
                            <p><span class="title">الخسائر الظاهرة</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p>
                            <span class="txt-title">
                                {{ json_decode($constat->vehicules[1]->zone_choc) }}
                            </span>
                            </p>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <table class="bloc-tbl">
                    <tbody>
                    <tr>
                        <td height="20">
                            <p><span class="title">ملاحظات</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p><span class="txt-title" style="color: #ffffff;">--- -------- -------- ------ --- -------- ------ -------- ----------</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p><span class="txt-title" style="color: #ffffff;">--- -------- -------- ------ --- -------- ------ -------- ----------</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p><span class="txt-title" style="color: #ffffff;">--- -------- -------- ------ --- -------- ------ -------- ----------</span></p>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <p><span class="txt-title" style="color: #ffffff;">--- -------- -------- ------ --- -------- ------ -------- ----------</span></p>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <table class="bloc-tbl">
                    <tbody>
                        <tr>
                            <td>
                                <span class="space">&nbsp;&nbsp;</span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span class="space">&nbsp;&nbsp;</span>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="5" height="15"></td>
        </tr>
        <tr>
            <td width="2%"></td>
            <td width="31%" valign="top">
                <p>
                    <span class="infos" style="font-size: 14px; line-height: 25px;">Ne rien modifier au constat après séparation des exemplaires des deux conducteurs. En cas de blessures ou en cas de pertes matérielles autres qu'aux les voitures A et B, enregistrer les données d'identification, l'adresse, etc...</span>
                </p>
            </td>
            <td width="34%" valign="top">
                <table class="bloc-tbl mt-0">
                    <tbody>
                    <tr>
                        <td width="50%" class="pd" align="left" valign="top">
                            <img width="65px" class="img" src="{{$_SERVER['DOCUMENT_ROOT'].'/storage/cdn/constateurs/signatures/'.$constat->id_constat.'/'.$constat->conducteurs[0]->id_conducteur.'/'.json_decode($constat->conducteurs[0]->signature)->filepath }}" />
                        </td>
                        <td width="50%" align="right" valign="top">
                            <img width="65px" class="img" src="{{$_SERVER['DOCUMENT_ROOT'].'/storage/cdn/constateurs/signatures/'.$constat->id_constat.'/'.$constat->conducteurs[1]->id_conducteur.'/'.json_decode($constat->conducteurs[1]->signature)->filepath }}" />
                        </td>
                    </tr>
                    <tr>
                        <td width="50%" valign="top">
                            <p><span class="infos">Voir déclaration de l'assuré au verso</span></p>
                        </td>
                        <td width="50%" class="right rtl pd" dir="rtl" valign="top" align="right">
                            <p><span class="infos">أنظرو تصريح المؤمن على ظهر الورقة</span></p>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
            <td width="31%" class="right rtl" dir="rtl" valign="top">
                <p>
                    <span class="infos" style="font-size: 17px; line-height: 26px;">لا يمكن تغيير شيء في هذه المعاينة بعد فصل نموذجي السائقين وفي حالة الجروح أو في حالة الخسائر المادية اللاحقة بغير السيارتين أ و ب، سجلوا بيانات التعريف والعنوان الخ ...</span>
                </p>
            </td>
            <td width="2%"></td>
        </tr>
        <tr>
            <td>
                <span class="space">&nbsp;&nbsp;</span>
            </td>
        </tr>
        </tbody>
    </table>
</div>
</body>
</html>
