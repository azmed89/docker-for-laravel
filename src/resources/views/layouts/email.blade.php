<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <style>
            a.links {
                display: inline-block;
                background: lightgreen;
                height: 50px;
            }
        </style>
    </head>

    <body style="font-family: arial;">
        <table style="background: #f3f2f0;" align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
            <tbody>
            <tr>
                <td align="center" width="100%">
                    <table align="center" border="0" cellpadding="0" cellspacing="0" width="700" style="background: white; margin-top: 30px;margin-bottom: 30px; color: black;">
                        <tbody>
                            <tr>
                                <td align="center" width="100%">
                                    <img src="{{ secure_asset('assets/images/mail/template_header.png') }}" style="width:100%;min-height:auto;display:block" alt="" border="0" height="auto" width="650" class="">
                                </td>
                            </tr>
                            <tr>
                                <td align="center" width="100%">
                                    <table style="width: 100%;" align="center" border="0" cellpadding="0" cellspacing="0" width="570">
                                        <tbody>
                                            <tr>
                                                <td height="40" width="100%" style="color: #555555;font-size: 18px;padding:22px;text-align:justify;line-height: 28px;">
                                                    @yield('body')
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="20" width="100%">
                                                    <hr style="width: 68%;height: 4px;background: #dd5554;border: none;">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="20" width="100%"></td>
                                            </tr>

                                            <tr>
                                                <td height="20" width="100%" ></td>
                                            </tr>
                                            <tr>
                                                <td height="40" width="100%" style="color: #555555;font-size: 18px;text-align: center;">
                                                    Cordialement,
                                                    <br>
                                                    {{ config('templates.entity.signature') }}
                                                    <br>
                                                    <a href="{{ config('templates.entity.subscription_link') }}" style="padding-top:10px; font-size: 24px;font-family: arial;padding:10px;text-decoration:underline;color:#555555;"  target="_blank">{{ config('templates.entity.subscription_link') }}</a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="40" width="100%"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td height="80" width="100%"></td>
            </tr>
            </tbody>
        </table>
    </body>
</html>
