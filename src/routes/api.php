<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Auth Administration
Route::prefix('v1')->group(function () {
    //Auth BO
    Route::prefix('/admin/auth')->group(function () {
        // Create New User
        Route::post('register', 'AuthController@register');
        // Login User
        Route::post('login', 'AuthController@login');
        Route::post('refresh', 'AuthController@refresh');
        // Below mention routes are available only for the authenticated users.
        Route::middleware(['auth.jwt','auth.user', 'api'])->group(function () {
            // Get user info
            Route::get('me', 'AuthController@me');
            // Logout user from application
            Route::get('logout', 'AuthController@logout');
        });
    });
});

// Auth Prestataire
Route::prefix('v1')->group(function () {

    Route::prefix('/pre/auth')->group(function () {

        // Create New User
        Route::post('register', 'AuthController@register');
        // Login User
        Route::post('login', 'AuthController@login');

        Route::post('refresh', 'AuthController@refresh');
        // Below mention routes are available only for the authenticated users.
        Route::middleware(['auth.jwt','auth.user_prestataire', 'api'])->group(function () {
            // Get user info
            Route::get('me', 'AuthController@me');
            // Logout user from application
            Route::get('logout', 'AuthController@logout');
        });
    });

    Route::prefix('users')->group(function () {
        Route::middleware('auth:api')->group(function () {
            // Get all users
            Route::get('list', 'UserController@index');
            Route::post('new', 'UserController@add');
        });
    });
});

// Auth Assistance
Route::prefix('v1')->group(function () {
    //Auth BO
    Route::prefix('/ass/auth')->group(function () {

        // Create New User
        Route::post('register', 'AuthController@register');
        // Login User
        Route::post('login', 'AuthController@login');
        // Below mention routes are available only for the authenticated users.
        Route::middleware(['auth.jwt','auth.user_assistance'])->group(function () {
            // Get user info
            Route::get('me', 'AuthController@me');
            // Logout user from application
            Route::get('logout', 'AuthController@logout');
        });
    });
});

// Auth Assurance
Route::prefix('v1')->group(function () {
    //Auth BO
    Route::prefix('/assu/auth')->group(function () {
        // Create New User
        Route::post('register', 'AuthController@register');
        // Login User
        Route::post('login', 'AuthController@login');
        // Below mention routes are available only for the authenticated users.
        Route::middleware(['auth.jwt','auth.user_assurance'])->group(function () {
            // Get user info
            Route::get('me', 'AuthController@me');
            // Logout user from application
            Route::get('logout', 'AuthController@logout');
        });
    });
});

// Auth Constateur
Route::prefix('v1')->group(function () {

    Route::prefix('/cons/auth')->group(function () {
        // Login User
        Route::post('login', 'AuthController@login');
        // Below mention routes are available only for the authenticated users.
        Route::middleware(['auth.jwt','auth.constateur', 'api'])->group(function () {
            // Get user info
            Route::get('me', 'AuthController@me');
            // Logout user from application
            Route::get('logout', 'AuthController@logout');
        });
    });
});
Route::prefix('v1')->group(function () {
    //Active compte
    Route::post('activation_compte', 'Auth\ResetPasswordController@reset');

});
